### What is this repository for? ###

This repository contains all the code that I created for a University Swipe Card System, built during my time at university.

The code is separated into three separate projects: SOFT252GUI, SOFT252DataModel and Utilities.

The Utilities library contains a set of custom interfaces and classes for implementing the observer pattern and command pattern.

The SOFT252DataModel library contains the data model used in the project. 

The SOFT252GUI library is the userinterface used for the project.

To Build:
You'll need Netbeans 8.2 or later to build and run the GUI. 

You can download the latest build here: https://drive.google.com/open?id=1_qcf2HzO2UXF8qumez-_wXGamZEfL_pO

To run it you'll need an up-to-date version of the Java runtime.


For more information please get in touch.

My email is joshcrawley18@yahoo.co.uk

My site is https://joshcrawley18.wixsite.com/joshuacrawley