/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package soft252gui.gui;

import soft252gui.commands.AddBuildingCommand;
import soft252gui.commands.GUICommandTracker;
import command.Command;
import datamodel.campus.Building;
import datamodel.campus.interfaces.IBuilding;
import datamodel.campus.interfaces.ICampus;
import javax.swing.JOptionPane;

/**
 *
 * @author jjcrawley
 */
public class AddBuildingForm extends javax.swing.JFrame 
{
    private ICampus m_currentCampus;
    
    /**
     * Creates new form EditBuildingForm
     */
    public AddBuildingForm()
    {
        initComponents();
    }    
    
    public void setCampus(ICampus campus)
    {
        m_currentCampus = campus;
        txtCurrentCampus.setText(m_currentCampus.getCampusCode());
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {

        lblAddBuildingHeader = new javax.swing.JLabel();
        btnAddBuilding = new javax.swing.JButton();
        pnlBuildingDetails = new javax.swing.JPanel();
        lblCampus = new javax.swing.JLabel();
        txtBuildingCode = new javax.swing.JTextField();
        lblBuildingCode = new javax.swing.JLabel();
        lblBuildingName = new javax.swing.JLabel();
        txtBuildingName = new javax.swing.JTextField();
        txtCurrentCampus = new javax.swing.JTextField();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Add Building");
        setResizable(false);
        addWindowListener(new java.awt.event.WindowAdapter()
        {
            public void windowClosed(java.awt.event.WindowEvent evt)
            {
                formWindowClosed(evt);
            }
        });

        lblAddBuildingHeader.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        lblAddBuildingHeader.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblAddBuildingHeader.setText("Add Building");

        btnAddBuilding.setText("Add Building");
        btnAddBuilding.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                btnAddBuildingActionPerformed(evt);
            }
        });

        pnlBuildingDetails.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.LOWERED));

        lblCampus.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblCampus.setText("Campus");

        lblBuildingCode.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblBuildingCode.setText("Building Code");

        lblBuildingName.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblBuildingName.setText("Building Name");

        txtCurrentCampus.setEditable(false);

        javax.swing.GroupLayout pnlBuildingDetailsLayout = new javax.swing.GroupLayout(pnlBuildingDetails);
        pnlBuildingDetails.setLayout(pnlBuildingDetailsLayout);
        pnlBuildingDetailsLayout.setHorizontalGroup(
            pnlBuildingDetailsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlBuildingDetailsLayout.createSequentialGroup()
                .addGroup(pnlBuildingDetailsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pnlBuildingDetailsLayout.createSequentialGroup()
                        .addContainerGap(15, Short.MAX_VALUE)
                        .addGroup(pnlBuildingDetailsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(lblBuildingCode, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(lblBuildingName, javax.swing.GroupLayout.DEFAULT_SIZE, 87, Short.MAX_VALUE)))
                    .addGroup(pnlBuildingDetailsLayout.createSequentialGroup()
                        .addGap(20, 20, 20)
                        .addComponent(lblCampus, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addGap(20, 20, 20)
                .addGroup(pnlBuildingDetailsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txtCurrentCampus, javax.swing.GroupLayout.PREFERRED_SIZE, 94, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtBuildingCode, javax.swing.GroupLayout.PREFERRED_SIZE, 94, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtBuildingName, javax.swing.GroupLayout.PREFERRED_SIZE, 94, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18))
        );
        pnlBuildingDetailsLayout.setVerticalGroup(
            pnlBuildingDetailsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlBuildingDetailsLayout.createSequentialGroup()
                .addGap(0, 11, Short.MAX_VALUE)
                .addGroup(pnlBuildingDetailsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblCampus)
                    .addComponent(txtCurrentCampus, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(pnlBuildingDetailsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblBuildingCode)
                    .addComponent(txtBuildingCode, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pnlBuildingDetailsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblBuildingName)
                    .addComponent(txtBuildingName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(16, 16, 16)
                        .addComponent(lblAddBuildingHeader, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(pnlBuildingDetails, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addContainerGap())
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(btnAddBuilding)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblAddBuildingHeader)
                .addGap(14, 14, 14)
                .addComponent(pnlBuildingDetails, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btnAddBuilding)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnAddBuildingActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAddBuildingActionPerformed
        
        addBuilding();
    }//GEN-LAST:event_btnAddBuildingActionPerformed

    private void formWindowClosed(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosed
        
        txtBuildingCode.setText("");
        txtBuildingName.setText("");
    }//GEN-LAST:event_formWindowClosed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[])
    {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(AddBuildingForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(AddBuildingForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(AddBuildingForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(AddBuildingForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable()
        {
            public void run() 
            {
                new AddBuildingForm().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAddBuilding;
    private javax.swing.JLabel lblAddBuildingHeader;
    private javax.swing.JLabel lblBuildingCode;
    private javax.swing.JLabel lblBuildingName;
    private javax.swing.JLabel lblCampus;
    private javax.swing.JPanel pnlBuildingDetails;
    private javax.swing.JTextField txtBuildingCode;
    private javax.swing.JTextField txtBuildingName;
    private javax.swing.JTextField txtCurrentCampus;
    // End of variables declaration//GEN-END:variables
    
    private void clearDisplay()
    {
        txtBuildingCode.setText("");
        txtBuildingName.setText("");
    }   
    
    private void addBuilding()
    {
        if(m_currentCampus == null)
        {
            JOptionPane.showMessageDialog(this, "error in campus");
            return;
        }
        if(verifyInput())
        {
            String name = txtBuildingName.getText();
            String code = txtBuildingCode.getText();
            
            IBuilding newBuilding = new Building(name, code);
            
            boolean success = GUICommandTracker.getInstance()
                    .executeCommand(new Command(new AddBuildingCommand(newBuilding, m_currentCampus)));
            
            if(!success)
            {
                JOptionPane.showMessageDialog(this, "building already exists");
            }
            else
            {
                JOptionPane.showMessageDialog(this, "building was added");
            }
        }
    }
    
    private boolean verifyInput()
    {
        return verifyCode() && verifyName();        
    }
    
    private boolean verifyCode()
    {
        return !txtBuildingCode.getText().isEmpty();
    }
    
    private boolean verifyName()
    {
        return !txtBuildingName.getText().isEmpty();
    }
}
