/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package soft252gui.gui;

import datamodel.campus.rooms.interfaces.IModeChangeable;
import datamodel.restrictions.OperatingMode;
import java.util.List;
import observer.ObservableEvent;
import observer.interfaces.IObservable;
import observer.interfaces.IObserver;

/**
 *
 * @author Joshua
 */
public class ModeChangeForm extends javax.swing.JFrame implements IObservable
{
    private IModeChangeable m_changingMode;
    private final ObservableEvent onModeChange;

    /**
     * Creates new form ModeChangeForm
     */
    public ModeChangeForm() 
    {
        onModeChange = new ObservableEvent();
        
        initComponents();
    }
    
    public void setupForm(IModeChangeable changingMode)
    {
        m_changingMode = changingMode;        
    }
    
    public void closeForm(IModeChangeable changingMode)
    {
        if(changingMode == m_changingMode || changingMode == null)
        {
            m_changingMode = null;
            setVisible(false);
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        lblModeChangeHeader = new javax.swing.JLabel();
        pnlDetails = new javax.swing.JPanel();
        lblDetails = new javax.swing.JLabel();
        lblReason = new javax.swing.JLabel();
        scrDetails = new javax.swing.JScrollPane();
        txaDetails = new javax.swing.JTextArea();
        scrReason = new javax.swing.JScrollPane();
        txaReason = new javax.swing.JTextArea();
        btnChangeMode = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                formWindowClosing(evt);
            }
        });

        lblModeChangeHeader.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        lblModeChangeHeader.setText("Mode Change");

        pnlDetails.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.LOWERED));

        lblDetails.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblDetails.setText("Details");

        lblReason.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblReason.setText("Reason");

        txaDetails.setColumns(20);
        txaDetails.setLineWrap(true);
        txaDetails.setRows(5);
        scrDetails.setViewportView(txaDetails);

        txaReason.setColumns(20);
        txaReason.setLineWrap(true);
        txaReason.setRows(5);
        scrReason.setViewportView(txaReason);

        javax.swing.GroupLayout pnlDetailsLayout = new javax.swing.GroupLayout(pnlDetails);
        pnlDetails.setLayout(pnlDetailsLayout);
        pnlDetailsLayout.setHorizontalGroup(
            pnlDetailsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlDetailsLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlDetailsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblDetails, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblReason, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(28, 28, 28)
                .addGroup(pnlDetailsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(scrDetails, javax.swing.GroupLayout.DEFAULT_SIZE, 208, Short.MAX_VALUE)
                    .addComponent(scrReason))
                .addGap(27, 27, 27))
        );
        pnlDetailsLayout.setVerticalGroup(
            pnlDetailsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlDetailsLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlDetailsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pnlDetailsLayout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(scrDetails, javax.swing.GroupLayout.PREFERRED_SIZE, 57, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(31, 31, 31))
                    .addGroup(pnlDetailsLayout.createSequentialGroup()
                        .addComponent(lblDetails)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addGroup(pnlDetailsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(scrReason, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblReason))
                .addContainerGap())
        );

        btnChangeMode.setText("Change Mode");
        btnChangeMode.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnChangeModeActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(154, 154, 154)
                        .addComponent(lblModeChangeHeader))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(31, 31, 31)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(btnChangeMode)
                            .addComponent(pnlDetails, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap(42, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblModeChangeHeader)
                .addGap(18, 18, 18)
                .addComponent(pnlDetails, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(btnChangeMode)
                .addContainerGap(19, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnChangeModeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnChangeModeActionPerformed
        
        changeMode();        
    }//GEN-LAST:event_btnChangeModeActionPerformed

    private void formWindowClosing(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosing
        // TODO add your handling code here:
        notifyObservers();
    }//GEN-LAST:event_formWindowClosing

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) 
    {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(ModeChangeForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(ModeChangeForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(ModeChangeForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(ModeChangeForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(() -> 
        {
            new ModeChangeForm().setVisible(true);
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnChangeMode;
    private javax.swing.JLabel lblDetails;
    private javax.swing.JLabel lblModeChangeHeader;
    private javax.swing.JLabel lblReason;
    private javax.swing.JPanel pnlDetails;
    private javax.swing.JScrollPane scrDetails;
    private javax.swing.JScrollPane scrReason;
    private javax.swing.JTextArea txaDetails;
    private javax.swing.JTextArea txaReason;
    // End of variables declaration//GEN-END:variables
    
    private void changeMode()
    {
        String details = txaDetails.getText();
        String reason = txaReason.getText();
        
        m_changingMode.changeMode(OperatingMode.Emergency);
        
        if(details.isEmpty() && reason.isEmpty())
        {
            m_changingMode.logModeChange();
        }
        else if(details.isEmpty())
        {
            m_changingMode.logModeChange(reason);
        }           
        else
        {
            m_changingMode.logModeChange(details, reason);
        }   
        
        notifyObservers();
        dispose();
    }

    @Override
    public Boolean registerObserver(IObserver observer) 
    {
        return onModeChange.registerObserver(observer);
    }

    @Override
    public Boolean removeObserver(IObserver toRemove) 
    {
        return onModeChange.removeObserver(toRemove);
    }

    @Override
    public void notifyObservers() 
    {
        onModeChange.notifyObservers();
    }  

    @Override
    public List<IObserver> getObservers() 
    {
        return onModeChange.getObservers();
    }   
}