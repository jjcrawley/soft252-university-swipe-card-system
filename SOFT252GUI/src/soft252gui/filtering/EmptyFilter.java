/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package soft252gui.filtering;

import java.util.List;

/**
 *
 * @author Joshua
 */
public class EmptyFilter<T> implements IFilter<T>
{
    @Override
    public List<T> filterList(List<T> list) 
    {
        return list;        
    }      

    @Override
    public String toString() 
    {
        return "None";
    }    
}
