/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package soft252gui.filtering;

import datamodel.logging.SwipeEntryTimeStamp;
import datamodel.logging.interfaces.ITimeStamp;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Joshua
 */
public class SwipeEntryFilter implements IFilter<ITimeStamp>
{
    public SwipeEntryFilter()
    {}
    
    @Override
    public List<ITimeStamp> filterList(List<ITimeStamp> list) 
    {
        List<ITimeStamp> stamps = new ArrayList<>();
        
        list.stream().forEach((currentStamp) -> 
        {
            if(currentStamp.getClass().equals(SwipeEntryTimeStamp.class))
            {
                stamps.add(currentStamp);
            }
        });     
        
        return stamps;
    }   

    @Override
    public String toString() 
    {
        return "Swipe Entry";
    }   
}
