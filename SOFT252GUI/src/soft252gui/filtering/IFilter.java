/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package soft252gui.filtering;

import java.util.List;

/**
 * Basic behaviour for filter that is capable of filtering out a list
 * @author Joshua
 * @param <T> The type that is being filtered
 */
public interface IFilter<T>
{
    public List<T> filterList(List<T> list);    
}
