/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package soft252gui.sorting;

import datamodel.users.interfaces.IUser;

/**
 *
 * @author Joshua
 */
public class SortByUserID extends Sorter<IUser>
{   
    public SortByUserID(SortOrder order)
    {
        super(order);                
    }
    
    @Override
    public int compare(IUser t, IUser t1) 
    {
        if(t == null || t1 == null)
        {
            return super.compare(t, t1);
        }
        
        if(e_order.equals(SortOrder.Ascending))
        {
            return Integer.compare(t.viewUserID(), t1.viewUserID());
        }
        else
        {
            return Integer.compare(t1.viewUserID(), t.viewUserID());
        }       
    }    

    @Override
    public String toString() 
    {
        return " UserID ";
    }   
}
