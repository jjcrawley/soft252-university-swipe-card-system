/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package soft252gui.sorting;

import datamodel.users.interfaces.IUser;

/**
 *
 * @author Joshua
 */
public class SortByUserSurname extends Sorter<IUser>
{

    public SortByUserSurname(SortOrder order)
    {
        super(order);
    }

    @Override
    public int compare(IUser t, IUser t1) 
    {
        if(t == null || t1 == null)
        {
            return super.compare(t, t1); //To change body of generated methods, choose Tools | Templates.
        }    
        
        String tSurname = t.getUserName().getSurname();
        String t1Surname = t1.getUserName().getSurname();
        
        if(e_order.equals(SortOrder.Ascending))
        {
            return tSurname.compareTo(t1Surname);
        }
        else
        {
            return t1Surname.compareTo(tSurname);
        }
    }

    @Override
    public String toString() 
    {
        return "User Surname";
    }   
}
