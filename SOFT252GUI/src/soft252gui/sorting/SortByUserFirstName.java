/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package soft252gui.sorting;

import datamodel.users.interfaces.IUser;

/**
 *
 * @author Joshua
 */
public class SortByUserFirstName extends Sorter<IUser>
{
    public SortByUserFirstName(SortOrder order)
    {
        super(order);
    }
    
    @Override
    public int compare(IUser t, IUser t1) 
    {
        if(t == null || t1 == null)
        {
            return super.compare(t, t1);
        }
        
        String tUserName = t.getUserName().getFirstName();
        String t1UserName = t1.getUserName().getFirstName();
        
        if(e_order.equals(SortOrder.Ascending))
        {
            return tUserName.compareTo(t1UserName);
        }
        else
        {
            return t1UserName.compareTo(tUserName);            
        }
    }     

    @Override
    public String toString() 
    {
        return "User First Name";
    }   
}
