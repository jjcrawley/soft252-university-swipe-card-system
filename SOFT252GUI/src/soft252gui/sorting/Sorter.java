/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package soft252gui.sorting;

import java.util.Comparator;

/**
 * A base class for basic sorting operations
 * Inherit from this class to provide custom sorting for lists by passing them into the List.Sort method
 * @author Joshua
 * @param <T>
 */
public abstract class Sorter<T> implements Comparator<T>
{
    protected SortOrder e_order;
    
    public Sorter(SortOrder order) 
    {
        e_order = order;
    }
    
    public final void setOrder(SortOrder newOrder)
    {
        if(newOrder != null)
        {
            e_order = newOrder;
        }
    }

    @Override
    public int compare(T t, T t1) 
    {
        if(t == null && t1 == null)
        {
            return 0;
        }
        else if(t == null)
        {
            return e_order.equals(SortOrder.Ascending) ? -1 : 1;
        }
        else
        {
            return e_order.equals(SortOrder.Ascending) ? 1 : -1;
        }
    }   
}
