/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package soft252gui.sorting;

import datamodel.logging.interfaces.ITimeStamp;

/**
 *
 * @author Joshua
 */
public class SortByDate extends Sorter<ITimeStamp>
{            
    public SortByDate(SortOrder order) 
    {
        super(order);
    }

    @Override
    public int compare(ITimeStamp t, ITimeStamp t1) 
    {
        if(t == null || t1 == null)
        {
            return super.compare(t, t1); 
        }   
        else
        {
            if(e_order.equals(SortOrder.Ascending))
            {
                return t.getCreationDate().compareTo(t1.getCreationDate());
            }
            else
            {
                return t1.getCreationDate().compareTo(t.getCreationDate());
            }
        }
    }
}
