/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package soft252gui.commands;

import command.interfaces.ICommandBehaviour;
import datamodel.campus.CentralServer;
import datamodel.campus.interfaces.ICampus;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Joshua
 */
public class RemoveCampusCommand implements ICommandBehaviour
{
    private final ICampus m_campus;
    private final List<RemoveBuildingCommand> m_rooms;
    
    public RemoveCampusCommand(ICampus campus)
    {
        m_campus = campus;
        m_rooms = new ArrayList<>();
        
        m_campus.getBuildings().forEach((building) -> 
        {
            m_rooms.add(new RemoveBuildingCommand(m_campus, building));
        });
    }

    @Override
    public Boolean doCommand() 
    {        
        m_rooms.stream().forEach((command) -> 
        {
            command.doCommand();
        });
        
        return CentralServer.getInstance().removeCampus(m_campus.getCampusCode());
    }

    @Override
    public Boolean undoCommand() 
    {
        m_rooms.stream().forEach((command) -> 
        {
            command.undoCommand();
        });
        
        return CentralServer.getInstance().addCampus(m_campus);
    }   
}
