/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package soft252gui.commands;

import command.CommandTracker;
import command.interfaces.ICommand;
import command.interfaces.ICommandTracker;

/**
 *
 * @author Joshua
 */
public class GUICommandTracker implements ICommandTracker
{
    private final static ICommandTracker s_tracker = new GUICommandTracker();

    private final ICommandTracker m_tracker = new CommandTracker();
    
    @Override
    public Boolean executeCommand(ICommand aCommand) 
    {
        return m_tracker.executeCommand(aCommand);
    }

    @Override
    public Boolean undoLastCommand() 
    {
        return m_tracker.undoLastCommand();
    }

    @Override
    public Boolean redoLastCommand() 
    {
        return m_tracker.redoLastCommand();
    }

    @Override
    public Boolean isUndoable() 
    {
        return m_tracker.isUndoable();
    }

    @Override
    public Boolean isRedoable() 
    {
        return m_tracker.isRedoable();
    }

    @Override
    public void clearCommands() 
    {
        m_tracker.clearCommands();
    }    
    
    public static ICommandTracker getInstance()
    {
        return s_tracker;        
    }  
}
