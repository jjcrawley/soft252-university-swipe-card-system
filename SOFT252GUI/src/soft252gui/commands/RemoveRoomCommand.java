/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package soft252gui.commands;

import command.interfaces.ICommandBehaviour;
import datamodel.campus.interfaces.IBuilding;
import datamodel.campus.rooms.interfaces.IRoom;

/**
 *
 * @author Joshua
 */
public class RemoveRoomCommand implements ICommandBehaviour
{
    private final IRoom m_room;
    private final IBuilding m_building;
    
    public RemoveRoomCommand(IRoom room, IBuilding building) 
    {
        m_room = room;
        m_building = building;
    }    

    @Override
    public Boolean doCommand() 
    {
        return m_building.removeRoom(m_room.getRoomCode());
    }

    @Override
    public Boolean undoCommand() 
    {
        return m_building.addRoom(m_room);
    }           
}
