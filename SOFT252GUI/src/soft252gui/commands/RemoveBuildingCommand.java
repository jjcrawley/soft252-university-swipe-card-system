/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package soft252gui.commands;

import command.interfaces.ICommandBehaviour;
import datamodel.campus.interfaces.IBuilding;
import datamodel.campus.interfaces.ICampus;
import datamodel.campus.rooms.interfaces.IRoom;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Joshua
 */
public class RemoveBuildingCommand implements ICommandBehaviour
{   
    private final ICampus m_campus;
    private final IBuilding m_building;
    private final List<IRoom> m_rooms;
    
    public RemoveBuildingCommand(ICampus campus, IBuilding building)
    {
        m_campus = campus;
        m_building = building;
        m_rooms = new ArrayList<>();
        
        m_rooms.addAll(m_building.getRooms());
    }

    @Override
    public Boolean doCommand() 
    {        
        return m_campus.removeBuilding(m_building.getBuildingCode());
    }

    @Override
    public Boolean undoCommand() 
    {
        m_rooms.forEach((room) -> 
        {
            m_building.addRoom(room);
        });
        
        return m_campus.addBuilding(m_building);
    }   
}
