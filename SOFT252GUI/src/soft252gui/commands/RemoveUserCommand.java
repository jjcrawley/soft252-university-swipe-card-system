/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package soft252gui.commands;

import command.interfaces.ICommandBehaviour;
import datamodel.campus.CentralServer;
import datamodel.users.interfaces.ICard;
import datamodel.users.interfaces.IUser;

/**
 *
 * @author Joshua
 */
public class RemoveUserCommand implements ICommandBehaviour
{
    private final IUser m_user;
    private final ICard m_userCard;
    
    public RemoveUserCommand(IUser user)
    {
        m_user = user;
        m_userCard = user.getUserCard();
    }

    @Override
    public Boolean doCommand() 
    {        
        return CentralServer.getInstance().removeUser(m_user.viewUserID());
    }

    @Override
    public Boolean undoCommand() 
    {
        m_user.assignCard(m_userCard);
        
        return CentralServer.getInstance().addUser(m_user);
    }        
}
