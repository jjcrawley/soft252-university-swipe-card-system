/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package soft252gui.commands;

import command.interfaces.ICommandBehaviour;
import datamodel.campus.interfaces.IBuilding;
import datamodel.campus.interfaces.ICampus;
import datamodel.campus.rooms.interfaces.IRoom;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Joshua
 */
public class AddBuildingCommand implements ICommandBehaviour
{
    private final IBuilding m_building;
    private final ICampus m_campus;
    private final List<IRoom> m_rooms;
    
    public AddBuildingCommand(IBuilding building, ICampus campus)
    {
        m_building = building;
        m_campus = campus;
        
        m_rooms = new ArrayList<>();
        
        m_rooms.addAll(building.getRooms());
    }
    
    @Override
    public Boolean doCommand() 
    {
        if(m_building.getRooms().isEmpty())
        {
            m_rooms.stream().forEach((room) -> 
            {
                m_building.addRoom(room);
            });
        }
        
        return m_campus.addBuilding(m_building);
    }

    @Override
    public Boolean undoCommand() 
    {
        return m_campus.removeBuilding(m_building.getBuildingCode());
    }           
}
