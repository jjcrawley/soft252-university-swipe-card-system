/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package soft252gui.commands;

import command.interfaces.ICommandBehaviour;
import datamodel.campus.interfaces.IBuilding;
import datamodel.campus.rooms.interfaces.IRoom;

/**
 *
 * @author Joshua
 */
public class AddRoomCommand implements ICommandBehaviour
{
    private final IBuilding m_building;
    private final IRoom m_room;
    
    public AddRoomCommand(IBuilding building, IRoom room)
    {
        m_building = building;
        m_room = room;
    }
    
    @Override
    public Boolean doCommand() 
    {
        return m_building.addRoom(m_room);
    }

    @Override
    public Boolean undoCommand() 
    {
        return m_building.removeRoom(m_room.getRoomCode());
    }   
}
