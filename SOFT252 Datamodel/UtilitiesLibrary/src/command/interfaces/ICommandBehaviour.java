/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package command.interfaces;

/**
 * A simple Command Behaviour, use this to define functionality that allows 
 * undo and redoable actions
 * @author jjcrawley
 */
public interface ICommandBehaviour 
{

    /**
     * Do the command
     * @return true if it was successful, false otherwise
     */
    public Boolean doCommand();

    /**
     * Undo the command
     * @return true if it was successful, false otherwise
     */
    public Boolean undoCommand();
}
