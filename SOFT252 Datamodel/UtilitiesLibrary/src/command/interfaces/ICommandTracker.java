/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package command.interfaces;

/**
 * Defines standard behaviours for a command tracker
 * @author jjcrawley
 */
public interface ICommandTracker 
{

    /**
     * Execute a command and keep track of it
     * @param aCommand the command to execute
     * @return true if the execution was successful, false otherwise
     */
    Boolean executeCommand(ICommand aCommand);

    /**
     * Undo the last command 
     * @return the state of the undo
     */
    Boolean undoLastCommand();

    /**
     * Redo the last executed command in the tracker
     * @return the state of the redo
     */
    Boolean redoLastCommand();

    /**
     * Says whether the tracker has any undoable commands
     * @return true if it does, false otherwise
     */
    Boolean isUndoable();

    /**
     * Says whether the command tracker has any redoable actions
     * @return true if it does, false otherwise
     */
    Boolean isRedoable();

    /**
     * reset the state of the command tracker
     */
    void clearCommands();
}
