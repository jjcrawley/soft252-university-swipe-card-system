/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package command;

import command.interfaces.ICommandTracker;
import command.interfaces.ICommand;
import java.util.Stack;

/**
 * As simple command tracker object that allows you to keep track of done and undone commands
 * @author jjcrawley
 */
public class CommandTracker implements ICommandTracker
{
    private final Stack<ICommand> m_doneStack = new Stack<>();
    private final Stack<ICommand> m_unDoneStack = new Stack<>();
    
    /**
     * Create a new command tracker
     */
    public CommandTracker()
    {}   
    
    /**
     * Executes and keeps track of the passed in command
     * @param aCommand the command to execute and track
     * @return true if the command was successfully executed, false otherwise
     */
    @Override
    public Boolean executeCommand(ICommand aCommand) 
    {       
        if(aCommand != null)
        {   
            if(aCommand.doCommand())
            {               
                m_doneStack.push(aCommand);
                
                return true;
            }            
        }       
        
        return false;
    }

    /**
     * Undo the last command that was executed
     * @return true if the command was successfully undone, false otherwise
     */
    @Override
    public Boolean undoLastCommand() 
    {       
        if(isUndoable())
        {
            ICommand tempCommand = m_doneStack.pop();
            
            if(tempCommand.undoCommand())
            {           
                m_unDoneStack.push(tempCommand);
            
                return true;
            }
        }
        
        return false;
    }

    /**
     * Redo the last undone command
     * @return true if the redo was successful, false otherwise
     */
    @Override
    public Boolean redoLastCommand() 
    {        
        if(isRedoable())
        {            
            ICommand tempCommand = m_unDoneStack.pop();
            
            if(tempCommand.doCommand())
            {            
                m_doneStack.push(tempCommand);
                
                return true;
            }            
        }
        
        return false;
    }    

    /**
     * Checks whether the tracker is capable of undoing any command
     * @return true if it can undo an command, false otherwise
     */
    @Override
    public Boolean isUndoable() 
    {
        return !m_doneStack.isEmpty();
    }

    /**
     * Check whether the tracker is capable of redoing any commands
     * @return true if it can, false otherwise
     */
    @Override
    public Boolean isRedoable() 
    {
        return !m_unDoneStack.isEmpty();
    }  

    /**
     * Allows you to reset the state of the tracker
     */
    @Override
    public void clearCommands() 
    {
        m_doneStack.clear();
        m_unDoneStack.clear();
    }   
}
