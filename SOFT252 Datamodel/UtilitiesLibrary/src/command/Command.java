/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package command;

import command.interfaces.ICommand;
import command.interfaces.ICommandBehaviour;

/**
 * A simple command class for encapsulating a variety of different commands
 * @author jjcrawley
 */
public class Command implements ICommand
{
    private final ICommandBehaviour m_delegate;
    private Boolean b_executed = false;

    /**
     * Creates a command with the specified command behaviour
     * @param behaviour the command behaviour to wrap
     */
    public Command(ICommandBehaviour behaviour)
    {
        m_delegate = behaviour;
    }
    
    /**
     * Checks whether the command has been executed
     * @return the executed state, true if it's been executed, false otherwise
     */
    @Override
    public Boolean isExecuted() 
    {
        return b_executed;
    }

    /**
     * Set the executed state
     * @param flag the new executed state, can't be null
     * @return the new executed state
     */
    @Override
    public Boolean setExecuted(Boolean flag) 
    {
        if(flag != null)
            b_executed = flag;
        
        return b_executed;
    }

    /**
     * Returns the undone state of the command
     * @return true if it's been undone, false otherwise
     */
    @Override
    public Boolean isUndone() 
    {
        return !b_executed;                
    }

    /**
     * Do the command that has been wrapped
     * @return true if the command was executed, false otherwise
     */
    @Override
    public Boolean doCommand() 
    {
        Boolean done = m_delegate.doCommand();
              
        b_executed = done;
                
        return done;
    }

    /**
     * Undo the command 
     * @return true if the command was successfully undone, false otherwise
     */
    @Override
    public Boolean undoCommand() 
    {
        Boolean undone = m_delegate.undoCommand();
                
        b_executed = !undone;
        
        return undone;
    }    
}