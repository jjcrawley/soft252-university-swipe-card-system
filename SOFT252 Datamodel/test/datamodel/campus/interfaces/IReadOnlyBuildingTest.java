/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datamodel.campus.interfaces;

import datamodel.campus.rooms.interfaces.IReadOnlyRoom;
import datamodel.restrictions.OperatingMode;
import java.util.List;
import org.junit.*;

/**
 *
 * @author jjcrawley
 */
public class IReadOnlyBuildingTest {
    
    public IReadOnlyBuildingTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testViewRooms() {
    }

    @Test
    public void testGetBuildingName() {
    }

    @Test
    public void testGetBuildingCode() {
    }

    @Test
    public void testGetCurrentMode() {
    }

    public class IReadOnlyBuildingImpl implements IReadOnlyBuilding {

        public List<IReadOnlyRoom> viewRooms() {
            return null;
        }

        public String getBuildingName() {
            return "";
        }

        public String getBuildingCode() {
            return "";
        }

        public OperatingMode getCurrentMode() {
            return null;
        }

        @Override
        public void onRead()
        {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        @Override
        public void onWrite()
        {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }
    }    
}
