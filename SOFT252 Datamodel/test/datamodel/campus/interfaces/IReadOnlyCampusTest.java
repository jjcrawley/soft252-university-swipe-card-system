/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datamodel.campus.interfaces;

import datamodel.restrictions.OperatingMode;
import java.util.List;
import org.junit.*;

/**
 *
 * @author jjcrawley
 */
public class IReadOnlyCampusTest {
    
    public IReadOnlyCampusTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testGetCampusCode() {
    }

    @Test
    public void testViewBuildings() {
    }

    @Test
    public void testGetCampusMode() {
    }

    public class IReadOnlyCampusImpl implements IReadOnlyCampus {

        public String getCampusCode() {
            return "";
        }

        public List<IReadOnlyBuilding> viewBuildings() {
            return null;
        }

        public OperatingMode getCampusMode() {
            return null;
        }

        @Override
        public void onRead()
        {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        @Override
        public void onWrite()
        {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }
    }   
}
