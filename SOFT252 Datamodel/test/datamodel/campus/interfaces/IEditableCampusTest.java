/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datamodel.campus.interfaces;

import datamodel.restrictions.OperatingMode;
import java.util.List;
import org.junit.*;

/**
 *
 * @author jjcrawley
 */
public class IEditableCampusTest {
    
    public IEditableCampusTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testGetBuilding() {
    }

    @Test
    public void testSetCampusCode() {
    }

    @Test
    public void testGetBuildings() {
    }

    @Test
    public void testRefactorBuildingCode_String_String() {
    }

    @Test
    public void testRefactorBuildingCode_IBuilding_String() {
    }

    @Test
    public void testMoveBuildingToCampus() {
    }

    public class IEditableCampusImpl implements IEditableCampus {

        public IBuilding getBuilding(String buildingCode) {
            return null;
        }

        public void setCampusCode(String campusCode) {
        }

        public List<IBuilding> getBuildings() {
            return null;
        }

        public void refactorBuildingCode(String oldCode, String newCode) {
        }

        public void refactorBuildingCode(IBuilding toRefactor, String newCode) {
        }

        public boolean moveBuildingToCampus(ICampus destination, String buildingCode) {
            return false;
        }

        @Override
        public String getCampusCode() {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        @Override
        public List<IReadOnlyBuilding> viewBuildings() {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        @Override
        public OperatingMode getCampusMode() {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        @Override
        public void onRead()
        {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        @Override
        public void onWrite()
        {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }
    }    
}
