/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datamodel.campus.interfaces;

import datamodel.campus.rooms.interfaces.IReadOnlyRoom;
import datamodel.campus.rooms.interfaces.IEditableRoom;
import datamodel.campus.rooms.interfaces.IRoom;
import datamodel.restrictions.OperatingMode;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author jjcrawley
 */
public class IBuildingEditableTest {
    
    public IBuildingEditableTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testSetBuildingCode() {
    }

    @Test
    public void testSetBuildingName() {
    }

    @Test
    public void testGetRoom() {
    }

    @Test
    public void testGetRooms() {
    }

    @Test
    public void testRefactorRoomCode_IRoom_String() {
    }

    @Test
    public void testRefactorRoomCode_String_String() {
    }

    @Test
    public void testMoveRoomToBuilding() {
    }

    public class IBuildingEditableImpl implements IBuildingEditable {

        public void setBuildingCode(String buildingCode) {
        }

        public void setBuildingName(String buildingName) {
        }

        public IRoom getRoom(String roomCode) {
            return null;
        }

        public List<IRoom> getRooms() {
            return null;
        }

        public void refactorRoomCode(IEditableRoom toRefactor, String newRoomCode) {
        }

        public void refactorRoomCode(String oldCode, String newRoomCode) {
        }

        public void moveRoomToBuilding(String roomCode, IBuilding moveTo) {
        }

        @Override
        public List<IReadOnlyRoom> viewRooms() {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        @Override
        public String getBuildingName() {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        @Override
        public String getBuildingCode() {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        @Override
        public OperatingMode getCurrentMode() {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        @Override
        public void refactorRoomCode(IRoom toRefactor, String newRoomCode) {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }       

        @Override
        public void onRead() {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        @Override
        public void onWrite() {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }
    }    
}
