/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datamodel.campus;

import datamodel.campus.interfaces.ICampus;
import datamodel.users.UserBuilder;
import datamodel.users.interfaces.IUser;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author jjcrawley
 */
public class CentralServerTest {
    
    private CentralServer m_instance;
    
    public CentralServerTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() 
    {
        m_instance = CentralServer.getInstance();
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testGetCampuses() {
    }

    @Test
    public void testViewCampuses() {
    }

    @Test
    public void testGetUsers() {
    }

    @Test
    public void testViewUsers() {
    }

    @Test
    public void testAddUser() 
    {
        IUser testUser = UserBuilder.getUserBuilder().setUserID(20).setUserAge(21).buildUser();
        
        m_instance.addUser(testUser);
        
        assertEquals(1, m_instance.viewUsers().size());
        
        assertFalse(m_instance.addUser(testUser));
        
        assertNotNull(m_instance.getUser(testUser.viewUserID()));
        
        assertTrue(m_instance.removeUser(testUser.viewUserID()));
        
        assertEquals(0, m_instance.viewUsers().size());
    }

    @Test
    public void testRemoveUser() {
    }

    @Test
    public void testGetUser() {
    }

    @Test
    public void testGetCampus() {
    }

    @Test
    public void testAddCampus() 
    {
        ICampus testCampus = new Campus("campus");
        
        m_instance.addCampus(testCampus);
        
        assertEquals(1, m_instance.viewCampuses().size());
        
        assertFalse(m_instance.addCampus(testCampus));
        
        assertNotNull(m_instance.getCampus(testCampus.getCampusCode()));
        
        assertTrue(m_instance.removeCampus(testCampus.getCampusCode()));
        
        assertEquals(0, m_instance.viewCampuses().size());
    }

    @Test
    public void testRemoveCampus() {
    }

    @Test
    public void testLoadLogFile() throws Exception {
    }

    @Test
    public void testSaveData() throws Exception {
    }

    @Test
    public void testLoadInCampusAndUsers() {
    }

    @Test
    public void testGetInstance() {
    }

    @Test
    public void testDispose() {
    }

    @Test
    public void testRefactorCampus_String_ICampus() {
    }

    @Test
    public void testRefactorCampus_String_String() {
    }

    @Test
    public void testRegisterObserver() {
    }

    @Test
    public void testRemoveObserver() {
    }

    @Test
    public void testNotifyObservers() {
    }

    @Test
    public void testGenerateTestData() {
    }

    @Test
    public void testWriteLogsToTextFile() throws Exception {
    }

    @Test
    public void testWriteCampusLogToText() {
    }

    @Test
    public void testWriteBuildingLogToText() {
    }

    @Test
    public void testWriteRoomLogToText() {
    }

    @Test
    public void testLoadCampuses() {
    }

    @Test
    public void testLoadUsers() {
    }

    @Test
    public void testGetObservers() {
    }

    @Test
    public void testUpdate() {
    }
    
}
