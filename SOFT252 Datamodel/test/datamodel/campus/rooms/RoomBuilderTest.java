/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datamodel.campus.rooms;

import datamodel.campus.Building;
import datamodel.campus.interfaces.IBuilding;
import datamodel.campus.rooms.interfaces.IRoom;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author jjcrawley
 */
public class RoomBuilderTest 
{
    private RoomBuilder m_testBuilder;
    
    public RoomBuilderTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() 
    {
        m_testBuilder = RoomBuilder.getRoomBuilder();
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testSetRoomCode() 
    {
        String testCode = "BBG001";
        try
        {
            m_testBuilder.setRoomCode(testCode);
            
            IRoom testRoom = m_testBuilder.buildRoom();
            
            assertEquals("didn't assign room code", testCode, testRoom.getRoomCode());
            
            m_testBuilder.setRoomCode("");
            fail("room code can't be empty");            
        }
        catch(IllegalArgumentException ex)
        {
            System.out.println(ex.getMessage());
        }     
    }

    @Test
    public void testSetRoomFloor()
    {
        int testFloor = 01;
        try
        {
            m_testBuilder.setRoomFloor(testFloor);
            
            IRoom testRoom = m_testBuilder.buildRoom();
            
            assertEquals("didn't assign room floor", testFloor, testRoom.getFloorNum());
            
            m_testBuilder.setRoomFloor(-1);
            fail("room floor can't be less than zero");            
        }
        catch(IllegalArgumentException ex)
        {
            System.out.println(ex.getMessage());
        }         
    }

    @Test
    public void testSetRoomComments() {
    }

    @Test
    public void testSetBuilding()
    {
        IBuilding testBuilding = new Building("Babbage", "BBG");
        
        try
        {
            m_testBuilder.setBuilding(testBuilding);
            
            IRoom testRoom = m_testBuilder.buildRoom();
            
            assertEquals("didn't assign building", testBuilding.getBuildingCode(), testRoom.viewBuilding().getBuildingCode());
            
            m_testBuilder.setBuilding(null);
            fail("buidling can't be null");            
        }
        catch(IllegalArgumentException ex)
        {
            System.out.println(ex.getMessage());
        }  
    }

    @Test
    public void testSetRoomType() 
    {
        try
        {
            m_testBuilder.setRoomType(RoomType.LectureHall);
            
            IRoom testRoom = m_testBuilder.buildRoom();
            
            assertEquals("didn't assign room type", RoomType.LectureHall, testRoom.getRoomType());
            
            m_testBuilder.setRoomType(null);
            fail("room type can't be null");            
        }
        catch(IllegalArgumentException ex)
        {
            System.out.println(ex.getMessage());
        }  
        
    }

    @Test
    public void testBuildRoom_0args() {
    }

    @Test
    public void testBuildRoom_RoomType() {
    }

    @Test
    public void testGetRoomBuilder() {
    }

    @Test
    public void testGetRoomBuilder_0args() {
    }

    @Test
    public void testGetRoomBuilder_IBuilding() {
    }
    
}
