/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datamodel.campus.rooms;

import datamodel.campus.rooms.interfaces.InterfacesSuite;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 *
 * @author jjcrawley
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({RoomBuilderTest.class, SecureRoomTest.class, EmptyRoomTest.class, LectureHallTest.class, RoomTest.class, RoomFactoryTest.class, InterfacesSuite.class, StaffRoomTest.class, ResearchLabTest.class, RoomTypeTest.class, StudentLabTest.class})
public class RoomsSuite {

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }
    
}
