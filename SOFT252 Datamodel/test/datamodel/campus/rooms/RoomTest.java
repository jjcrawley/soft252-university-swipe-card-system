/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datamodel.campus.rooms;

import datamodel.campus.Building;
import datamodel.campus.rooms.interfaces.IRoom;
import datamodel.restrictions.OperatingMode;
import datamodel.users.Card;
import datamodel.users.UserBuilder;
import datamodel.users.UserRole;
import datamodel.users.interfaces.IUser;
import java.time.LocalTime;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author jjcrawley
 */
public class RoomTest 
{
    private RoomBuilder m_builder;
    private IRoom m_testRoom;
    
    public RoomTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() 
    {
        m_builder = RoomBuilder.getRoomBuilder(new Building("Babbage", "BBG"));
        m_testRoom = m_builder.setRoomCode("BBG001").setRoomComments("nothing to report").setRoomFloor(0).setRoomType(RoomType.StudentLab).buildRoom();
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testSetupDefaultRestrictions() {
    }

    @Test
    public void testGetBuilding() {
    }

    @Test
    public void testViewBuilding() {
    }

    @Test
    public void testGetRoomCode() {
    }

    @Test
    public void testSetRoomCode() {
    }

    @Test
    public void testGetFloorNum() {
    }

    @Test
    public void testSetFloorNum() {
    }

    @Test
    public void testGetMode() {
    }

    @Test
    public void testGetRoomType() {
    }

    @Test
    public void testGetComments() {
    }

    @Test
    public void testSetComments() {
    }

    @Test
    public void testGetLog() {
    }

    @Test
    public void testAddRestriction() {
    }

    @Test
    public void testAllowedAccess_IUser() 
    {
        
    }

    @Test
    public void testAllowedAccess_IUser_LocalTime() 
    {
        IUser guestTest = UserBuilder.getUserBuilder().setUserRole(UserRole.Guest).setUserCard(new Card(1111)).buildUser();
        IUser studentTest = UserBuilder.getUserBuilder().setUserRole(UserRole.Student).setUserCard(new Card(1111)).buildUser();
        IUser managerTest = UserBuilder.getUserBuilder().setUserRole(UserRole.Manager).setUserCard(new Card(1111)).buildUser();
        IUser securityTest = UserBuilder.getUserBuilder().setUserRole(UserRole.Security).setUserCard(new Card(1111)).buildUser();
        
        assertFalse("failed to restrict: ", m_testRoom.allowedAccess(guestTest));
        assertTrue("failed to restrict: ", m_testRoom.allowedAccess(studentTest, LocalTime.of(13, 00)));
        assertTrue("faild to restrict: ", m_testRoom.allowedAccess(managerTest));
        assertTrue("faild to restrict: ", m_testRoom.allowedAccess(securityTest));
        
        m_testRoom.changeMode(OperatingMode.Emergency);
        assertFalse("failed to restrict: ", m_testRoom.allowedAccess(studentTest, LocalTime.of(13, 00)));   
        assertTrue("faild to restrict: ", m_testRoom.allowedAccess(securityTest));
    }

    @Test
    public void testSwipeCard()
    {
        IUser guestTest = UserBuilder.getUserBuilder().setUserRole(UserRole.Guest).setUserCard(new Card(1111)).buildUser();
        IUser studentTest = UserBuilder.getUserBuilder().setUserRole(UserRole.Student).setUserCard(new Card(2222)).buildUser();
        IUser managerTest = UserBuilder.getUserBuilder().setUserRole(UserRole.Manager).setUserCard(new Card(3333)).buildUser();
        IUser securityTest = UserBuilder.getUserBuilder().setUserRole(UserRole.Security).setUserCard(new Card(4444)).buildUser();
        
        m_testRoom.swipeCard(guestTest.getUserCard());
        m_testRoom.swipeCard(studentTest.getUserCard());
        assertEquals("didn't log correctly: ", 2, m_testRoom.getLog().viewEntries().size());
               
        
        m_testRoom.changeMode(OperatingMode.Emergency);
        
        m_testRoom.swipeCard(guestTest.getUserCard());
        m_testRoom.swipeCard(managerTest.getUserCard());        
        m_testRoom.swipeCard(securityTest.getUserCard()); 
        
        m_testRoom.getLog().resetLogger();
        
        m_testRoom.getLog().viewEntries().stream().forEach((currentEntry) -> 
        {
            System.out.println(currentEntry.toString());
        });        
    }

    @Test
    public void testLogSwipeCard() 
    {
       
    }

    @Test
    public void testChangeMode() {
    }

    @Test
    public void testLogModeChange_0args() 
    {
        m_testRoom.logModeChange();
        m_testRoom.logModeChange("fire drill");
        
        m_testRoom.getLog().viewEntries().stream().forEach((currentEntry) -> 
        {
            System.out.println(currentEntry.toString());
        });   
    }

    @Test
    public void testLogModeChange_String_String() {
    }

    @Test
    public void testToString() {
    }

    public class RoomImpl extends Room {

        public void setupDefaultRestrictions() {
        }

        public RoomType getRoomType() {
            return null;
        }
    }   

    @Test
    public void testRegisterObserver() {
    }

    @Test
    public void testRemoveObserver() {
    }

    @Test
    public void testNotifyObservers() {
    }    

    @Test
    public void testSetBuilding() {
    }

    @Test
    public void testAllowedAccess_IReadOnlyUser() {
    }

    @Test
    public void testAllowedAccess_IReadOnlyUser_LocalTime() {
    }

    @Test
    public void testLogModeChange_String() {
    }

    @Test
    public void testGetObservers() {
    }

    @Test
    public void testOnRead() {
    }

    @Test
    public void testOnWrite() {
    }   
}
