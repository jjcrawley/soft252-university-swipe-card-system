/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datamodel.campus.rooms.interfaces;

import datamodel.campus.interfaces.IBuilding;
import datamodel.campus.interfaces.IReadOnlyBuilding;
import datamodel.campus.rooms.RoomType;
import datamodel.logging.Logger;
import datamodel.restrictions.OperatingMode;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author jjcrawley
 */
public class IRoomTest {
    
    public IRoomTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testGetBuilding() {
    }

    @Test
    public void testSetRoomCode() {
    }

    @Test
    public void testSetFloorNum() {
    }

    @Test
    public void testGetRoomType() {
    }

    @Test
    public void testSetComments() {
    }

    @Test
    public void testGetLog() {
    }

    public class IRoomImpl implements IEditableRoom {

        public IBuilding getBuilding() {
            return null;
        }

        public void setRoomCode(String roomCode) {
        }

        public void setFloorNum(int floorNum) {
        }

        public RoomType getRoomType() {
            return null;
        }

        public void setComments(String comments) {
        }

        public Logger getLog() {
            return null;
        }

        @Override
        public String getRoomCode() {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        @Override
        public int getFloorNum() {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        @Override
        public OperatingMode getMode() {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        @Override
        public String getComments() {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        @Override
        public IReadOnlyBuilding viewBuilding() {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        @Override
        public void setBuilding(IBuilding newBuilding) {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        @Override
        public void onRead() {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        @Override
        public void onWrite() {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }
    }

    
    
}
