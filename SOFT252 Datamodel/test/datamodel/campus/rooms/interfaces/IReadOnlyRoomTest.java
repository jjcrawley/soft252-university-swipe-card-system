/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datamodel.campus.rooms.interfaces;

import datamodel.campus.interfaces.IReadOnlyBuilding;
import datamodel.restrictions.OperatingMode;
import org.junit.*;

/**
 *
 * @author jjcrawley
 */
public class IReadOnlyRoomTest {
    
    public IReadOnlyRoomTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testGetRoomCode() {
    }

    @Test
    public void testGetFloorNum() {
    }

    @Test
    public void testGetMode() {
    }

    @Test
    public void testGetComments() {
    }

    @Test
    public void testViewBuilding() {
    }

    public class IReadOnlyRoomImpl implements IReadOnlyRoom {

        public String getRoomCode() {
            return "";
        }

        public int getFloorNum() {
            return 0;
        }

        public OperatingMode getMode() {
            return null;
        }

        public String getComments() {
            return "";
        }

        public IReadOnlyBuilding viewBuilding() {
            return null;
        }

        @Override
        public void onRead()
        {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        @Override
        public void onWrite()
        {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }
    }  

    
}
