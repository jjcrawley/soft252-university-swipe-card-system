/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datamodel.campus.rooms.interfaces;

import datamodel.restrictions.OperatingMode;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author jjcrawley
 */
public class IModeChangeableTest {
    
    public IModeChangeableTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testChangeMode() {
    }

    @Test
    public void testLogModeChange_0args() {
    }

    @Test
    public void testLogModeChange_String_String() {
    }

    public class IModeChangeableImpl implements IModeChangeable {

        public void changeMode(OperatingMode changeTo) {
        }

        public void logModeChange() {
        }

        public void logModeChange(String reason, String comment) {
        }

        @Override
        public void logModeChange(String reason) {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }
    }    

    @Test
    public void testLogModeChange_String() {
    }

    
}
