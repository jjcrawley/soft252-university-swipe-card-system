/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datamodel.campus;

import datamodel.campus.interfaces.IBuilding;
import datamodel.campus.rooms.RoomBuilder;
import datamodel.campus.rooms.RoomType;
import datamodel.campus.rooms.interfaces.IRoom;
import datamodel.restrictions.OperatingMode;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author jjcrawley
 */
public class BuildingTest 
{
    private IBuilding m_testBuilding;
    private IBuilding m_emptyBuilding;
    
    public BuildingTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() 
    {
        m_testBuilding = new Building("Babbage", "BBG");
        m_emptyBuilding = new Building();
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testViewRooms() {
    }

    @Test
    public void testGetRooms() {
    }

    @Test
    public void testGetBuildingName() {
    }

    @Test
    public void testSetBuildingName() 
    {
        String newName = "Smeaton";
        
        m_testBuilding.setBuildingName(newName);
        
        assertEquals("testing building name setter: ", newName, m_testBuilding.getBuildingName());
        
        try
        {
            m_testBuilding.setBuildingName("");
            fail("can't assign an empty name");
        }
        catch(IllegalArgumentException ex)
        {
            System.out.println(ex.getMessage());
        }        
    }

    @Test
    public void testGetBuildingCode() {
    }

    @Test
    public void testSetBuildingCode() 
    {
        String newCode = "SMT";
        
        m_testBuilding.setBuildingCode(newCode);
        
        assertEquals("testing building code setter: ", newCode, m_testBuilding.getBuildingCode());
        
        try
        {
            m_testBuilding.setBuildingCode("");
            fail("can't assign an empty code");
        }
        catch(IllegalArgumentException ex)
        {
            System.out.println(ex.getMessage());
        }
    }

    @Test
    public void testGetCurrentMode() {
    }

    @Test
    public void testGetLog() {
    }

    @Test
    public void testGetRoomLogs() {
    }

    @Test
    public void testAddRoom() 
    {
        IRoom testRoom = RoomBuilder.getRoomBuilder(m_testBuilding).setBuilding(m_testBuilding).setRoomCode("SMT001").setRoomFloor(0).buildRoom(RoomType.StudentLab);
        
        try
        {
            m_testBuilding.addRoom(testRoom);
            assertFalse(m_testBuilding.addRoom(testRoom));
            m_testBuilding.addRoom(null);
            m_testBuilding.removeRoom(testRoom.getRoomCode());
            
            assertEquals("testing remove", 0, m_testBuilding.getRooms().size());
        }
        catch(IllegalArgumentException ex)
        {
            System.out.println(ex.getMessage());
        }                
    }

    @Test
    public void testRemoveRoom() 
    {       
        assertFalse("testing remove with empty string: ", m_testBuilding.removeRoom(""));
        assertFalse("testing remove with no room in building", m_testBuilding.removeRoom("jhj"));
    }

    @Test
    public void testMoveRoomToBuilding()
    {
        IRoom testRoom = RoomBuilder.getRoomBuilder(m_testBuilding).setBuilding(m_testBuilding).setRoomCode("SMT001").setRoomFloor(0).buildRoom(RoomType.StudentLab);
        
        m_testBuilding.addRoom(testRoom);
        
        m_testBuilding.moveRoomToBuilding(testRoom.getRoomCode(), m_emptyBuilding);
        
        System.out.println("testing move room to building");
        assertEquals("testing move room to building: ", 1, m_emptyBuilding.getRooms().size());
        m_emptyBuilding.removeRoom(testRoom.getRoomCode());
    }

    @Test
    public void testRefactorRoomCode_IRoom_String() 
    {
        IRoom testRoom = RoomBuilder.getRoomBuilder(m_testBuilding).setBuilding(m_testBuilding).setRoomCode("SMT001").setRoomFloor(0).buildRoom(RoomType.StudentLab);
        
        try
        {
            String newCode = "BBG210";
            m_testBuilding.addRoom(testRoom);
            m_testBuilding.refactorRoomCode(testRoom, newCode);
            assertEquals("didn't refactor correctly: ", newCode, m_testBuilding.getRoom(newCode).getRoomCode());
            m_testBuilding.removeRoom(newCode);
            
            m_testBuilding.refactorRoomCode("", newCode);
        }
        catch(IllegalArgumentException ex)
        {
            System.out.println(ex.getMessage());
        }
    }

    @Test
    public void testRefactorRoomCode_String_String() 
    {
    }

    @Test
    public void testGetRoom() {
    }

    @Test
    public void testChangeMode() 
    {
        m_testBuilding.changeMode(OperatingMode.Emergency);
        
        assertEquals("didn't change mode properly: ", OperatingMode.Emergency, m_testBuilding.getCurrentMode());
    }

    @Test
    public void testLogModeChange_0args() 
    {
        m_testBuilding.logModeChange();
        assertEquals("didn't log the mode change: ", 1, m_testBuilding.getLog().viewEntries().size());
    }

    @Test
    public void testLogModeChange_String_String() {
    }

    @Test
    public void testToString() {
    }

    @Test
    public void testRegisterObserver() {
    }

    @Test
    public void testRemoveObserver() {
    }

    @Test
    public void testNotifyObservers() {
    }

    @Test
    public void testClearBuilding() 
    {
        m_testBuilding.clearBuilding();
        
        assertTrue(m_testBuilding.getRooms().isEmpty());
    }

    @Test
    public void testLogModeChange_String() {
    }

    @Test
    public void testGetObservers() {
    }

    @Test
    public void testOnRead() {
    }

    @Test
    public void testOnWrite() {
    }
    
}
