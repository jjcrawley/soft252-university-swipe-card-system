/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datamodel.campus;

import datamodel.campus.interfaces.IBuilding;
import datamodel.campus.interfaces.ICampus;
import datamodel.restrictions.OperatingMode;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author jjcrawley
 */
public class CampusTest 
{
    private ICampus m_emtpyCampus;
    private ICampus m_testCampus;
    
    public CampusTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() 
    {
        m_emtpyCampus = new Campus("campus2");
        m_testCampus = new Campus("campus");
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testGetCampusCode() 
    {
        
    }

    @Test
    public void testSetCampusCode() 
    {
        try
        {
            String campusCode = "newCode";
        
            m_testCampus.setCampusCode(campusCode);
        
            assertEquals("didn't assign properly: ", campusCode, m_testCampus.getCampusCode());
            
            m_testCampus.setCampusCode("");
            fail("can't assign an empty campus code");
        }
        catch(IllegalArgumentException ex)
        {
            //System.out.println(ex.getMessage());
        }
    }

    @Test
    public void testViewBuildings() {
    }

    @Test
    public void testGetBuildings() {
    }

    @Test
    public void testGetCampusMode() {
    }

    @Test
    public void testGetLog() {
    }

    @Test
    public void testGetBuildingLogs() {
    }

    @Test
    public void testGetRoomLogs() {
    }

    @Test
    public void testAddBuilding() 
    {
        IBuilding testBuilding = new Building();
        testBuilding.setBuildingCode("newCode");
        
        try
        {            
            assertTrue(m_testCampus.addBuilding(testBuilding)); 
            //assertEquals(1, m_testCampus.getBuildings().size());
            assertTrue(m_testCampus.removeBuilding(testBuilding.getBuildingCode()));
            
            m_testCampus.addBuilding(null);
            fail("can't assign a null building");
        }
        catch(IllegalArgumentException ex)
        {
            //System.out.println(ex.getMessage());
        }
    }

    @Test
    public void testRemoveBuilding() {
    }

    @Test
    public void testRefactorBuildingCode_String_String() 
    {
         IBuilding testBuilding = new Building("Babbage", "BBG");    
         
         m_testCampus.addBuilding(testBuilding);
         
         m_testCampus.refactorBuildingCode(testBuilding, "BGB");
         
         assertFalse(m_testCampus.getBuilding(testBuilding.getBuildingCode()) == null);
    }

    @Test
    public void testRefactorBuildingCode_IBuilding_String() {
    }

    @Test
    public void testMoveBuildingToCampus() 
    {
        try
        {
            IBuilding testBuilding = new Building("Babbage", "BBG");  

            //System.out.println(testBuilding.getBuildingCode());
            m_testCampus.addBuilding(testBuilding);
                       
            System.out.println(m_testCampus.moveBuildingToCampus(m_emtpyCampus, testBuilding.getBuildingCode()));

            //System.out.println(m_emtpyCampus.getBuildings().size() + " " + m_testCampus.getBuildings().size());
            assertEquals("didn't move the building: ", 1, m_emtpyCampus.getBuildings().size());
            
            m_emtpyCampus.removeBuilding(testBuilding.getBuildingCode());
        }
        catch(IllegalArgumentException ex)
        {
            System.out.println(ex.getMessage());
        }
    }

    @Test
    public void testGetBuilding() {
    }

    @Test
    public void testChangeMode() 
    {
        try
        {
            m_testCampus.changeMode(OperatingMode.Emergency);
            assertEquals(OperatingMode.Emergency, m_testCampus.getCampusMode());
            m_testCampus.changeMode(OperatingMode.Normal);
            m_testCampus.changeMode(null);
        }
        catch(IllegalArgumentException ex)
        {
            System.out.println(ex.getMessage());
        }
    }

    @Test
    public void testLogModeChange_0args() 
    {
        m_testCampus.logModeChange();
        assertEquals(1, m_testCampus.getLog().viewEntries().size());
    }

    @Test
    public void testLogModeChange_String_String() {
    }

    @Test
    public void testToString() {
    }

    @Test
    public void testRegisterObserver() {
    }

    @Test
    public void testRemoveObserver() {
    }

    @Test
    public void testNotifyObservers() {
    }

    @Test
    public void testClearCampus() 
    {
        m_testCampus.clearCampus();
        
        m_testCampus.getBuildings().forEach((building) -> 
        {
            assertTrue(building.getRooms().isEmpty());
        });
    }

    @Test
    public void testLogModeChange_String() {
    }

    @Test
    public void testGetObservers() {
    }

    @Test
    public void testOnRead() {
    }

    @Test
    public void testOnWrite() {
    }
    
}
