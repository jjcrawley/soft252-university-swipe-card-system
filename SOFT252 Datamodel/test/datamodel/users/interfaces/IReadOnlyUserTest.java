/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datamodel.users.interfaces;

import datamodel.details.interfaces.IReadOnlyAddress;
import datamodel.users.Gender;
import datamodel.users.UserRole;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author jjcrawley
 */
public class IReadOnlyUserTest {
    
    public IReadOnlyUserTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testViewUserID() {
    }

    @Test
    public void testViewRole() {
    }

    @Test
    public void testViewUserName() {
    }

    @Test
    public void testViewUserAddress() {
    }

    @Test
    public void testViewGender() {
    }

    public class IReadOnlyUserImpl implements IReadOnlyUser {

        public int viewUserID() {
            return 0;
        }

        public UserRole viewRole() {
            return null;
        }

        public IReadOnlyName viewUserName() {
            return null;
        }

        public IReadOnlyAddress viewUserAddress() {
            return null;
        }

        public Gender viewGender() {
            return null;
        }

        @Override
        public int viewAge() {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        @Override
        public IReadOnlyCard viewCard() {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }
    }

    @Test
    public void testViewAge() {
    }     

    @Test
    public void testViewCard() {
    }    
}
