/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datamodel.users.interfaces;

import datamodel.users.UserRole;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author jjcrawley
 */
public class ICardTest {
    
    public ICardTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testGetCardRoles() {
    }

    @Test
    public void testAddRole() {
    }

    @Test
    public void testRemoveRole() {
    }

    @Test
    public void testGetCardID() {
    }

    @Test
    public void testGetUser() {
    }

    public class ICardImpl implements ICard {

        

        public Boolean addRole(UserRole toAdd) {
            return null;
        }

        public boolean removeRole(UserRole toRemove) {
            return false;
        }

        public int viewCardID() {
            return 0;
        }

        public IUser viewUser() {
            return null;
        }

        @Override
        public boolean hasUserRole(UserRole toCheck) {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        @Override
        public void setUser(IUser user) {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        @Override
        public List<UserRole> viewCardRoles() {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }
    }     

    @Test
    public void testSetUser() {
    }    
}
