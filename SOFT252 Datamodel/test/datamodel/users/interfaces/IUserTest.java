/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datamodel.users.interfaces;

import datamodel.details.interfaces.IAddress;
import datamodel.details.interfaces.IReadOnlyAddress;
import datamodel.users.Card;
import datamodel.users.Gender;
import datamodel.users.UserRole;
import org.junit.*;

/**
 *
 * @author jjcrawley
 */
public class IUserTest {
    
    public IUserTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testAssignCard() {
    }

    @Test
    public void testGetUserCard() {
    }

    @Test
    public void testGetUserName() {
    }

    @Test
    public void testGetAddressDetails() {
    }

    public class IUserImpl implements IUser {

        public void assignCard(Card toAssign) {
        }

        public ICard getUserCard() {
            return null;
        }

        public IName getUserName() {
            return null;
        }

        public IAddress getAddressDetails() {
            return null;
        }

        @Override
        public int viewUserID() {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        @Override
        public UserRole viewRole() {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        @Override
        public IReadOnlyName viewUserName() {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        @Override
        public IReadOnlyAddress viewUserAddress() {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        @Override
        public Gender viewGender() {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        @Override
        public void setAge(int newAge) {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        @Override
        public int viewAge() {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        @Override
        public void assignCard(ICard toAssign) {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        @Override
        public IReadOnlyCard viewCard() {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        @Override
        public void setGender(Gender newGender) throws IllegalArgumentException {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        @Override
        public void clearUser()
        {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }
    }

    @Test
    public void testSetAge() {
    }     

    @Test
    public void testSetGender() {
    }

    @Test
    public void testClearUser() {
    }
}
