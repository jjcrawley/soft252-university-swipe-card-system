/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datamodel.users.interfaces;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author jjcrawley
 */
public class IReadOnlyNameTest {
    
    public IReadOnlyNameTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testGetFirstName() {
    }

    @Test
    public void testGetSurname() {
    }

    @Test
    public void testGetMiddleName() {
    }

    @Test
    public void testGetFullName() {
    }

    public class IReadOnlyNameImpl implements IReadOnlyName {

        public String getFirstName() {
            return "";
        }

        public String getSurname() {
            return "";
        }

        public String getMiddleName() {
            return "";
        }

        public String getFullName() {
            return "";
        }
    }      
}
