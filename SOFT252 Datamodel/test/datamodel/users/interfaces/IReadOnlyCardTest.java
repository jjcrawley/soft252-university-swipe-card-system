/*
 * SOFT252 Assignment
 * Author Joshua Crawley
 * Student ID: 10469886
 */
package datamodel.users.interfaces;

import datamodel.users.UserRole;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author jjcrawley
 */
public class IReadOnlyCardTest {
    
    public IReadOnlyCardTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testViewCardID() {
    }

    @Test
    public void testViewUser() {
    }

    @Test
    public void testHasUserRole() {
    }

    @Test
    public void testViewCardRoles() {
    }

    public class IReadOnlyCardImpl implements IReadOnlyCard {

        public int viewCardID() {
            return 0;
        }

        public IReadOnlyUser viewUser() {
            return null;
        }

        public boolean hasUserRole(UserRole toCheck) {
            return false;
        }

        public List<UserRole> viewCardRoles() {
            return null;
        }
    }
    
}
