/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datamodel.users;

import datamodel.users.interfaces.ICard;
import datamodel.users.interfaces.IUser;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author jjcrawley
 */
public class UserTest 
{    
    private IUser m_testUser;
    
    public UserTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() 
    {
        m_testUser = UserBuilder.getUserBuilder().buildUser(UserRole.Guest);
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testGetAddressDetails() 
    {
        String newLine1 = "random";
        
        m_testUser.getAddressDetails().setAddressLine1(newLine1);
        
        assertEquals(newLine1, m_testUser.getAddressDetails().getAddressLine1());
    }

    @Test
    public void testViewUserAddress() {
    }

    @Test
    public void testViewGender() {
    }

    @Test
    public void testGetUserName() 
    {
        String newFirstName = "JJ";
        
        m_testUser.getUserName().setFirstName(newFirstName);
        
        assertEquals(newFirstName, m_testUser.viewUserName().getFirstName());
    }

    @Test
    public void testViewUserName() {
    }

    @Test
    public void testAssignCard() 
    {
        ICard testCard = new Card(111);
        
        m_testUser.assignCard(testCard);
        
        assertEquals(111, m_testUser.viewCard().viewCardID());
    }

    @Test
    public void testGetUserCard() 
    {
        
    }

    @Test
    public void testViewUserID() {
    }

    @Test
    public void testViewRole() {
    }

    @Test
    public void testGetUserBuilder() {
    }

    @Test
    public void testGetUserFactory() {
    }

    public class UserImpl extends User {

        public UserRole viewRole() {
            return null;
        }
    }

    @Test
    public void testViewAge() {
    }

    @Test
    public void testSetAge() 
    {
        int testAge = 25;
        
        m_testUser.setAge(testAge);
        
        assertEquals(testAge, m_testUser.viewAge());
    }    

    @Test
    public void testSetGender() 
    {
        m_testUser.setGender(Gender.Female);
        
        assertEquals(Gender.Female, m_testUser.viewGender());
    }

    @Test
    public void testViewCard() {
    }

    @Test
    public void testClearUser() 
    {
        m_testUser.clearUser();
        
        assertNull(m_testUser.getUserCard());
    }

    @Test
    public void testToString() {
    }   
}
