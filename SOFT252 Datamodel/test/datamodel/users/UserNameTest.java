/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datamodel.users;

import datamodel.users.interfaces.IName;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author jjcrawley
 */
public class UserNameTest 
{
    private IName m_testName;
    private IName m_emptyName;
    
    public UserNameTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp()
    {
        m_emptyName = new UserName();
        m_testName = new UserName("Jimmy", "Jam");
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testGetFirstName() {
    }

    @Test
    public void testGetSurname() {
    }

    @Test
    public void testGetMiddleName() {
    }

    @Test
    public void testGetFullName() {
    }

    @Test
    public void testSetFirstName() 
    {
        String newFirstName = "James";
        
        try
        {
            m_testName.setFirstName(newFirstName);
            assertEquals(newFirstName, m_testName.getFirstName());
            
            m_emptyName.setFirstName("");
        }
        catch(IllegalArgumentException ex)
        {
            System.out.println(ex.getMessage());
        }               
    }

    @Test
    public void testSetMiddleName() 
    {
        String newSecondName = "John";
        
        try
        {
            m_testName.setMiddleName(newSecondName);
            assertEquals(newSecondName, m_testName.getMiddleName());
            
            m_emptyName.setMiddleName("");
        }
        catch(IllegalArgumentException ex)
        {
            System.out.println(ex.getMessage());
        }   
    }

    @Test
    public void testSetSurname() 
    {
        String newSurname = "Smith";
        
        try
        {
            m_testName.setSurname(newSurname);
            assertEquals(newSurname, m_testName.getSurname());
            
            m_emptyName.setSurname("");
        }
        catch(IllegalArgumentException ex)
        {
            System.out.println(ex.getMessage());
        }   
    }   
}
