/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datamodel.users;

import datamodel.users.interfaces.ICard;
import datamodel.users.interfaces.IUser;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author jjcrawley
 */
public class CardTest 
{
    private ICard m_testCard;
    
    public CardTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() 
    {
        m_testCard = new Card(25);
    }
    
    @After
    public void tearDown() {
    }   

    @Test
    public void testAddRole() 
    {      
        assertTrue("didn't add the role properly", m_testCard.addRole(UserRole.Guest));
        assertFalse("mustn't have the same role type on the card", m_testCard.addRole(UserRole.Guest));
        //System.out.println(m_testCard.hasUserRole(UserRole.Guest));
        assertFalse(m_testCard.addRole(null));      
    }

    @Test
    public void testRemoveRole() 
    {
        assertTrue("didn't add the role properly", m_testCard.addRole(UserRole.Guest));
        //System.out.println(m_testCard.hasUserRole(UserRole.Guest));
        assertTrue("can't remove a role that doesn't exist", m_testCard.removeRole(UserRole.Guest));       
        assertFalse(m_testCard.removeRole(UserRole.Guest));
        
        IUser testUser = UserBuilder.getUserBuilder().buildUser(UserRole.Guest);
        
        m_testCard.setUser(testUser);
        
        assertFalse(m_testCard.removeRole(UserRole.Guest));
    }

    @Test
    public void testGetCardID() {
    }

    @Test
    public void testGetUser() {
    }

    @Test
    public void testToString() {
    }

    @Test
    public void testHasUserRole() 
    {
        assertTrue("didn't add the role properly", m_testCard.addRole(UserRole.Guest));
        assertTrue(m_testCard.hasUserRole(UserRole.Guest));
        assertFalse(m_testCard.hasUserRole(UserRole.EmergencyResponder));
    }

    @Test
    public void testSetUser()
    {
        m_testCard.setUser(UserBuilder.getUserBuilder().buildUser());
        assertNotNull(m_testCard.viewUser());
    }    

    @Test
    public void testViewCardID() 
    {
        assertEquals("didn't create properly", 25, m_testCard.viewCardID());
    }

    @Test
    public void testViewUser() {
    }

    @Test
    public void testViewCardRoles() {
    }
}
