/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datamodel.users;

import datamodel.users.interfaces.InterfacesSuite;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 *
 * @author jjcrawley
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({ManagerTest.class, StudentTest.class, UserFactoryTest.class, ContractCleanerTest.class, GenderTest.class, InterfacesSuite.class, EmergencyResponderTest.class, UserRoleTest.class, SecurityTest.class, GuestTest.class, StaffTest.class, CardTest.class, UserBuilderTest.class, UserTest.class, EmptyUserTest.class, UserNameTest.class})
public class UsersSuite {

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }
    
}
