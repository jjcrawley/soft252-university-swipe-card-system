/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datamodel.users;

import datamodel.details.AddressBuilder;
import datamodel.details.interfaces.IAddress;
import datamodel.users.interfaces.ICard;
import datamodel.users.interfaces.IName;
import datamodel.users.interfaces.IUser;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author jjcrawley
 */
public class UserBuilderTest 
{
    private IAddress m_testAddress;   
    private IName m_userName;
    private Gender m_userGender;    
    private UserBuilder m_testBuilder;
    
    public UserBuilderTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() 
    {
        m_testAddress = AddressBuilder.getAddressBuilder().setAddressLine1("blah").setAddressLine2("blah").setCity("here").setCounty("county").setPostCode("apostcode").buildAddress();
        m_userName = new UserName("Jimmy", "Jam");
        m_userGender = Gender.Male;
        m_testBuilder = UserBuilder.getUserBuilder();
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testSetUserName() 
    {
        try
        {
            m_testBuilder.setUserName(m_userName);

            IUser user = m_testBuilder.buildUser();

            assertEquals("failed to set user name", m_userName.getFirstName(), user.getUserName().getFirstName());
            
            m_testBuilder.setUserName(null);
            fail("user name can't be null");
        }
        catch(IllegalArgumentException ex)
        {
            System.out.println(ex.getMessage());
        }
    }

    @Test
    public void testSetUserAddress() 
    {
        try
        {
            m_testBuilder.setUserAddress(m_testAddress);

            IUser user = m_testBuilder.buildUser();

            assertEquals("failed to set user address", m_testAddress.getAddressLine1(), user.getAddressDetails().getAddressLine1());
            
            m_testBuilder.setUserAddress(null);
            fail("user address can't be null");
        }
        catch(IllegalArgumentException ex)
        {
            System.out.println(ex.getMessage());
        }        
    }

    @Test
    public void testSetUserGender() 
    {
        try
        {
            m_testBuilder.setUserGender(m_userGender);

            IUser user = m_testBuilder.buildUser();

            assertEquals("failed to set user gender", m_userGender, user.viewGender());
            
            m_testBuilder.setUserGender(null);
            fail("user gender can't be null");
        }
        catch(IllegalArgumentException ex)
        {
            System.out.println(ex.getMessage());
        }     
    }

    @Test
    public void testSetUserID() 
    {
        int testId = 25;
        try
        {
            m_testBuilder.setUserID(testId);

            IUser user = m_testBuilder.buildUser();

            assertEquals("failed to set user address", testId, user.viewUserID());
            
            m_testBuilder.setUserID(-20);
            fail("user id can't be negative");
        }
        catch(IllegalArgumentException ex)
        {
            System.out.println(ex.getMessage());
        }     
    }

    @Test
    public void testSetUserCard()
    {
        ICard testCard = new Card(25);
        try
        {
            m_testBuilder.setUserCard(testCard);

            IUser user = m_testBuilder.buildUser();

            assertEquals("failed to set user card", testCard.viewCardID(), user.getUserCard().viewCardID());
            
            m_testBuilder.setUserCard(null);
            fail("user card can't be null");
        }
        catch(IllegalArgumentException ex)
        {
            System.out.println(ex.getMessage());
        }     
    }

    @Test
    public void testSetUserRole() 
    {
        try
        {
            m_testBuilder.setUserRole(UserRole.ContractCleaner);

            IUser user = m_testBuilder.buildUser();

            assertEquals("failed to set user address", user.viewRole(), UserRole.ContractCleaner);
            
            m_testBuilder.setUserRole(null);
            fail("user role can't be null");
        }
        catch(IllegalArgumentException ex)
        {
            System.out.println(ex.getMessage());
        }     
    }

    @Test
    public void testGetUserBuilder() {
    }

    @Test
    public void testBuildUser_0args() {
    }

    @Test
    public void testBuildUser_UserRole() {
    }

    @Test
    public void testSetUserAge() {
    }
    
}
