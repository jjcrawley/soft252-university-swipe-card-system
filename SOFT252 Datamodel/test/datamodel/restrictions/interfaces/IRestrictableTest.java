/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datamodel.restrictions.interfaces;

import datamodel.logging.Access;
import datamodel.users.interfaces.ICard;
import datamodel.users.interfaces.IReadOnlyUser;
import datamodel.users.interfaces.IUser;
import java.time.LocalTime;
import org.junit.*;

/**
 *
 * @author jjcrawley
 */
public class IRestrictableTest {
    
    public IRestrictableTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testAllowedAccess_IUser() {
    }

    @Test
    public void testAllowedAccess_IUser_LocalTime() {
    }

    @Test
    public void testSwipeCard() {
    }

    @Test
    public void testLogSwipeCard() {
    }

    public class IRestrictableImpl implements IRestrictable {

        public Boolean allowedAccess(IUser toCheck) {
            return null;
        }

        public Boolean allowedAccess(IUser toCheck, LocalTime timeToCheck) {
            return null;
        }

        public Access swipeCard(ICard swipeCard) {
            return null;
        }

        public void logSwipeCard(ICard swipeCard, Access accessed) {
        }

        @Override
        public Boolean allowedAccess(IReadOnlyUser toCheck) {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        @Override
        public Boolean allowedAccess(IReadOnlyUser toCheck, LocalTime timeToCheck) {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }
    }    

    @Test
    public void testAllowedAccess_IReadOnlyUser() {
    }

    @Test
    public void testAllowedAccess_IReadOnlyUser_LocalTime() {
    }    
}
