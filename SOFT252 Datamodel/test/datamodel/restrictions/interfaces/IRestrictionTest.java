/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datamodel.restrictions.interfaces;

import datamodel.restrictions.OperatingMode;
import datamodel.restrictions.TimePeriod;
import datamodel.users.UserRole;
import datamodel.users.interfaces.IReadOnlyUser;
import datamodel.users.interfaces.IUser;
import java.time.LocalTime;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author jjcrawley
 */
public class IRestrictionTest {
    
    public IRestrictionTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testAddTimePeriod() {
    }

    @Test
    public void testRemoveTimePeriod() {
    }

    @Test
    public void testAllowAccess_3args() {
    }

    @Test
    public void testAllowAccess_IUser_OperatingMode() {
    }

    @Test
    public void testGetModeRestriction() {
    }

    @Test
    public void testGetRole() {
    }

    @Test
    public void testSetAccessMode() {
    }

    @Test
    public void testSetRole() {
    }

    public class IRestrictionImpl implements IRestriction {

        public Boolean addTimePeriod(TimePeriod toAdd) {
            return null;
        }

        public Boolean removeTimePeriod(int index) {
            return null;
        }

        public Boolean allowAccess(IUser toCheck, LocalTime timetoCheck, OperatingMode mode) {
            return null;
        }

        public Boolean allowAccess(IUser toCheck, OperatingMode mode) {
            return null;
        }

        public OperatingMode getModeRestriction() {
            return null;
        }

        public UserRole getRole() {
            return null;
        }

        public void setAccessMode(OperatingMode mode) {
        }

        public void setRole(UserRole toRestrict) {
        }

        @Override
        public Boolean allowAccess(IReadOnlyUser toCheck, LocalTime timetoCheck, OperatingMode mode) {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        @Override
        public Boolean allowAccess(IReadOnlyUser toCheck, OperatingMode mode) {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }
    }   

    @Test
    public void testAllowAccess_IReadOnlyUser_OperatingMode() {
    }    
}
