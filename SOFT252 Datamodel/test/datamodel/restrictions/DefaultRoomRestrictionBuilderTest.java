/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datamodel.restrictions;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author jjcrawley
 */
public class DefaultRoomRestrictionBuilderTest {
    
    public DefaultRoomRestrictionBuilderTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testBuildLectureHallDefaults() {
    }

    @Test
    public void testBuildSecureRoomDefaults() {
    }

    @Test
    public void testBuildStaffRoomDefaults() {
    }

    @Test
    public void testBuildResearchLabDefaults() {
    }

    @Test
    public void testBuildStudentLabDefaults() {
    }

    @Test
    public void testBuildSecurityDefaults() {
    }

    @Test
    public void testBuildStudentDefaults() {
    }

    @Test
    public void testBuildStaffDefaults() {
    }

    @Test
    public void testBuildContractCleanerDefaults() {
    }

    @Test
    public void testBuildManagerDefaults() {
    }

    @Test
    public void testBuildEmergencyResponderDefaults() {
    }

    @Test
    public void testBuildGuestDefaults() {
    }

    @Test
    public void testGetInstance() {
    }
    
}
