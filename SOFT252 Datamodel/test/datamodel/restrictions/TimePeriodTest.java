/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datamodel.restrictions;

import java.time.LocalTime;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author jjcrawley
 */
public class TimePeriodTest 
{
    private TimePeriod m_testPeriod; 
    
    public TimePeriodTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() 
    {
        m_testPeriod = new TimePeriod(LocalTime.of(1, 50, 20), LocalTime.of(5, 50));
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testWithinRange() 
    {
        assertTrue(m_testPeriod.withinRange(LocalTime.of(3, 20)));
    }

    @Test
    public void testGetStartTime() {
    }

    @Test
    public void testGetEndTime() {
    }

    @Test
    public void testEquals() 
    {
        TimePeriod testEquals = new TimePeriod(LocalTime.of(5, 50), LocalTime.of(1, 50, 20));
        
        assertTrue("not the same: ", m_testPeriod.equals(testEquals));
    }

    @Test
    public void testHashCode() {
    }    
}
