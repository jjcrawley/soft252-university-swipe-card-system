/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datamodel.restrictions;

import datamodel.restrictions.interfaces.IRestriction;
import datamodel.users.Card;
import datamodel.users.UserBuilder;
import datamodel.users.UserRole;
import datamodel.users.interfaces.IUser;
import java.time.LocalTime;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author jjcrawley
 */
public class RestrictionTest 
{
    private IRestriction m_testRestrictionSimple;
    private IRestriction m_testTimedRestriction;
    
    public RestrictionTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() 
    {
        m_testRestrictionSimple = new Restriction(UserRole.Guest, OperatingMode.Emergency);
        m_testTimedRestriction = new Restriction(UserRole.Guest, OperatingMode.Normal);
        m_testTimedRestriction.addTimePeriod(new TimePeriod(LocalTime.of(1, 0, 0), LocalTime.of(12, 00)));
        m_testTimedRestriction.addTimePeriod(new TimePeriod(LocalTime.of(13, 00), LocalTime.of(18, 00)));
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testGetRole() {
    }

    @Test
    public void testSetAccessMode() 
    {
        try
        {            
            m_testRestrictionSimple.setAccessMode(OperatingMode.Normal);
            assertEquals("didn't set the mode properly", OperatingMode.Normal, m_testRestrictionSimple.getModeRestriction());
            m_testRestrictionSimple.setAccessMode(OperatingMode.Emergency);
            
            m_testRestrictionSimple.setAccessMode(null);
            fail("can't set a null mode");
        }
        catch(IllegalArgumentException ex)
        {
            System.out.println(ex.getMessage());
        }
    }

    @Test
    public void testGetModeRestriction() {
    }

    @Test
    public void testSetRole()
    {
        try
        {
            m_testRestrictionSimple.setRole(UserRole.ContractCleaner);
            assertEquals("didn't set the role properly", UserRole.ContractCleaner, m_testRestrictionSimple.getRole());
            m_testRestrictionSimple.setRole(UserRole.Guest);
            
            m_testRestrictionSimple.setRole(null);
            fail("can't set a null role");
        }
        catch(IllegalArgumentException ex)
        {
            System.out.println(ex.getMessage());
        }        
    }

    @Test
    public void testAddTimePeriod() 
    {
        try
        {
            assertTrue(m_testRestrictionSimple.addTimePeriod(new TimePeriod(LocalTime.of(2, 00), LocalTime.of(13, 00))));
            
            m_testRestrictionSimple.addTimePeriod(null);
            fail("can't add a null time period");
        }
        catch(IllegalArgumentException ex)
        {
            System.out.println(ex.getMessage());
        }
    }

    @Test
    public void testRemoveTimePeriod() 
    {
        try
        {
            assertTrue(m_testRestrictionSimple.removeTimePeriod(0));
            
            m_testRestrictionSimple.removeTimePeriod(1);
            fail("index is out of range");
        }
        catch(IndexOutOfBoundsException ex)
        {
            System.out.println(ex.getMessage());
        }
    }

    @Test
    public void testAllowAccess_3args() {
    }    

    @Test
    public void testAllowAccess_IReadOnlyUser_OperatingMode() 
    {
        IUser guestTest = UserBuilder.getUserBuilder().setUserRole(UserRole.Guest).setUserCard(new Card(1111)).buildUser();
        
        assertFalse("failed to restrict ", m_testRestrictionSimple.allowAccess(guestTest, OperatingMode.Normal));
        assertTrue("failed to allow access ", m_testRestrictionSimple.allowAccess(guestTest, OperatingMode.Emergency));
        assertTrue("failed to restrict based on time: ", m_testTimedRestriction.allowAccess(guestTest, LocalTime.of(5, 00), OperatingMode.Normal));
        assertFalse("failed to restrict based on time: ", m_testTimedRestriction.allowAccess(guestTest, LocalTime.of(12, 30), OperatingMode.Normal));
    }    
}
