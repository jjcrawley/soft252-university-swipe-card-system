/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datamodel.logging;

import java.time.LocalDateTime;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author jjcrawley
 */
public class TimeStampTest 
{
    private TimeStamp m_testStamp;
    private LocalDateTime m_testDate;
    
    public TimeStampTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() 
    {
        m_testDate = LocalDateTime.now();
        m_testStamp = new SimpleTimeStamp(m_testDate);
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testGetCreationDate() 
    {
        assertEquals("date wasn't setup properly", m_testDate.toLocalDate(), m_testStamp.getCreationDate());
    }

    @Test
    public void testGetCreationTime() 
    {
        assertEquals("time wasn't set up correctly", m_testDate.toLocalTime(), m_testStamp.getCreationTime());
    }

    @Test
    public void testGetDateString() {
    }

    @Test
    public void testGetTimeString() {
    }

    @Test
    public void testGetTimeStampDetails() {
    }

    @Test
    public void testToString() {
    }

    public class TimeStampImpl extends TimeStamp {

        public String getTimeStampDetails() {
            return "";
        }
    }     
}
