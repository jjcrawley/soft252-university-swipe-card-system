/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datamodel.logging;

import datamodel.campus.Building;
import datamodel.campus.interfaces.IBuilding;
import datamodel.campus.rooms.RoomType;
import datamodel.restrictions.OperatingMode;
import datamodel.users.Card;
import datamodel.users.UserBuilder;
import datamodel.users.UserName;
import datamodel.users.interfaces.ICard;
import datamodel.users.interfaces.IName;
import datamodel.users.interfaces.IUser;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author jjcrawley
 */
public class SwipeEntryTimeStampBuilderTest 
{    
    private SwipeEntryTimeStampBuilder m_testBuilder;
    
    private String m_roomCode;
    private IBuilding m_testBuilding;
    private int m_floorNum;
    
    private IUser m_testUser;
    private ICard m_userCard;
    private IName m_userName;  
    
    public SwipeEntryTimeStampBuilderTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() 
    {
        m_userName = new UserName("Timmy", "James", "Turner");
        m_userCard = new Card(22222);
        m_testUser = UserBuilder.getUserBuilder().
                setUserCard(m_userCard).
                setUserName(m_userName).
                buildUser();
        
        m_testBuilding = new Building("Code", "Name");
        m_roomCode = "BBG001";
        m_floorNum = 1;
        
        m_testBuilder = new SwipeEntryTimeStampBuilder();        
        
        //(SwipeEntryTimeStamp)builder.buildSwipeEntryStamp(m_testUser.getUserCard(), Access.Refused, OperatingMode.Emergency);
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testSetRoomCode() 
    {
        m_testBuilder.setRoomCode(m_roomCode);
        
        SwipeEntryTimeStamp testStamp = (SwipeEntryTimeStamp)m_testBuilder.buildSwipeEntryStamp(m_testUser.getUserCard(), Access.Refused, OperatingMode.Emergency);
        
        assertEquals("incorrect creation", m_roomCode, testStamp.getRoomCode());
    }

    @Test
    public void testSetMyBuilding() 
    {
        m_testBuilder.setMyBuilding(m_testBuilding);
        
        SwipeEntryTimeStamp testStamp = (SwipeEntryTimeStamp)m_testBuilder.buildSwipeEntryStamp(m_testUser.getUserCard(), Access.Refused, OperatingMode.Emergency);
        
        assertEquals("incorrect creation", m_testBuilding.getBuildingCode(), testStamp.getBuildingCode());
    }

    @Test
    public void testSetRoomType() 
    {
        //not actually using room type
    }

    @Test
    public void testSetRoomMode() 
    {
        m_testBuilder.setRoomMode(OperatingMode.Normal);
        
        SwipeEntryTimeStamp testStamp = (SwipeEntryTimeStamp)m_testBuilder.buildSwipeEntryStamp(m_testUser.getUserCard(), Access.Refused);
        
        assertEquals("incorrect creation", OperatingMode.Normal, testStamp.getMode());
    }

    @Test
    public void testSetFloorNum() 
    {
        m_testBuilder.setFloorNum(m_floorNum);
        
        SwipeEntryTimeStamp testStamp = (SwipeEntryTimeStamp)m_testBuilder.buildSwipeEntryStamp(m_testUser.getUserCard(), Access.Refused, OperatingMode.Emergency);
        
        assertEquals("incorrect creation", m_floorNum, testStamp.getRoomFloor());
    }

    @Test
    public void testBuildSwipeEntryStamp_ICard_Access() 
    {        
        SwipeEntryTimeStamp testStamp = (SwipeEntryTimeStamp)m_testBuilder.buildSwipeEntryStamp(m_testUser.getUserCard(), Access.Refused);
        
        assertEquals("incorrect creation", m_testUser.getUserCard().viewCardID(), testStamp.getCardID());        
        assertEquals("incorrect creation", Access.Refused, testStamp.getAccessed());
        assertEquals("incorrect creation", m_testUser.viewUserName().getFullName(), testStamp.getUserName());
    }

    @Test
    public void testBuildSwipeEntryStamp_3args() 
    {
        SwipeEntryTimeStamp testStamp = (SwipeEntryTimeStamp)m_testBuilder.buildSwipeEntryStamp(m_testUser.getUserCard(), Access.Refused, OperatingMode.Emergency);
        
        assertEquals("incorrect creation", m_testUser.getUserCard().viewCardID(), testStamp.getCardID());        
        assertEquals("incorrect creation", Access.Refused, testStamp.getAccessed());
        assertEquals("incorrect creation", m_testUser.viewUserName().getFullName(), testStamp.getUserName());        
        assertEquals("incorrect creation", OperatingMode.Emergency, testStamp.getMode());
    }   
}