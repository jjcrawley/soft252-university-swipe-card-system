/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datamodel.logging;

import datamodel.campus.Building;
import datamodel.campus.Campus;
import datamodel.campus.interfaces.IBuilding;
import datamodel.campus.interfaces.ICampus;
import datamodel.campus.rooms.EmptyRoom;
import datamodel.campus.rooms.interfaces.IRoom;
import datamodel.users.UserBuilder;
import datamodel.users.interfaces.IUser;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author jjcrawley
 */
public class LogFileTest 
{
    private LogFile m_testFile;
    
    public LogFileTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp()
    {
        m_testFile = new LogFile();
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testAddRoomLog()
    {
        IRoom room = new EmptyRoom();
        
        m_testFile.addRoomLog(room);
        
        assertEquals("didn't add correctly", 1, m_testFile.getRoomLogs().size());
        
        try
        {
            m_testFile.addRoomLog(null);
            fail("shouldn't add a null room log");
        }
        catch(IllegalArgumentException ex)
        {
            System.out.println(ex.getMessage());
        }
    }

    @Test
    public void testAddBuildingLog() 
    {
        IBuilding room = new Building();
        
        m_testFile.addBuildingLog(room);
        
        assertEquals("didn't add correctly", 1, m_testFile.getBuildingLogs().size());
        
        try
        {
            m_testFile.addBuildingLog(null);
            fail("shouldn't add a null building log");
        }
        catch(IllegalArgumentException ex)
        {
            System.out.println(ex.getMessage());
        }
    }

    @Test
    public void testAddCampusLog() 
    {
        ICampus campus = new Campus();
        
        m_testFile.addCampusLog(campus);
        
        assertEquals("didn't add correctly", 1, m_testFile.getCampusLogs().size());
        
        try
        {
            m_testFile.addCampusLog(null);
            fail("shouldn't add a null campus log");
        }
        catch(IllegalArgumentException ex)
        {
            System.out.println(ex.getMessage());
        }
    }

    @Test
    public void testAddUser() 
    {
        IUser user = UserBuilder.getUserBuilder().buildUser();
        
        m_testFile.addUser(user);
        
        assertEquals("didn't add correctly", 1, m_testFile.getUsers().size());
        
        try
        {
            m_testFile.addUser(null);
            fail("shouldn't add a null user");
        }
        catch(IllegalArgumentException ex)
        {
            System.out.println(ex.getMessage());
        }
    }

    @Test
    public void testAddCampus() 
    {
        ICampus campus = new Campus();
        
        m_testFile.addCampus(campus);
        
        assertEquals("didn't add correctly", 1, m_testFile.getCampusesModel().size());
        
        try
        {
            m_testFile.addCampus(null);
            fail("shouldn't add a null campus");
        }
        catch(IllegalArgumentException ex)
        {
            System.out.println(ex.getMessage());
        }
    }

    @Test
    public void testViewRoomLogs() {
    }

    @Test
    public void testViewBuildingLogs() {
    }

    @Test
    public void testViewCampusLogs() {
    }

    @Test
    public void testViewUsers() {
    }

    @Test
    public void testGetCampusesModel() {
    }

    @Test
    public void testGetUsers() {
    }

    @Test
    public void testGetCreationDate() {
    }

    @Test
    public void testCloseFile() 
    {
        
    }    

    @Test
    public void testGetRoomLogs() {
    }

    @Test
    public void testGetBuildingLogs() {
    }

    @Test
    public void testGetCampusLogs() {
    }

    @Test
    public void testOnRead() {
    }

    @Test
    public void testOnWrite() {
    }
}