/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datamodel.logging;

import java.time.LocalDateTime;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author jjcrawley
 */
public class LoggerTest 
{
    private Logger m_testLogger;
    private LocalDateTime m_date;
    
    public LoggerTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() 
    {
        m_date = LocalDateTime.now();
        m_testLogger = new Logger(m_date.toLocalDate());
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testGetCreationDate() 
    {
        assertEquals("didn't create the logger properly", m_date.toLocalDate(), m_testLogger.getCreationDate());
    }

    @Test
    public void testMakeEntry() 
    {
        TimeStamp testStamp = new SimpleTimeStamp();
        
        m_testLogger.makeEntry(testStamp);
        
        assertEquals("didn't make entry", 1, m_testLogger.viewEntries().size());
        
        assertFalse("can't add a null entry", m_testLogger.makeEntry(null));
    }

    @Test
    public void testViewEntries() {
    }

    @Test
    public void testResetLogger() {
    }
    
}
