/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datamodel.logging.interfaces;

import java.time.LocalDate;
import java.time.LocalTime;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author jjcrawley
 */
public class ITimeStampTest {
    
    public ITimeStampTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testGetCreationDate() {
    }

    @Test
    public void testGetCreationTime() {
    }

    @Test
    public void testGetDateString() {
    }

    @Test
    public void testGetTimeString() {
    }

    @Test
    public void testGetTimeStampDetails() {
    }

    public class ITimeStampImpl implements ITimeStamp {

        public LocalDate getCreationDate() {
            return null;
        }

        public LocalTime getCreationTime() {
            return null;
        }

        public String getDateString() {
            return "";
        }

        public String getTimeString() {
            return "";
        }

        public String getTimeStampDetails() {
            return "";
        }
    }      
}
