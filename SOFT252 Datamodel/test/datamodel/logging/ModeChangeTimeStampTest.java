/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datamodel.logging;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author jjcrawley
 */
public class ModeChangeTimeStampTest 
{   
    ModeChangeTimeStamp m_testStamp;
    
    public ModeChangeTimeStampTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() 
    {
        m_testStamp = new ModeChangeTimeStamp("no details", "no reason");
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testGetReason() 
    {
        assertEquals("incorrect creation", "no details", m_testStamp.getDetails());
        
        try
        {
            m_testStamp = new ModeChangeTimeStamp(null, null);
            fail("shouldn't have been created");
        }
        catch(IllegalArgumentException ex)
        {
            System.out.println(ex.getMessage());
        }
    }

    @Test
    public void testGetDetails() 
    {
        assertEquals("incorrect creation", "no reason", m_testStamp.getReason());
    }

    @Test
    public void testGetTimeStampDetails() {
    }

    @Test
    public void testToString() {
    }    
}