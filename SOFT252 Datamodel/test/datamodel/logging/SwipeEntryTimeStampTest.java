/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datamodel.logging;

import datamodel.campus.Building;
import datamodel.campus.interfaces.IBuilding;
import datamodel.campus.rooms.RoomType;
import datamodel.restrictions.OperatingMode;
import datamodel.users.Card;
import datamodel.users.UserBuilder;
import datamodel.users.UserName;
import datamodel.users.interfaces.ICard;
import datamodel.users.interfaces.IName;
import datamodel.users.interfaces.IUser;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author jjcrawley
 */
public class SwipeEntryTimeStampTest 
{
    private SwipeEntryTimeStamp m_testStamp;
    
    private String m_roomCode;
    private IBuilding m_testBuilding;
    private int m_floorNum;
    
    private IUser m_testUser;
    private ICard m_userCard;
    private IName m_userName;       
    
    public SwipeEntryTimeStampTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() 
    {
        m_userName = new UserName("Timmy", "James", "Turner");
        m_userCard = new Card(22222);
        m_testUser = UserBuilder.getUserBuilder().
                setUserCard(m_userCard).
                setUserName(m_userName).
                buildUser();
        
        m_testBuilding = new Building();
        m_roomCode = "BBG";
        m_floorNum = 1;
        
        SwipeEntryTimeStampBuilder builder = new SwipeEntryTimeStampBuilder(m_roomCode, m_testBuilding, m_floorNum, RoomType.StaffRoom);
        
        
        m_testStamp = (SwipeEntryTimeStamp)builder.buildSwipeEntryStamp(m_testUser.getUserCard(), Access.Refused, OperatingMode.Emergency);
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testGetCardID() 
    {
        assertEquals("did not create properly", 22222, m_testStamp.getCardID());
    }

    @Test
    public void testGetUserName() 
    {
        assertEquals("did not create properly", m_userName.getFullName(), m_testStamp.getUserName());                
    }

    @Test
    public void testGetBuildingCode() 
    {
        assertEquals("did not create properly", m_testBuilding.getBuildingCode(), m_testStamp.getBuildingCode());
    }

    @Test
    public void testGetRoomFloor() 
    {
        assertEquals("did not create properly", m_floorNum, m_testStamp.getRoomFloor());
    }

    @Test
    public void testGetRoomCode() 
    {
        assertEquals("did not create properly", m_roomCode, m_testStamp.getRoomCode());
    }

    @Test
    public void testGetMode() 
    {
        assertEquals("did not create properly", OperatingMode.Emergency, m_testStamp.getMode());
    }

    @Test
    public void testGetAccessed() 
    {
        assertEquals("did not create properly", Access.Refused, m_testStamp.getAccessed());
    }

    @Test
    public void testGetTimeStampDetails() {
    }

    @Test
    public void testToString() {
    }
    
}
