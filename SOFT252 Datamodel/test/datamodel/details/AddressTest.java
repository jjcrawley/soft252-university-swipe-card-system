/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datamodel.details;

import datamodel.details.interfaces.IAddress;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author jjcrawley
 */
public class AddressTest 
{
    private AddressBuilder m_builder;
    private IAddress m_testAddress;
    
    public AddressTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() 
    {
        m_builder = AddressBuilder.getAddressBuilder(); 
        
        m_testAddress = m_builder.buildAddress();
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testSetAddressLine1() 
    {
        String line1 = "twentyfive";
        
        m_testAddress.setAddressLine1(line1);        
        
        assertEquals("testing addressLine 1", line1, m_testAddress.getAddressLine1());
    }

    @Test
    public void testSetAddressLine2()
    {
        String line2 = "twentynine";
        
        m_testAddress.setAddressLine2(line2);        
        
        assertEquals("testing addressLine 2", line2, m_testAddress.getAddressLine2());
    }

    @Test
    public void testSetCity() 
    {
        String city = "Plymouth";
        
        m_testAddress.setCity(city);        
        
        assertEquals("testing city", city, m_testAddress.getCity());
    }

    @Test
    public void testSetCounty() 
    {
        String county = "Devon";
        
        m_testAddress.setCounty(county);        
        
        assertEquals("testing addressLine 1", county, m_testAddress.getCounty());
    }

    @Test
    public void testSetPostCode() 
    {
        String postCode = "PL1 0UL";
        
        m_testAddress.setPostCode(postCode);        
        
        assertEquals("testing addressLine 1", postCode, m_testAddress.getPostCode());
    }

    @Test
    public void testGetAddressLine1() {
    }

    @Test
    public void testGetAddressLine2() {
    }

    @Test
    public void testGetCity() {
    }

    @Test
    public void testGetCounty() {
    }

    @Test
    public void testGetPostCode() {
    }
    
}
