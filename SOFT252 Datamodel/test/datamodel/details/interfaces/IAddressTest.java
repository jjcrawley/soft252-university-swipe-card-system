/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datamodel.details.interfaces;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author jjcrawley
 */
public class IAddressTest {
    
    public IAddressTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testSetAddressLine1() {
    }

    @Test
    public void testSetAddressLine2() {
    }

    @Test
    public void testSetCity() {
    }

    @Test
    public void testSetCounty() {
    }

    @Test
    public void testSetPostCode() {
    }

    public class IAddressImpl implements IAddress {

        public void setAddressLine1(String newLine1) {
        }

        public void setAddressLine2(String newLine2) {
        }

        public void setCity(String newCity) {
        }

        public void setCounty(String newCounty) {
        }

        public void setPostCode(String newPostCode) {
        }

        @Override
        public String getAddressLine1() {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        @Override
        public String getAddressLine2() {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        @Override
        public String getCity() {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        @Override
        public String getCounty() {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        @Override
        public String getPostCode() {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }
    }    

   
}
