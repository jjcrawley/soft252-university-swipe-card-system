/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datamodel.details.interfaces;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author jjcrawley
 */
public class IReadOnlyAddressTest {
    
    public IReadOnlyAddressTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testGetAddressLine1() {
    }

    @Test
    public void testGetAddressLine2() {
    }

    @Test
    public void testGetCity() {
    }

    @Test
    public void testGetCounty() {
    }

    @Test
    public void testGetPostCode() {
    }

    public class IReadOnlyAddressImpl implements IReadOnlyAddress {

        public String getAddressLine1() {
            return "";
        }

        public String getAddressLine2() {
            return "";
        }

        public String getCity() {
            return "";
        }

        public String getCounty() {
            return "";
        }

        public String getPostCode() {
            return "";
        }
    }     

    
}
