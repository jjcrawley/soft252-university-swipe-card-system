/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datamodel.details;

import datamodel.details.interfaces.IAddress;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author jjcrawley
 */
public class AddressBuilderTest 
{
    private AddressBuilder m_builder;
    private IAddress m_testAddress;
    
    public AddressBuilderTest() 
    {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() 
    {
        m_builder = AddressBuilder.getAddressBuilder();                
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testSetAddressLine1() 
    {
        String line1 = "twentyfive";
        
        m_builder.setAddressLine1(line1);
        
        m_testAddress = m_builder.buildAddress();
        
        assertEquals("testing addressLine 1", line1, m_testAddress.getAddressLine1());
        
        try
        {
            m_builder.setAddressLine1(null);
            fail("address line 1 can't be null");
        }
        catch(IllegalArgumentException ex)
        {
            System.out.println(ex.getMessage());
        }
    }

    @Test
    public void testSetAddressLine2() 
    {
        String line2 = "five";
        m_builder.setAddressLine2(line2);
        
        m_testAddress = m_builder.buildAddress();
        
        assertEquals("testing addressline 2", line2, m_testAddress.getAddressLine2());
        
        try
        {
            m_builder.setAddressLine2(null);
            fail("address line 2 can't be null");
        }
        catch(IllegalArgumentException ex)
        {
            System.out.println(ex.getMessage());
        }
    }

    @Test
    public void testSetCity() 
    {
        String city = "Plymouth";
        
        m_builder.setCity(city);
        
        m_testAddress = m_builder.buildAddress();
        
        assertEquals("testing city", city, m_testAddress.getCity());
        
        try
        {
            m_builder.setCity(null);
            fail("city can't be null");
        }
        catch(IllegalArgumentException ex)
        {
            System.out.println(ex.getMessage());
        }
    }

    @Test
    public void testSetCounty() 
    {
        String county = "Plymouth";
        
        m_builder.setCounty(county);
        
        m_testAddress = m_builder.buildAddress();
        
        assertEquals("testing county", county, m_testAddress.getCounty());
        
        try
        {
            m_builder.setCounty(null);
            fail("county can't be null");
        }
        catch(IllegalArgumentException ex)
        {
            System.out.println(ex.getMessage());
        }
    }

    @Test
    public void testSetPostCode() 
    {
        String postCode = "PL1 1UX";
        
        m_builder.setPostCode(postCode);
        
        m_testAddress = m_builder.buildAddress();
        
        assertEquals("testing postcode", postCode, m_testAddress.getPostCode());
        
        try
        {
            m_builder.setPostCode(null);
            fail("post code can't be null");
        }
        catch(IllegalArgumentException ex)
        {
            System.out.println(ex.getMessage());
        }
    }

    @Test
    public void testGetAddressBuilder() {
    }

    @Test
    public void testBuildAddress() {
    }
    
}
