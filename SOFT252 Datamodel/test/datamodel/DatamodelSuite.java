/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datamodel;

import datamodel.campus.CampusSuite;
import datamodel.details.DetailsSuite;
import datamodel.logging.LoggingSuite;
import datamodel.restrictions.RestrictionsSuite;
import datamodel.users.UsersSuite;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 *
 * @author jjcrawley
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({DetailsSuite.class, RestrictionsSuite.class, UsersSuite.class, CampusSuite.class, LoggingSuite.class})
public class DatamodelSuite {

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }
    
}
