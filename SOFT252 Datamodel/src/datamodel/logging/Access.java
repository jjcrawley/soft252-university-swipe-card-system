/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datamodel.logging;

import java.io.Serializable;

/**
 * An access enumeration that defines what access types are permitted in the system
 * @author jjcrawley
 */
public enum Access implements Serializable
{

    /**
     * Access was allowed
     */
    Allowed,

    /**
     * Access was refused
     */
    Refused;    
}
