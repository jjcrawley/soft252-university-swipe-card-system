/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datamodel.logging;

import datamodel.logging.interfaces.ILogger;
import datamodel.logging.interfaces.ITimeStamp;
import java.util.*;
import java.time.*;
import java.io.Serializable;

/**
 * A simple logger that maintains a list of time stamps in system memory
 * @author jjcrawley
 */
public class Logger implements ILogger, Serializable
{
    private LocalDate m_creationDate;
    private final List<ITimeStamp> m_logEntries;
    
    /**
     * Create an empty logger with the creation date set to the current date
     */
    public Logger()
    {
        m_logEntries = new ArrayList<>();
        m_creationDate = LocalDate.now();
    }
    
    /**
     * Create an empty logger with the prescribed creation date
     * @param creationDate the creation date
     * @throws IllegalArgumentException thrown when the date is null
     */
    public Logger(LocalDate creationDate) throws IllegalArgumentException
    {
        if(creationDate == null)
        {
            throw new IllegalArgumentException("date can't be null");
        }
        m_logEntries = new ArrayList<>();
        m_creationDate = creationDate;
    }

    /**
     * Get the creation date of the logger
     * @return the logger creation date
     */
    public LocalDate getCreationDate() 
    {
        return m_creationDate;
    }      
    
    @Override
    public Boolean makeEntry(ITimeStamp toLog)
    {
        if(toLog == null)
        {
            return false;            
        }
        
        return m_logEntries.add(toLog);
    }

    @Override
    public void resetLogger() 
    {
        m_creationDate = LocalDate.now();
        m_logEntries.clear();
    }
   
    @Override
    public List<ITimeStamp> viewEntries()
    {
        List<ITimeStamp> copiedLog = new ArrayList<>();
        
        m_logEntries.stream().forEach((currentEntry) ->
        {
            copiedLog.add(currentEntry);
        });
        
        return copiedLog;
    }  
}
