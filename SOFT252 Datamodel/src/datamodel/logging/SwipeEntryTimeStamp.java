
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datamodel.logging;

import datamodel.restrictions.OperatingMode;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * A time stamp that is used to store additional details about a card swipe
 * The stamp stores: cardID, userName, buildingCode, roomFloor, roomCode, accessMode, accessed
 * @author jjcrawley
 */
public class SwipeEntryTimeStamp extends TimeStamp implements Serializable
{
    private final int m_cardID;
    private final String m_userName;
    private final String m_buildingCode;
    private final int m_roomFloor;
    private final String m_roomCode;
    private final OperatingMode e_mode;
    private final Access e_accessed;

    /**
     * Create an empty swipe stamp with no prescribed details
     */
    public SwipeEntryTimeStamp()
    {
        super();
        m_cardID = 0;
        m_buildingCode = "Unknown";
        m_userName = "Unknown";
        m_roomFloor = 0;
        m_roomCode = "Unknown";
        e_mode = OperatingMode.Normal;
        e_accessed = Access.Refused;
    }    
    
    /**
     * Create a time stamp with the prescribed details
     * @param cardID the card id of the user
     * @param userName the users name
     * @param buildingCode the buildings code
     * @param roomFloor the room floor
     * @param roomCode the room code
     * @param mode the rooms operating mode
     * @param accessed the access type
     * @throws IllegalArgumentException thrown when either nullable values are null, or the card id and floor number are less than 0
     */
    public SwipeEntryTimeStamp(int cardID, String userName, String buildingCode, int roomFloor, String roomCode, OperatingMode mode, Access accessed) throws IllegalArgumentException
    {
        super();
        
        if(userName == null || buildingCode == null || roomCode == null || mode == null || accessed == null)
        {
            throw new IllegalArgumentException("cannot initialise with null arguments");            
        }
        
        if(cardID < 0 || userName.isEmpty() || buildingCode.isEmpty() || roomFloor < 0 || roomCode.isEmpty())
        {
            throw new IllegalArgumentException("unable to initialise with the current arguments");
        }
        else
        {            
            m_cardID = cardID;
            m_userName = userName;
            m_buildingCode = buildingCode;
            m_roomFloor = roomFloor;
            m_roomCode = roomCode;
            e_mode = mode;
            e_accessed = accessed;
        }
    }
    
    /**
     * Create a time stamp with the prescribed details
     * @param cardID the card id of the user
     * @param userName the users name
     * @param buildingCode the buildings code
     * @param roomFloor the room floor
     * @param roomCode the room code
     * @param mode the rooms operating mode
     * @param accessed the access type
     * @param creationTime the creation date of the stamp
     * @throws IllegalArgumentException thrown when either nullable values are null, or the card id and floor number are less than 0
     */
    public SwipeEntryTimeStamp(int cardID, String userName, String buildingCode, int roomFloor, String roomCode, OperatingMode mode, Access accessed, LocalDateTime creationTime) throws IllegalArgumentException
    {
        super(creationTime);
        
        if(userName == null || buildingCode == null || roomCode == null || mode == null || accessed == null)
        {
            throw new IllegalArgumentException("cannot initialise with null arguments");            
        }
        
        if(cardID < 0 || userName.isEmpty() || buildingCode.isEmpty() || roomFloor < 0 || roomCode.isEmpty())
        {
            throw new IllegalArgumentException("unable to initialise with the current arguments");
        }
        else
        {
            m_cardID = cardID;
            m_userName = userName;
            m_buildingCode = buildingCode;
            m_roomFloor = roomFloor;
            m_roomCode = roomCode;
            e_mode = mode;
            e_accessed = accessed;
        }      
    }
    
    /**
     * Retrieve the card id of the stamp
     * @return stored card id
     */
    public int getCardID() 
    {
        return m_cardID;
    }
    
    /**
     * Retrieve the user name of the stamp
     * @return stored user name
     */
    public String getUserName() 
    {
        return m_userName;
    }   

    /**
     * Retrieve the building code of the stamp
     * @return stored building code
     */
    public String getBuildingCode() 
    {
        return m_buildingCode;
    }   

    /**
     * Retrieve the room floor number of the stamp
     * @return the stored floor number
     */
    public int getRoomFloor() 
    {
        return m_roomFloor;
    }    

    /**
     * Retrieve the room code of the stamp
     * @return the stored room code
     */
    public String getRoomCode() 
    {
        return m_roomCode;
    }    

    /**
     * Retrieve the operating mode of the stamp
     * @return the stored operating mode
     */
    public OperatingMode getMode() 
    {
        return e_mode;
    }    

    /**
     * Retrieve the access details of the stamp
     * @return the stored access
     */
    public Access getAccessed() 
    {
        return e_accessed;
    }    

    @Override
    public String getTimeStampDetails() 
    {      
        return "Swipe Entry room: " + m_roomCode + ", floor: " + m_roomFloor + 
                ", mode: " + e_mode + ", building: " + m_buildingCode + ", cardID: " + 
                m_cardID + ", user: " + m_userName + ", access: " + e_accessed;
    }   

    /**
     * Retrieve a stored representation of the time stamp: DateTime, buildingCode, 
     * roomFloor, roomCode, mode, card id, userName, access
     * @return formatted representation of the stamp
     */
    @Override
    public String toString() 
    {
        return super.toString() + ", building: " + m_buildingCode + ", floor: " + m_roomFloor + 
                ", room: " + m_roomCode + ", mode: " + e_mode + " cardID: " + m_cardID + 
                ", user: " + m_userName + ", access: " + e_accessed;
    }    
}