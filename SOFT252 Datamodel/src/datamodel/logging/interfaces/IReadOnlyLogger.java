/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datamodel.logging.interfaces;

import java.util.List;

/**
 * Defines standard behaviour for a read only logger
 * @author jjcrawley
 */
public interface IReadOnlyLogger 
{

    /**
     * View the time stamps stored in the logger
     * @return a list of time stamps stored in the logger
     */
    public List<ITimeStamp> viewEntries();       
}
