/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datamodel.logging.interfaces;

import java.time.LocalDate;
import java.time.LocalTime;

/**
 * Defines standard behaviour for a time stamp
 * @author jjcrawley
 */
public interface ITimeStamp 
{

    /**
     * Retrieve the creation date of the stamp
     * @return the creation date
     */
    public LocalDate getCreationDate();

    /**
     * Retrieve the creation time of the time stamp
     * @return the creation time
     */
    public LocalTime getCreationTime();

    /**
     * Retrieve a formatted version of the creation date
     * @return creation date as a formated string
     */
    public String getDateString();

    /**
     * Retrieve a formatted version of the creation time
     * @return the creation time as a formatted string
     */
    public String getTimeString();

    /**
     * Retrieve any details associated with the time stamp as a formatted string
     * @return the time stamp details as a formatted string
     */
    public String getTimeStampDetails();
}
