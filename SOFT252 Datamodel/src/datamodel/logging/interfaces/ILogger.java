/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datamodel.logging.interfaces;

/**
 * Defines standard behaviour for a logger
 * @author jjcrawley
 */
public interface ILogger extends IReadOnlyLogger
{

    /**
     * Make an entry into the log
     * @param toLog the time stamp to log
     * @return true if the log was successful, false otherwise
     */
    public Boolean makeEntry(ITimeStamp toLog); 

    /**
     * Resets the logger
     */
    public void resetLogger();
}
