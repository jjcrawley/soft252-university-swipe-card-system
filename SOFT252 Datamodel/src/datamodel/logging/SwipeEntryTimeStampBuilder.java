/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datamodel.logging;

import datamodel.campus.Building;
import datamodel.campus.interfaces.IBuilding;
import datamodel.campus.rooms.RoomType;
import datamodel.restrictions.OperatingMode;
import datamodel.users.interfaces.ICard;

/**
 * A builder used to to create and construct SwipeEntryTimeStamps
 * Preferred over manually constructing a stamp
 * @author jjcrawley
 */
public class SwipeEntryTimeStampBuilder 
{
    private String m_roomCode = "Unknown";
    private IBuilding m_building = new Building("Unknown", "Unknown");
    private int m_floorNum = 0;
    private RoomType m_roomType = RoomType.LectureHall;
    private OperatingMode m_currentMode = OperatingMode.Normal;
    
    /**
     * Creates an empty time stamp builder
     */
    public SwipeEntryTimeStampBuilder()
    {      
        
    }
    
    /**
     * Creates a time stamp builder with the prescribed properties
     * @param roomCode the room code
     * @param building the building
     * @param floorNumber the floor number
     * @param roomType the room type
     * @throws IllegalArgumentException thrown when nullable parameters are null, the room code is empty or the floor number is less than 0
     */
    public SwipeEntryTimeStampBuilder(String roomCode, IBuilding building, int floorNumber, RoomType roomType) throws IllegalArgumentException
    {   
        if(roomCode == null || building == null || roomType == null)
        {
            throw new IllegalArgumentException("can't construct with current parameters");            
        }
        if(roomCode.isEmpty() || floorNumber < 0)
        {
            throw new IllegalArgumentException("can't construct with current parameters");
        }
        else
        {
            m_roomCode = roomCode;
            m_building = building;
            m_floorNum = floorNumber;
            m_roomType = roomType;
        }   
    }

    /**
     * Set the room code of the time stamp to construct
     * @param roomCode the room code
     * @return this instance of the builder
     * @throws IllegalArgumentException thrown when room code is null or empty
     */
    public SwipeEntryTimeStampBuilder setRoomCode(String roomCode) throws IllegalArgumentException
    {
        if(roomCode == null || roomCode.isEmpty())
        {
            throw new IllegalArgumentException("room code can't be empty");
        }
        else
        {
            m_roomCode = roomCode;
            return this;
        }  
    }

    /**
     * Set the building of the builder
     * @param myBuilding the building
     * @return this instance of the builder
     * @throws IllegalArgumentException thrown when the building is null
     */
    public SwipeEntryTimeStampBuilder setMyBuilding(IBuilding myBuilding) throws IllegalArgumentException
    {
        if(myBuilding == null)
        {
            throw new IllegalArgumentException("building can't be null");
        }
        else
        {
            m_building = myBuilding;
            return this;
        } 
    }
    
    /**
     * Set the room type of the builder
     * @param roomType the room type
     * @return this instance of the builder
     * @throws IllegalArgumentException thrown when room type is null
     */
    public SwipeEntryTimeStampBuilder setRoomType(RoomType roomType) throws IllegalArgumentException
    {
        if(roomType == null)
        {
            throw new IllegalArgumentException("room type can't be null");
        }
        else
        {
            m_roomType = roomType;
            return this;
        }
    }
    
    /**
     * Sets the operating mode of the builder
     * @param mode the operating mode
     * @return this instance of the builder
     * @throws IllegalArgumentException thrown when mode is null
     */
    public SwipeEntryTimeStampBuilder setRoomMode(OperatingMode mode) throws IllegalArgumentException
    {
        if(mode == null)
        {
            throw new IllegalArgumentException("mode can't be null");
        }
        else
        {
            m_currentMode = mode; 
            return this;
        }
    }
    
    /**
     * Set the floor number of the builder
     * @param floorNum the floor number
     * @return this instance of the builder
     * @throws IllegalArgumentException thrown when the floor number is less than 0
     */
    public SwipeEntryTimeStampBuilder setFloorNum(int floorNum) throws IllegalArgumentException
    {
        if(floorNum < 0)
        {
            throw new IllegalArgumentException("the floor number can't be below 0");
        }
        else
        {
            m_floorNum = floorNum;
            return this;
        }
    }
    
    /**
     * Creates the constructed stamp with the additional details provided
     * @param userCard the user card to be logged
     * @param accessed the access 
     * @return the newly constructed time stamp
     * @throws IllegalArgumentException thrown when the either argument is null
     */
    public TimeStamp buildSwipeEntryStamp(ICard userCard, Access accessed) throws IllegalArgumentException
    {
        if(userCard == null || accessed == null)
        {
            throw new IllegalArgumentException("can't build using a null card or accessed value");
        }
        else
        {
            return new SwipeEntryTimeStamp(userCard.viewCardID(), userCard.viewUser().viewUserName().getFullName(), 
                    m_building.getBuildingCode(), m_floorNum, m_roomCode, m_currentMode, accessed);
        }
    }
    
    /**
     * Build the time stamp with the additional details provided
     * @param userCard the user card to log
     * @param accessed the access
     * @param mode the operating mode
     * @return the newly constructed time stamp
     * @throws IllegalArgumentException thrown when either argument is null
     */
    public TimeStamp buildSwipeEntryStamp(ICard userCard, Access accessed, OperatingMode mode) throws IllegalArgumentException
    {
        if(userCard == null || accessed == null || mode == null)
        {
            throw new IllegalArgumentException("can't build with a null card or null accessed or null mode");
        }
        else
        {
            return new SwipeEntryTimeStamp(userCard.viewCardID(), userCard.viewUser().viewUserName().getFullName(), 
                    m_building.getBuildingCode(), m_floorNum, m_roomCode, mode, accessed);
        }
    }
}
