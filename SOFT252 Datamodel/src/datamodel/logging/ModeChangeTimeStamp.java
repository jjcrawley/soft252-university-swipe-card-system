/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datamodel.logging;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * A time stamp implementation to be used when logging mode changes in a system
 * @author jjcrawley
 */
public class ModeChangeTimeStamp extends TimeStamp implements Serializable
{
    private final String m_reason;
    private final String m_details;
    
    /**
     * Create an empty stamp 
     */
    public ModeChangeTimeStamp()
    {
        super();
        
        m_reason = "no reason specified";
        m_details = "no details specified";
    }

    /**
     * Create a stamp with the prescribed properties
     * @param details the details of the mode change
     * @param reason the reason for the mode change
     * @throws IllegalArgumentException thrown when any argument is null, or the details are empty
     */
    public ModeChangeTimeStamp(String details, String reason) throws IllegalArgumentException
    {
        super();
        
        if(details == null || reason == null)
        {
            throw new IllegalArgumentException("can't except null arguments");  
        }
        if(details.isEmpty())
        {
            throw new IllegalArgumentException("there needs to be details on the mode change");       
        }
        else
        {
            this.m_reason = reason;
            this.m_details = details;
        }   
    }   

    /**
     * Create a stamp with the prescribed properties
     * @param details the details of the mode change
     * @param reason the reason for the mode change
     * @param creationDateTime the creation date of the stamp
     * @throws IllegalArgumentException thrown any argument is null, or the details are empty
     */
    public ModeChangeTimeStamp(String details, String reason, LocalDateTime creationDateTime) throws IllegalArgumentException
    {        
        super(creationDateTime);
        
        if(details == null || reason == null || creationDateTime == null)
        {
            throw new IllegalArgumentException("can't except null arguments");  
        }
        if(details.isEmpty())
        {
            throw new IllegalArgumentException("there needs to be details on the mode change");       
        }
        else
        {
            m_reason = reason;
            m_details = details;
        }   
    }

    /**
     * Get the stored reason in the stamp
     * @return the reason for the mode change
     */
    public String getReason()
    {
        return m_reason;
    }   

    /**
     * Get the stored details in the stamp
     * @return the details of the mode change
     */
    public String getDetails() 
    {
        return m_details;
    } 

    @Override
    public String getTimeStampDetails() 
    {
        return "Mode Change details: " + m_details + ", reason: " + m_reason;
    }    

    /**
     * Creates a string representation of the stamp: DateTime, details, reason
     * @return the formatted string representation
     */
    @Override
    public String toString() 
    {
        return super.toString() + " details: " + m_details + ", reason: " + m_reason;
    }    
}
