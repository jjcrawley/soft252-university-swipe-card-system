/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datamodel.logging;

import java.time.LocalDateTime;

/**
 * A simple time stamp that just has a creation date with no exclusive properties
 * @author jjcrawley
 */
public class SimpleTimeStamp extends TimeStamp
{

    /**
     * Create an empty time time stamp
     */
    public SimpleTimeStamp()
    {
        super();        
    }   
    
    /**
     * Create a simple time stamp with the prescribed creation date and time
     * @param creationDateTime the stamps creation date time
     */
    public SimpleTimeStamp(LocalDateTime creationDateTime) 
    {
        super(creationDateTime);
    } 

    @Override
    public String getTimeStampDetails() 
    {       
        return super.toString();
    }   
}
