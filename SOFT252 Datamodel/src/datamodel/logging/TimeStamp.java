/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datamodel.logging;

import datamodel.logging.interfaces.ITimeStamp;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

/**
 * A base class for all time stamps that have a creation date and time
 * @author jjcrawley
 */
public abstract class TimeStamp implements ITimeStamp, Serializable
{
    //private final LocalDate m_creationDate;
    //private final LocalTime m_creationTime;
    private final LocalDateTime m_creationTimeAndDate;
    //private ReentrantLock lock;
    
    /**
     * Create a time stamp with the creation datetime set to now
     */
    public TimeStamp()
    {
        //lock = new ReentrantLock();
        //m_creationDate = LocalDate.now();
        //m_creationTime = LocalTime.now();
        m_creationTimeAndDate = LocalDateTime.now();
    }  
    
    /**
     * Create a time stamp with the prescribed creationDateTime
     * @param creationDateTime the creationDateTime
     */
    public TimeStamp(LocalDateTime creationDateTime)
    {
        if(creationDateTime == null)
        {
            throw new IllegalArgumentException("can't initialise with a null date");
        }
        else
        {
            m_creationTimeAndDate = creationDateTime;
        }
    }

    @Override
    public LocalDate getCreationDate() 
    {
        return m_creationTimeAndDate.toLocalDate();
        //return LocalDate.from(m_creationDate);
        //return creationDate;
    }

    @Override
    public LocalTime getCreationTime() 
    {
        return m_creationTimeAndDate.toLocalTime();
        //return LocalTime.from(m_creationTime);
        //return creationTime;
    }   
    
    @Override
    public String getDateString()
    {
        //DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("yyyy:m:dd");
        //return dateFormatter.format(m_creationTimeAndDate);
        
        String dateToString;
        
        dateToString =  m_creationTimeAndDate.getDayOfMonth() + "/";
        dateToString += m_creationTimeAndDate.getMonthValue() + "/";
        dateToString += m_creationTimeAndDate.getYear();        
        
        return dateToString;       
    }   
    
    @Override
    public String getTimeString()
    {
        String timeToString;
        
        timeToString = m_creationTimeAndDate.getHour() + ":";
        timeToString += m_creationTimeAndDate.getMinute();
        //timeToString += m_creationTimeAndDate.getSecond();
        
        return timeToString;
    }

    @Override
    public abstract String getTimeStampDetails();   

    /**
     * Gets a formatted string of the time stamp: Date, time
     * @return the time stamp as a string
     */
    @Override
    public String toString() 
    {
        return " Date: " + getDateString() + " " + getTimeString();
    }    
}
