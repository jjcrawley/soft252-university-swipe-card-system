/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datamodel.logging;

import java.time.*;
import java.util.*;
import datamodel.campus.interfaces.IBuilding;
import datamodel.campus.interfaces.ICampus;
import datamodel.campus.rooms.interfaces.IRoom;
import datamodel.users.interfaces.IUser;
import serialisation.ISerialisable;

/**
 * A simple log file class
 * This is used primarily for the bulk saving and loading of campus models, user models,
 * and each log file in the system.
 * @author jjcrawley
 */
public class LogFile implements ISerialisable
{
    private final LocalDateTime m_creationDate;
    private final Map<String, Logger> m_roomLogs;
    private final Map<String, Logger> m_buildingLogs;
    private final Map<String, Logger> m_campusLogs;
    private final Map<Integer, IUser> m_users;
    private final Map<String, ICampus> m_allCampuses;   
    
    /**
     * Create an empty log file
     * The creation date is initialised to the current date
     */
    public LogFile()
    {            
        m_creationDate = LocalDateTime.now();
        m_roomLogs = new HashMap<>();
        m_buildingLogs = new HashMap<>();
        m_campusLogs = new HashMap<>();
        m_users = new HashMap<>();
        m_allCampuses = new HashMap<>();
    }
    
    /**
     * Create a log file with the specified creation date
     * @param creationDate the creation date
     */
    public LogFile(LocalDateTime creationDate)
    {        
        m_creationDate = creationDate;
        m_roomLogs = new HashMap<>();
        m_buildingLogs = new HashMap<>();
        m_campusLogs = new HashMap<>();
        m_users = new HashMap<>();
        m_allCampuses = new HashMap<>();
    } 
    
    /**
     * Add a rooms log to the log file
     * @param toAdd the room with the log file to add
     * @return true if the log was successful, false otherwise
     * @throws IllegalArgumentException thrown if the room is null
     */
    public Boolean addRoomLog(IRoom toAdd) throws IllegalArgumentException
    {
        if(toAdd == null)
        {
            throw new IllegalArgumentException("can't add an null room log");
        }
        else
        {
            Logger log = toAdd.getLog();
            return m_roomLogs.put(toAdd.getRoomCode(), log) != null;
        }
    }   
    
    /**
     * Adds a buildings log to the log file
     * @param toAdd the building's log to add
     * @return true if the log was successful, false otherwise
     * @throws IllegalArgumentException thrown when the toAdd is null
     */
    public Boolean addBuildingLog(IBuilding toAdd) throws IllegalArgumentException
    {
        if(toAdd == null)
        {
            throw new IllegalArgumentException("can't add a null building log");
        }
        else
        {
            Logger log = toAdd.getLog();
            return m_buildingLogs.put(toAdd.getBuildingCode(), log) != null;
        }
    }    
    
    /**
     * Adds a given campuses log to the log file
     * @param toAdd the campuses log to add
     * @return true if the log was successful, false otherwise
     * @throws IllegalArgumentException thrown when the campus is null
     */
    public Boolean addCampusLog(ICampus toAdd) throws IllegalArgumentException
    {
        if(toAdd == null)
        {
            throw new IllegalArgumentException("can't add null campus log");
        }
        else
        {
            Logger log = toAdd.getLog();
            return m_campusLogs.put(toAdd.getCampusCode(), log) != null;
        }        
    }
    
    /**
     * Adds a user to the log file
     * @param toAdd the user to add
     * @return true if the add was successful, false otherwise
     * @throws IllegalArgumentException thrown when the user is null
     */
    public Boolean addUser(IUser toAdd) throws IllegalArgumentException
    {
        if(toAdd == null)
        {
            throw new IllegalArgumentException("can't add a null user");
        }
        else
        {
            return m_users.put(toAdd.viewUserID(), toAdd) != null;
        }
    }
    
    /**
     * Adds a campus to the log file
     * @param toAdd the campus to add
     * @return true if add was successful, false otherwise
     * @throws IllegalArgumentException thrown when the campus is null
     */
    public Boolean addCampus(ICampus toAdd) throws IllegalArgumentException
    {
        if(toAdd == null)
        {
            throw new IllegalArgumentException("can't add a null campus");
        }
        else
        {
            return m_allCampuses.put(toAdd.getCampusCode(), toAdd) != null;
        }                        
    } 

    /**
     * Get a map of the room logs stored in the file
     * @return the room log map
     */
    public Map<String, Logger> getRoomLogs() 
    {
        return m_roomLogs;
    }

    /**
     * Gets the map of buildings logs stored in the file
     * @return the buildings log map
     */
    public Map<String, Logger> getBuildingLogs() 
    {
        return m_buildingLogs;
    }

    /**
     * Get a map of the campus logs
     * @return the campus logs map
     */
    public Map<String, Logger> getCampusLogs() 
    {
        return m_campusLogs;
    }   
    
    /**
     * View the room logs as a collection
     * @return a collection representation of the room logs
     */
    public Collection<Logger> viewRoomLogs()
    {       
        return convertToList(m_buildingLogs);
    }    
    
    /**
     * View the buildings logs as a collection
     * @return a collection representation of the building logs
     */
    public Collection<Logger> viewBuildingLogs()
    {
        return convertToList(m_buildingLogs);
    }
    
    /**
     * View the campus logs as a collection
     * @return a collection representation of the campus logs
     */
    public Collection<Logger> viewCampusLogs()
    {
        return convertToList(m_campusLogs);
    }
    
    /**
     * View the users as a collection
     * @return a collection of the users
     */
    public Collection<IUser> viewUsers()
    {
        return convertToList(m_users);
    }
    
    /**
     * Get the campus model stored in the file
     * @return the campuses model
     */
    public Map<String, ICampus> getCampusesModel()
    {
        return m_allCampuses;        
    }
    
    /**
     * Get the users stored in the file
     * @return the users in the log file
     */
    public Map<Integer, IUser> getUsers()
    {
        return m_users;
    }
    
    private <K, T> Collection<T> convertToList(Map<K, T> map)
    {
        List<T> copiedValues = new ArrayList<>();
        
        map.values().stream().forEach((current) -> 
        {
            copiedValues.add(current);
        });
        
        return copiedValues;
    }
    
    /**
     * Retrieve the creation date and time of the log file
     * @return the creationDateTime
     */
    public LocalDateTime getCreationDate()
    {
        return m_creationDate;
    }   
    
    /**
     * A simple method to close the log file, this will wipe out all data stored in the opened file
     */
    public void closeFile()
    {
        m_allCampuses.clear();
        m_buildingLogs.clear();
        m_campusLogs.clear();
        m_users.clear();
        m_roomLogs.clear();
    }

    @Override
    public void onRead() 
    {
        m_allCampuses.values().stream().forEach((campus) -> 
        {
            campus.onRead();
        });
    }

    @Override
    public void onWrite() 
    {
    }
}
