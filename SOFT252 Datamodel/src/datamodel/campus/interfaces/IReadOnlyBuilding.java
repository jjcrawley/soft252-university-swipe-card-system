/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datamodel.campus.interfaces;

import datamodel.campus.rooms.interfaces.IReadOnlyRoom;
import datamodel.restrictions.OperatingMode;
import java.util.List;
import serialisation.ISerialisable;

/**
 * Defines basic functionality for a read only building
 * @author jjcrawley
 */
public interface IReadOnlyBuilding extends ISerialisable
{

    /**
     * Get a read only copy of all the rooms in the building
     * @return a list of all the rooms stored in the building
     */
    public List<IReadOnlyRoom> viewRooms();

    /**
     * Get the buildings name
     * @return the buildings name
     */
    public String getBuildingName();

    /**
     * Get the building code
     * @return the building code
     */
    public String getBuildingCode();

    /**
     * Get the current operating mode of the building
     * @return the current operating mode
     */
    public OperatingMode getCurrentMode();    
}
