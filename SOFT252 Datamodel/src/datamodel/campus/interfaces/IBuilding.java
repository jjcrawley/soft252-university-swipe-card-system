/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datamodel.campus.interfaces;

import datamodel.campus.rooms.interfaces.IModeChangeable;
import datamodel.campus.rooms.interfaces.IRoom;
import datamodel.logging.Logger;
import java.util.List;
import observer.interfaces.IObservable;

/**
 * Defines basic functionality for a building that can be observed and have its mode changed
 * @author jjcrawley
 */
public interface IBuilding extends IBuildingEditable, IObservable, IModeChangeable
{
    
    /**
     * Retrieve a copy of the room logs stored in the building
     * @return a list of room loggers
     */
    public List<Logger> getRoomLogs() throws IllegalArgumentException;

    /**
     * Retrieve the buildings log
     * @return the building log
     */
    public Logger getLog();

    /**
     * Allows you to add a room to the building
     * @param toAdd the room to add
     * @return true if the addition was successful, false otherwise
     */
    public Boolean addRoom(IRoom toAdd) throws IllegalArgumentException;

    /**
     * Remove a room from the building 
     * @param roomCode the room code of the room to remove
     * @return true if the removal was successful, false otherwise
     */
    public Boolean removeRoom(String roomCode) throws IllegalArgumentException;     
    
    
    /**
     * Clears the building
     * Use this to perform building cleaning procedures
     */
    public void clearBuilding();
}
