/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datamodel.campus.interfaces;

import datamodel.restrictions.OperatingMode;
import java.util.List;
import serialisation.ISerialisable;

/**
 * Defines basic functionality that allows for a campus that can have it's data read
 * @author jjcrawley
 */
public interface IReadOnlyCampus extends ISerialisable
{

    /**
     * Retrieve the campus code
     * @return the campuses code
     */
    public String getCampusCode();

    /**
     * get a readonly copy of the buildings stored in the campus
     * @return the buildings stored in the campus
     */
    public List<IReadOnlyBuilding> viewBuildings();

    /**
     * Get the campuses current operating mode
     * @return the campuses current operating mode
     */
    public OperatingMode getCampusMode();
}
