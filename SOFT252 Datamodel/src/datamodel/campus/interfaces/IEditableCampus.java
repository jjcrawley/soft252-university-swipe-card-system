/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datamodel.campus.interfaces;

import java.util.List;

/**
 * Defines basic functionality for a campus that can have its details edited
 * @author jjcrawley
 */
public interface IEditableCampus extends IReadOnlyCampus
{

    /**
     * Allows you retrieve a building that can have it's details edited and read
     * @param buildingCode the building code that you want to retrieve
     * @return the building from the campus, returns null if the building doesn't exist
     */
    public IBuilding getBuilding(String buildingCode);

    /**
     * Change the current campuses code
     * @param campusCode the new campus code
     */
    public void setCampusCode(String campusCode);

    /**
     * Retrieve a copy of the buildings as editable buildings
     * @return the list of stored buildings
     */
    public List<IBuilding> getBuildings();

    /**
     * Allows you to refactor a buildings code given the buildings code
     * @param oldCode the building code you want to change
     * @param newCode the code to change it to
     */
    public void refactorBuildingCode(String oldCode, String newCode);

    /**
     * Allows you to refactor a building code given the building
     * @param toRefactor the building to refactor 
     * @param newCode the new building code
     */
    public void refactorBuildingCode(IBuilding toRefactor, String newCode);

    /**
     * Allows you to move a building from one campus to another
     * @param destination the campus to move to
     * @param buildingCode the code of the building to move
     * @return true if the move was successful, false otherwise
     */
    public boolean moveBuildingToCampus(ICampus destination, String buildingCode);
}
