/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datamodel.campus.interfaces;

import datamodel.campus.rooms.interfaces.IModeChangeable;
import datamodel.logging.Logger;
import java.util.List;
import observer.interfaces.IObservable;

/**
 * Defines basic functionality for a Campus
 * @author jjcrawley
 */
public interface ICampus extends IEditableCampus, IObservable, IModeChangeable
{

    /**
     * Allows you add a building to the current campus
     * @param toAdd the building to add
     * @return true if the addition was successful, false otherwise
     */
    public Boolean addBuilding(IBuilding toAdd); 

    /**
     * Remove a building from the campus given the buildings code
     * @param buildingCode the code of the building to remove
     * @return true if the removal was successful, false otherwise
     */
    public Boolean removeBuilding(String buildingCode);

    /**
     * Get the log of the campus
     * @return the campuses log
     */
    public Logger getLog();

    /**
     * Retrieve all of the loggers stored in the buildings within the campus
     * @return a list of all the building loggers
     */
    public List<Logger> getBuildingLogs();

    /**
     * Retrieve all of the room loggers stored in the campuses buildings
     * @return a copy of all of the room loggers
     */
    public List<Logger> getRoomLogs();
    
    /**
     * Clears the campus
     * Use this when you wish to clear the campus
     */
    public void clearCampus();
}