/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datamodel.campus.interfaces;

import datamodel.campus.rooms.interfaces.IRoom;
import java.util.List;


/**
 * Defines basic functionality for a building that can have its details edited
 * @author jjcrawley
 */
public interface IBuildingEditable extends IReadOnlyBuilding
{

    /**
     * Set the building code
     * @param buildingCode the new building code
     */
    public void setBuildingCode(String buildingCode);

    /**
     * Set the buildings name
     * @param buildingName the new building name
     */
    public void setBuildingName(String buildingName);

    /**
     * Get a room from the building, not read only
     * @param roomCode the room code of the room to retrieve
     * @return the room with the corresponding code
     */
    public IRoom getRoom(String roomCode);

    /**
     * Retrieve a list of rooms stored in the building
     * @return the list of rooms in the building
     */
    public List<IRoom> getRooms();

    /**
     * Allows you to refactor a room code given a room
     * @param toRefactor the room to refactor
     * @param newRoomCode the new room code
     */
    public void refactorRoomCode(IRoom toRefactor, String newRoomCode);

    /**
     * Allows you to refactor a room code given the original code
     * @param oldCode the code of the building to refactor
     * @param newRoomCode the new room code
     */
    public void refactorRoomCode(String oldCode, String newRoomCode);

    /**
     * Allows you to move a room to a building
     * @param roomCode the room to remove given the room code
     * @param moveTo the building to move the room to
     */
    public void moveRoomToBuilding(String roomCode, IBuilding moveTo);
}
