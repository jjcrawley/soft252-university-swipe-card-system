/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datamodel.campus;

import datamodel.campus.interfaces.IBuilding;
import datamodel.campus.rooms.interfaces.IReadOnlyRoom;
import datamodel.campus.rooms.interfaces.IRoom;
import datamodel.logging.Logger;
import datamodel.logging.ModeChangeTimeStamp;
import datamodel.logging.TimeStamp;
import datamodel.restrictions.OperatingMode;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import observer.ObservableEvent;
import observer.interfaces.IObserver;

/**
 * A standard building that implements the IBuilding interface
 * The implementation provides controls to create and maintain a list of rooms stored inside a building
 * @author jjcrawley
 */
public class Building implements IBuilding
{
    //private final List<Room> rooms;
    //private final RoomsManager rooms;
    private final Map<String, IRoom> m_rooms;
    private String m_buildingName = "Unknown";
    private String m_buildingCode = "Unknown";
    private OperatingMode e_currentMode = OperatingMode.Normal;
    private transient Logger m_buildingLog;
    private transient ObservableEvent onChange = new ObservableEvent();
    
    /**
     * Create an empty building
     */
    public Building()
    {
        //rooms = new ArrayList<>();
        m_buildingLog = new Logger();
        //rooms = new RoomsManager();
        m_rooms = new HashMap<>();
    }  
    
    /**
     * Creates an empty building with the prescribed name and building code
     * @param name The buildings name, can't be null
     * @param buildingCode the buildings code, can't be null or empty
     * @throws IllegalArgumentException thrown when either parameter is null or the building code is empty 
     */
    public Building(String name, String buildingCode) throws IllegalArgumentException
    {
        if(name == null)
        {
            throw new IllegalArgumentException("can't create a building name can't be null");
        }
        else if(buildingCode == null || buildingCode.isEmpty())
        {
            throw new IllegalArgumentException("can't create a building without a code");
        }
        else
        {
            m_buildingName = name;
            m_buildingCode = buildingCode;            
            m_buildingLog = new Logger();
            m_rooms = new HashMap<>();               
        }   
    }

    @Override
    public List<IReadOnlyRoom> viewRooms() 
    {
        List<IReadOnlyRoom> copyOfRooms = new ArrayList<>();
                
        m_rooms.values().stream().forEach((currentRoom) -> 
        {
            copyOfRooms.add(currentRoom);
        });
        
        return copyOfRooms;
    }    

    @Override
    public List<IRoom> getRooms() 
    {
        List<IRoom> copyOfRooms = new ArrayList<>();
                
        m_rooms.values().stream().forEach((currentRoom) -> 
        {
            copyOfRooms.add(currentRoom);
        });
        
        return copyOfRooms;        
    }   
    
    @Override
    public String getBuildingName() 
    {
        return m_buildingName;
    }

    @Override
    public void setBuildingName(String buildingName) throws IllegalArgumentException
    {
        if(buildingName == null || buildingName.isEmpty())
        {
            throw new IllegalArgumentException("the building must have a name");
        }
        else
        {
            m_buildingName = buildingName;
        }
    }

    @Override
    public String getBuildingCode() 
    {
        return m_buildingCode;
    }

    @Override
    public void setBuildingCode(String buildingCode) throws IllegalArgumentException
    {
        if(buildingCode == null || buildingCode.isEmpty())
        {
            throw new IllegalArgumentException("the building must have a code");
        } 
        else
        {
            m_buildingCode = buildingCode;
            //notifyObservers();
        }
    }

    @Override
    public OperatingMode getCurrentMode() 
    {
        return e_currentMode;
    }

    @Override
    public Logger getLog() 
    {
        return m_buildingLog;
    }   
    
    @Override
    public List<Logger> getRoomLogs()
    {
        List<Logger> copyOfRoomLoggers = new ArrayList<>(m_rooms.size());
        
        m_rooms.values().stream().forEach((currentRoom) -> 
        {
            copyOfRoomLoggers.add(currentRoom.getLog());
        });
        
        return copyOfRoomLoggers;
    }
    
    @Override
    public Boolean addRoom(IRoom toAdd) throws IllegalArgumentException
    {
        if(toAdd == null)
        {
            throw new IllegalArgumentException("can't add a null building");            
        }
        else if(m_rooms.containsKey(toAdd.getRoomCode()) || m_rooms.containsValue(toAdd))
        {
            return false;
        }
        else
        {
            m_rooms.put(toAdd.getRoomCode(), toAdd);
            
            toAdd.setBuilding(this);
            toAdd.registerObserver(CentralServer.getInstance());
            toAdd.changeMode(e_currentMode);
            notifyObservers();
            return true;                        
        }        
    } 
    
    @Override
    public Boolean removeRoom(String roomCode) 
    {  
        if(roomCode == null || roomCode.isEmpty())
        {
            return false;
        }
        else if(m_rooms.containsKey(roomCode))
        {
            m_rooms.remove(roomCode).removeObserver(CentralServer.getInstance());
            notifyObservers();
            return true;
        }       
        
        return false;                
    }
    
    @Override
    public void moveRoomToBuilding(String roomCode, IBuilding moveTo) throws IllegalArgumentException
    {
        if(moveTo == null || roomCode == null || roomCode.isEmpty())
        {
            throw new IllegalArgumentException("can't move room with the current parameters");
        }
        else
        {
            IRoom roomToMove;
            
            roomToMove = m_rooms.get(roomCode);
            
            m_rooms.remove(roomCode);
                        
            roomToMove.setBuilding(moveTo);
            moveTo.addRoom(roomToMove);
            
            notifyObservers();
        }
    }
    
    @Override
    public void refactorRoomCode(IRoom toRefactor, String newRoomCode) throws IllegalArgumentException
    {
        if(toRefactor == null || newRoomCode == null || newRoomCode.isEmpty())
        {
            throw new IllegalArgumentException("can't refactor with the current parameters");                        
        }
        else
        {
            String oldCode = toRefactor.getRoomCode();

            m_rooms.remove(oldCode);

            toRefactor.setRoomCode(newRoomCode);

            addRoom(toRefactor);
            
            notifyObservers();
        }
    }
    
    @Override
    public void refactorRoomCode(String oldCode, String newRoomCode) throws IllegalArgumentException
    {
        if(oldCode == null || newRoomCode == null)
        {
            throw new IllegalArgumentException("can't refactor with null parameters");
        }
        
        if(oldCode.isEmpty() || newRoomCode.isEmpty())
        {
            throw new IllegalArgumentException("can't refactor to an empty string");               
        }
        else
        {
            IRoom roomToRefactor = m_rooms.get(oldCode);
            
            if(roomToRefactor == null)
            {
                throw new IllegalArgumentException("the old room isn't present in the building");
            }
            else
            {            
                m_rooms.remove(oldCode);

                roomToRefactor.setRoomCode(newRoomCode);

                addRoom(roomToRefactor);
                
                notifyObservers();
            }    
        }
    }
    
    @Override
    public IRoom getRoom(String roomCode)
    {
        return m_rooms.get(roomCode);
    }  
    
    @Override
    public void clearBuilding()
    {
        m_rooms.clear();
    }

    @Override
    public void changeMode(OperatingMode changeTo) throws IllegalArgumentException
    {
        if(changeTo != null)
        {
            e_currentMode = changeTo;
            
            m_rooms.values().stream().forEach((currentRoom) -> 
            {
                currentRoom.changeMode(changeTo);
            });
            
            notifyObservers();
        }
        else
        {
            throw new IllegalArgumentException("the building must have a operating mode");
        }
    }

    @Override
    public void logModeChange() 
    {
        String details = "Building: " + m_buildingCode + " mode was changed to " + e_currentMode;
        String reason = "No details specified";
        
        TimeStamp modeChange = new ModeChangeTimeStamp(details, reason);
        m_buildingLog.makeEntry(modeChange);
        //notifyObservers();
    }   

    @Override
    public void logModeChange(String reason) throws IllegalArgumentException
    {
        String details = "Building: " + m_buildingCode + " mode was changed to " + e_currentMode;       
        
        if(reason == null)
        {
            throw new IllegalArgumentException("Reason can't be null");
        }
        else
        {        
            TimeStamp modeChange = new ModeChangeTimeStamp(details, reason);
            m_buildingLog.makeEntry(modeChange);
            //notifyObservers();        
        }
    }

    @Override
    public void logModeChange(String reason, String details) throws IllegalArgumentException
    {
        if(reason == null || details == null)
        {
            throw new IllegalArgumentException("can't log a mode change with null details");
        }
        else
        {
            TimeStamp modeChange = new ModeChangeTimeStamp(details, reason);
            m_buildingLog.makeEntry(modeChange);            
            //notifyObservers();
        }   
    }
    
    /**
     * Get a string representation of the building: code + name
     * @return formatted string of the buildings details 
     */
    @Override
    public String toString() 
    {
        return "" + m_buildingCode + " " + m_buildingName;
    }   

    @Override
    public final Boolean registerObserver(IObserver observer) 
    {
        if(onChange == null)
        {
            onChange = new ObservableEvent();
        }
        
        return onChange.registerObserver(observer);
    }

    @Override
    public final Boolean removeObserver(IObserver toRemove) 
    {
        if(onChange == null)
        {
            return false;
        }
        
        return onChange.removeObserver(toRemove);
    }

    @Override
    public final void notifyObservers() 
    {
        if(onChange == null)
        {
            return;
        }
        
        onChange.notifyObservers();
    }

    @Override
    public List<IObserver> getObservers() 
    {
        return onChange.getObservers();
    }   

    @Override
    public void onRead() 
    {
        m_buildingLog = new Logger();
        onChange = new ObservableEvent();
        
        m_rooms.values().stream().forEach((room) -> 
        {
            room.onRead();
        });
    }

    @Override
    public void onWrite() 
    {
        
    }       
}