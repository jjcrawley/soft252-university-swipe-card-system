/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datamodel.campus;

import datamodel.campus.interfaces.IBuilding;
import datamodel.campus.interfaces.ICampus;
import datamodel.campus.interfaces.IReadOnlyCampus;
import datamodel.campus.rooms.RoomBuilder;
import datamodel.campus.rooms.RoomType;
import datamodel.campus.rooms.interfaces.IRoom;
import datamodel.details.AddressBuilder;
import datamodel.details.interfaces.IAddress;
import datamodel.logging.LogFile;
import datamodel.users.*;
import datamodel.users.interfaces.IReadOnlyUser;
import datamodel.users.interfaces.IUser;
import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.logging.Level;
import observer.ObservableEvent;
import observer.interfaces.IObservable;
import observer.interfaces.IObserver;

/**
 * A central server implementation that is designed to hold and maintain a hash map of users and campuses
 * The implementation uses the singleton design pattern
 * @author jjcrawley
 */
public class CentralServer implements Serializable, IObservable, IObserver
{
    //private final List<Campus> campuses;    
    //private final List<User> users;
    private final Map<String, ICampus> m_campuses;
    private final Map<Integer, IUser> m_users;
    
    private static CentralServer s_serverInstance = new CentralServer();
    private final ObservableEvent onChangeEvent;
    
    private CentralServer()
    {
        onChangeEvent = new ObservableEvent();
        //campuses = new ArrayList<>();
        //users = new ArrayList<>();
        
        m_campuses = new HashMap<>();
        m_users = new HashMap<>();       
    } 
    
    /**
     * Generate some test data
     */
    public void generateTestData()
    {
        generateTestUsers();
        generateTestCampusModel();        
    }
    
    /**
     * This is purely to generate user test data
     */
    private void generateTestUsers()
    {
        AddressBuilder builder = AddressBuilder.getAddressBuilder();
        
        IAddress address = builder.setAddressLine1("12 somewhere place").
                setCity("Middle of nowhere").
                setCounty("Somewhere").
                setPostCode("ACode").
                buildAddress();
        
        addUser(UserBuilder.getUserBuilder().
                setUserID(253232332).
                setUserAddress(address).
                setUserAge(20).
                setUserRole(UserRole.Security).
                setUserGender(Gender.Female).
                setUserName(new UserName("Belle", "Farlock")).
                buildUser());
        
        address = builder.setAddressLine1("13 somewhere place").                
                buildAddress();
        
        addUser(UserBuilder.getUserBuilder().
                setUserID(202020202).
                setUserAddress(address).
                setUserAge(25).
                setUserRole(UserRole.ContractCleaner).
                setUserGender(Gender.Female).
                setUserName(new UserName("Jane", "NotFarlock")).
                buildUser());
        
        address = builder.setAddressLine1("15 somewhere place").                
                buildAddress(); 
        
        addUser(UserBuilder.getUserBuilder().
                setUserID(132626226).
                setUserAddress(address).
                setUserAge(23).
                setUserGender(Gender.Male).
                setUserName(new UserName("James", "AName","NearLock")).
                buildUser(UserRole.Staff));
        
        address = builder.setAddressLine1("17 somewhere place").
                setCity("ACity").
                setCounty("SomeCounty").
                setPostCode("AnotherPostCode").
                setAddressLine2("InAnApartment").
                buildAddress(); 
        
        addUser(UserBuilder.getUserBuilder().
                setUserID(326223226).
                setUserAddress(address).
                setUserAge(23).
                setUserGender(Gender.Male).
                setUserName(new UserName("Timmy", "AnotherName","StudentLock")).
                buildUser(UserRole.Student));
        
        address = builder.setAddressLine1("19 nowhere place").                
                setAddressLine2("").
                buildAddress(); 
        
        addUser(UserBuilder.getUserBuilder().
                setUserID(326223226).
                setUserAddress(address).
                setUserAge(23).
                setUserGender(Gender.Male).
                setUserName(new UserName("Jimmy", "Person","Surname")).
                buildUser(UserRole.Guest));
        
        address = builder.setAddressLine1("20 nowhere place").                
                buildAddress(); 
        
        addUser(UserBuilder.getUserBuilder().
                setUserID(326423226).
                setUserAddress(address).
                setUserAge(30).
                setUserGender(Gender.Male).
                setUserName(new UserName("Jimmy","Another")).
                buildUser(UserRole.EmergencyResponder));
        
        address = builder.setAddressLine1("10 nowhere place").                
                buildAddress(); 
        
        addUser(UserBuilder.getUserBuilder().
                setUserID(455445454).
                setUserAddress(address).
                setUserAge(20).
                setUserGender(Gender.Female).
                setUserName(new UserName("Tina","Thompson")).
                buildUser(UserRole.Manager));
    }
    
    /**
     * This is purely to generate a campus test model
     */
    private void generateTestCampusModel()
    {
        RoomBuilder builder = RoomBuilder.getRoomBuilder();
        
        ICampus newCampus = new Campus("Plymouth Campus");
        
        IBuilding newBuilding = new Building("Babbage", "BBG");
        
        newBuilding.addRoom(builder.setBuilding(newBuilding).
                setRoomCode("BBG001").
                setRoomFloor(0).
                buildRoom(RoomType.StudentLab));
        
        newBuilding.addRoom(builder.setBuilding(newBuilding).
                setRoomCode("BBG002").
                setRoomFloor(0).
                buildRoom(RoomType.ResearchLab));    
        
        newBuilding.addRoom(builder.setBuilding(newBuilding).
                setRoomCode("BBG003").
                setRoomFloor(0).
                buildRoom(RoomType.LectureHall));     
        
        newBuilding.addRoom(builder.setBuilding(newBuilding).
                setRoomCode("BBG004").
                setRoomFloor(0).
                buildRoom(RoomType.SecureRoom));     
        
        newBuilding.addRoom(builder.setBuilding(newBuilding).
                setRoomCode("BBG005").
                setRoomFloor(0).
                buildRoom(RoomType.StaffRoom));     
        
        newCampus.addBuilding(newBuilding);
        
        newBuilding = new Building("Smeaton", "SMT");
        
        newBuilding.addRoom(builder.setBuilding(newBuilding).
                setRoomCode("SMT001").
                setRoomFloor(0).
                buildRoom(RoomType.StaffRoom));
        
        newBuilding.addRoom(builder.setBuilding(newBuilding).
                setRoomCode("SMT002").
                setRoomFloor(0).
                buildRoom(RoomType.StudentLab));
        
        newCampus.addBuilding(newBuilding);
        
        addCampus(newCampus);
        
        newCampus = new Campus("Plympton Campus");
        
        newBuilding = new Building("Building", "BLD");
        
        newBuilding.addRoom(builder.setBuilding(newBuilding).
                setRoomCode("BLD001").
                setRoomFloor(0).
                buildRoom(RoomType.StudentLab));
        
        newBuilding.addRoom(builder.
                setBuilding(newBuilding).
                setRoomCode("BLD002").
                setRoomFloor(0).
                buildRoom(RoomType.ResearchLab));        
        
        newCampus.addBuilding(newBuilding);
        
        newBuilding = new Building("Other", "OTR");
        
        newBuilding.addRoom(builder.setBuilding(newBuilding).
                setRoomCode("OTR001").
                setRoomFloor(0).
                buildRoom(RoomType.StaffRoom));
        
        newBuilding.addRoom(builder.setBuilding(newBuilding).
                setRoomCode("OTR002").
                setRoomFloor(0).
                buildRoom(RoomType.StudentLab));
        
        newCampus.addBuilding(newBuilding);
        
        addCampus(newCampus);
    }
    
    /**
     * Gets a list of the stored campuses
     * @return the campuses as a list
     */
    public List<ICampus> getCampuses()
    {
        List<ICampus> copiedCampuses = new ArrayList<>();
        
        m_campuses.values().stream().forEach((currentCampus) ->
        {
            copiedCampuses.add(currentCampus);
        });
        
        return copiedCampuses;
    }
    
    /**
     * View the campuses as a read only representation
     * @return a list of read only campuses
     */
    public List<IReadOnlyCampus> viewCampuses()
    {
        List<IReadOnlyCampus> copiedCampuses = new ArrayList<>();
        
        m_campuses.values().stream().forEach((currentCampus) -> 
        {
            copiedCampuses.add(currentCampus);
        });
        
        return copiedCampuses;        
    }

    /**
     * Retrieve a list representation of the users stored within
     * @return a list of the stored users
     */
    public List<IUser> getUsers() 
    {
        List<IUser> copiedUsers = new ArrayList<>();
        
        m_users.values().stream().forEach((currentUser) -> 
        {
            copiedUsers.add(currentUser);
        });
        
        return copiedUsers;        
    }
    
    /**
     * Returns a list of users as a read only representation
     * @return the users as a read only representation
     */
    public List<IReadOnlyUser> viewUsers() 
    {
        List<IReadOnlyUser> copiedUsers = new ArrayList<>();
        
        m_users.values().stream().forEach((currentUser) -> 
        {
            copiedUsers.add(currentUser);
        });
        
        return copiedUsers;        
    }
    
    /**
     * Add a user to the central server
     * @param toAdd the user to add
     * @return true if the addition was successful, false otherwise
     */
    public Boolean addUser(IUser toAdd)
    {
        if(toAdd == null || toAdd.getClass() == EmptyUser.class)
        {
            return false;
        }
        else if(m_users.containsKey(toAdd.viewUserID()) || m_users.containsValue(toAdd))
        {
            return false;
        }
        else
        {
            m_users.put(toAdd.viewUserID(), toAdd);
            notifyObservers();
            return true;                        
        }               
    }   
    
    /**
     * Removes a user from the server given their user id
     * @param userID the id of the user to remove
     * @return true if the removal was successful, false otherwise
     */
    public Boolean removeUser(int userID)
    {
        if(userID <= 0)
        {
            return false;
        }
        else if(m_users.containsKey(userID))
        {
            IUser user = m_users.remove(userID);
            user.clearUser();
            
            notifyObservers();
            return true;
        }       
        
        return false;
    }
    
    /**
     * Get a user from the central server given their user id
     * @param userID the user id of the user to retrieve
     * @return the user
     */
    public IUser getUser(int userID)
    {
        return m_users.get(userID);
    }  
    
    /**
     * Retrieve a campus from the central server given its campus code
     * @param campusCode the campus code of the campus to retrieve
     * @return the campus
     */
    public ICampus getCampus(String campusCode)
    {
        return m_campuses.get(campusCode);
    }
    
    /**
     * Adds a campus to the central server
     * @param toAdd the campus to add
     * @return true if the addition was successful, false otherwise
     */
    public Boolean addCampus(ICampus toAdd)
    {
        if(toAdd == null)
        {
            return false;
        }
        else if(m_campuses.containsKey(toAdd.getCampusCode()) || m_campuses.containsValue(toAdd))
        {
            return false;
        }
        else
        {
            m_campuses.put(toAdd.getCampusCode(), toAdd);
            toAdd.registerObserver(this);
            notifyObservers();
            return true;                        
        }              
    }    
    
    /**
     * Removes a campus from the central server given its campus code
     * @param campusCode the campus code of the campus to remove
     * @return true if the campus was removed, false otherwise
     */
    public Boolean removeCampus(String campusCode)
    {
        if(campusCode.isEmpty())
        {
            return false;
        }
        else if(m_campuses.containsKey(campusCode))
        {
            ICampus removedCampus = m_campuses.remove(campusCode);
            removedCampus.removeObserver(this);
            removedCampus.clearCampus();
            
            notifyObservers();
            return true;
        }       
        
        return false;        
    }
    
    /**
     * Allows you to refactor a campus code given a new code and the campus to refactor
     * @param newCode the new campus code
     * @param toRefactor the campus to refactor
     * @return true if the refactor was successful, false otherwise
     * @throws IllegalArgumentException thrown when the new code is empty or null, or the campus is null
     */
    public boolean refactorCampus(String newCode, ICampus toRefactor) throws IllegalArgumentException
    {
        if(toRefactor ==  null)
        {
            throw new IllegalArgumentException("toRefactor can't be null");
        }        
        else if(m_campuses.containsKey(toRefactor.getCampusCode()))
        {    
            m_campuses.remove(toRefactor.getCampusCode());
            
            toRefactor.setCampusCode(newCode);            
            notifyObservers();
            
            return addCampus(toRefactor);
        }
        else
        {
            return false;            
        }
    }
    
    /**
     * Refactor a campus code given the campuses old code and its new code
     * @param newCode the code to refactor to
     * @param oldCode the code of the campus to refactor
     * @return true if the refactor was successful, false otherwise
     */
    public boolean refactorCampus(String newCode, String oldCode)
    {          
        if(m_campuses.containsKey(oldCode))
        {
            ICampus campusToRefactor = m_campuses.get(oldCode);
            
            m_campuses.remove(oldCode);
            
            campusToRefactor.setCampusCode(newCode);
            notifyObservers();
   
            return addCampus(campusToRefactor);
        }
        else
        {
            return false;            
        }
    }
    
    /**
     * Load in a log file
     * @param loadedFile the file that you wish to load
     * @return the loaded logfile
     * @throws IOException thrown when their is an unexpected read error
     * @throws ClassNotFoundException thrown when the class type of the file is unknown
     * @throws ClassCastException thrown when the file trying to be loaded isn't a log file
     */
    public LogFile loadLogFile(File loadedFile) throws IOException, ClassNotFoundException, ClassCastException
    {
        LogFile loadedLogFile;       
        
        try(ObjectInputStream inputStream = new ObjectInputStream(
                new BufferedInputStream(
                new FileInputStream(loadedFile))))
        {
            Object loadedData = inputStream.readObject();
            
            loadedLogFile = (LogFile)loadedData;
            loadedLogFile.onRead();
        }
        
        return loadedLogFile;       
    }
    
    /**
     * Save the central server data into a file location
     * this will automatically write a text file of the logs into the same folder as the file location
     * @param fileLocation the file to save to
     * @throws IOException Thrown when their is an unexpected error in the data stream
     */
    public void saveData(File fileLocation) throws IOException 
    {          
        LogFile savedFile = new LogFile();
        
        m_campuses.values().stream().forEach((currentCampus) -> 
        {
            currentCampus.getBuildings().stream().forEach((currentBuilding) -> 
            {
                currentBuilding.getRooms().stream().forEach((currentRoom) -> 
                {
                    savedFile.addRoomLog(currentRoom);
                });
                
                savedFile.addBuildingLog(currentBuilding);
            });
            
            savedFile.addCampusLog(currentCampus);
            savedFile.addCampus(currentCampus);
        });
        
        m_users.values().stream().forEach((currentUser) -> 
        {
            savedFile.addUser(currentUser);
        });        
                
        try(ObjectOutputStream outStream = new ObjectOutputStream(
                new BufferedOutputStream(
                new FileOutputStream(fileLocation))))
        {
            outStream.writeObject(savedFile);            
        } 
        
        writeLogsToTextFile(new File(fileLocation.getParentFile(), "Log.txt"));               
    }
    
    /**
     * Allows you to save out all of the campus, building and room logs into a text file
     * This will overwrite the file 
     * @param fileLocation the file to save the text file to
     * @throws IOException thrown when an unexpected stream error occurs
     */
    public void writeLogsToTextFile(File fileLocation) throws IOException
    {
        try(BufferedWriter writer = new BufferedWriter(new FileWriter(fileLocation)))
        {
            m_campuses.values().stream().forEach((currentCampus) -> 
            {
                writeCampusLogToText(writer, currentCampus);
                
                currentCampus.getBuildings().stream().forEach((currentBuilding) -> 
                {
                    writeBuildingLogToText(writer, currentBuilding);
                    
                    currentBuilding.getRooms().stream().forEach((currentRoom) -> 
                    {
                        writeRoomLogToText(writer, currentRoom);
                    });                   
                });              
            });
        }       
    }
    
    /**
     * This will write a campus log to a text file using a text stream
     * @param writer the stream to write with
     * @param currentCampus the campus to log
     */
    public void writeCampusLogToText(BufferedWriter writer, ICampus currentCampus) 
    {
        currentCampus.getLog().viewEntries().forEach((entry) -> 
        {                
            try 
            {
                writer.write(entry.getTimeStampDetails());
                writer.newLine();
                //writer.append(entry.getTimeStampDetails() + "\n");
            } 
            catch (IOException ex) 
            {
                java.util.logging.Logger.getLogger(CentralServer.class.getName()).log(Level.SEVERE, null, ex);
            }
        });        
    }
    
    /**
     * Writes a building log to a text file using the prescribed writer
     * @param writer the stream to write with
     * @param building the building to write
     */
    public void writeBuildingLogToText(BufferedWriter writer, IBuilding building)
    {
        building.getLog().viewEntries().stream().forEach((entry) -> 
        {
            try 
            {
                writer.write(entry.getTimeStampDetails() + "\n");
                writer.newLine();
                //writer.append(entry.getTimeStampDetails() + "\n");
            } 
            catch (IOException ex) 
            {
                java.util.logging.Logger.getLogger(CentralServer.class.getName()).log(Level.SEVERE, null, ex);
            }
        });        
    }
    
    /**
     * Writes a room log to a text file using the prescribed writer
     * @param writer the stream to write to
     * @param room the room to log
     */
    public void writeRoomLogToText(BufferedWriter writer, IRoom room)
    {
        room.getLog().viewEntries().stream().forEach((entry) -> 
        {                            
            try 
            {
                writer.write(entry.getTimeStampDetails() + "\n");
                writer.newLine();
                //writer.append(entry.getTimeStampDetails() + "\n");
            } 
            catch (IOException ex) 
            {
                java.util.logging.Logger.getLogger(CentralServer.class.getName()).log(Level.SEVERE, null, ex);
            }                                                        
        });  
    }
    
    /**
     * Allows you to load in a log file, this will load in the campuses and the users
     * @param loadFrom the log file to load
     */
    public void loadInCampusAndUsers(LogFile loadFrom)
    {
        loadCampuses(loadFrom);
        loadUsers(loadFrom);
    } 
    
    /**
     * Load in a campus model from the given log file
     * @param logFile the log file to load from
     */
    public void loadCampuses(LogFile logFile)
    {        
        m_campuses.clear();        
        
        for(Entry<String, ICampus> currentCampus : logFile.getCampusesModel().entrySet())
        {
            currentCampus.getValue().registerObserver(this);
            m_campuses.put(currentCampus.getKey(), currentCampus.getValue());
            
            currentCampus.getValue().getBuildings().stream().forEach((building) -> 
            {
                building.registerObserver(this);
                
                building.getRooms().stream().forEach((room) -> 
                {
                    room.registerObserver(this);
                });               
            });            
        }   
        
        notifyObservers();
    }
    
    /**
     * Load in users from the given log file
     * @param logFile the log file to load from
     */
    public void loadUsers(LogFile logFile)
    {
        m_users.clear();      
        
        for(Entry<Integer, IUser> currentUser : logFile.getUsers().entrySet())
        {
            m_users.put(currentUser.getKey(), currentUser.getValue());
        }
        
        notifyObservers();
    }
    
    /**
     * Gets the central server
     * @return the central server instance
     */
    public static CentralServer getInstance()
    {
        if(s_serverInstance == null)
        {
            s_serverInstance = new CentralServer();
        }
        
        return s_serverInstance;
    } 
    
    /**
     * disposes of the central server instance and all the data within
     */
    public static void dispose()
    {
        s_serverInstance = null;
    }

    @Override
    public Boolean registerObserver(IObserver observer) 
    {
        //System.out.println("registered");
        return onChangeEvent.registerObserver(observer);
    }

    @Override
    public Boolean removeObserver(IObserver toRemove) 
    {
        return onChangeEvent.removeObserver(toRemove);
    }

    @Override
    public void notifyObservers() 
    {
        onChangeEvent.notifyObservers();
    }  

    @Override
    public List<IObserver> getObservers() 
    {
        return onChangeEvent.getObservers();
    }    

    @Override
    public void update() 
    {
        notifyObservers();
    }
}
