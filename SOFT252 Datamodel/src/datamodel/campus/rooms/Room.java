/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datamodel.campus.rooms;

import datamodel.campus.Building;
import datamodel.campus.interfaces.IBuilding;
import datamodel.campus.interfaces.IReadOnlyBuilding;
import datamodel.campus.rooms.interfaces.IRoom;
import datamodel.logging.*;
import datamodel.restrictions.OperatingMode;
import datamodel.restrictions.interfaces.IRestriction;
import datamodel.users.interfaces.ICard;
import datamodel.users.interfaces.IReadOnlyUser;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import observer.ObservableEvent;
import observer.interfaces.IObserver;
import serialisation.ISerialisable;

/**
 * An abstract room class, provides all the basic functionality of a room
 * @author jjcrawley
 */
public abstract class Room implements IRoom, ISerialisable
{
    private String m_roomCode = "Unknown";
    private IBuilding m_building = new Building();
    private int m_floorNum = 0;
    private OperatingMode e_currentMode = OperatingMode.Normal;
    //RoomType roomType;
    private String m_comments = "Empty room";   
    private final List<IRestriction> m_restrictions;
    private transient Logger m_roomLog;
    private transient ObservableEvent onChangeEvent = new ObservableEvent();
    
    /**
     * A simple swipe entry builder for simple creation of room swipe entries
     */
    protected transient SwipeEntryTimeStampBuilder p_entryBuilder;
    
    /**
     * Create an empty room
     */
    protected Room()
    {
        m_restrictions = new ArrayList<>();        
        m_roomLog = new Logger(); 
        p_entryBuilder = new SwipeEntryTimeStampBuilder(m_roomCode, m_building, m_floorNum, RoomType.StaffRoom);        
    }    

    /**
     * Create an empty room with the prescribed details
     * @param building the rooms building
     * @param roomCode the rooms code
     * @param floorNum the rooms floor number
     * @param comments the rooms comments
     * @throws IllegalArgumentException - thrown when either of the details is null, the room code is empty or the floor number is less than 0
     */
    protected Room(IBuilding building, String roomCode, int floorNum, String comments) throws IllegalArgumentException
    {
        if(roomCode == null || comments == null)
        {
           throw new IllegalArgumentException("can't initialise with null values"); 
        }
        else if(building == null || roomCode.isEmpty() || floorNum < 0)
        {
            throw new IllegalArgumentException("initialisation errors");
        }
        else
        {
            m_roomCode = roomCode;
            m_floorNum = floorNum;            
            m_comments = comments;
            m_building = building;
            m_restrictions = new ArrayList<>();            
            p_entryBuilder = new SwipeEntryTimeStampBuilder(m_roomCode, m_building, m_floorNum, RoomType.StaffRoom);
            m_roomLog = new Logger();
        } 
    }
    
    /**
     * Override to allow for restrictions to be set up by inheriting classes
     */
    public abstract void setupDefaultRestrictions();    
    
    @Override
    public void setBuilding(IBuilding newBuilding) throws IllegalArgumentException
    {
        if(newBuilding == null)
        {
            throw new IllegalArgumentException("new building can't be null");
        }
        else
        {
            m_building = newBuilding;
        }
    }

    @Override
    public IReadOnlyBuilding viewBuilding() 
    {
        return m_building;
    }

    @Override
    public String getRoomCode() 
    {
        return m_roomCode;
    }

    @Override
    public void setRoomCode(String roomCode) 
    {
        if(roomCode == null || roomCode.isEmpty())
        {   
            throw new IllegalArgumentException("room code can't be empty");
        }
        else
        {           
            m_roomCode = roomCode;
            p_entryBuilder.setRoomCode(roomCode);        
            notifyObservers();
        }
    }   

    @Override
    public int getFloorNum() 
    {
        return m_floorNum;
    }

    @Override
    public void setFloorNum(int floorNum) 
    {
        if(floorNum >= 0)
        {
            m_floorNum = floorNum;
            p_entryBuilder.setFloorNum(m_floorNum);  
            notifyObservers();
        }
        else
        {
            throw new IllegalArgumentException("can't have a negative floor value");
        }
    }

    @Override
    public OperatingMode getMode()
    {
        return e_currentMode;
    }  

    @Override
    public abstract RoomType getRoomType();

    @Override
    public String getComments() 
    {
        return m_comments;
    }

    @Override
    public void setComments(String comments) 
    { 
        if(comments == null)
        {
            throw new IllegalArgumentException("comments can't be null");
        }
        else
        {
            m_comments = comments;
            notifyObservers();       
        }
    }
    
    @Override
    public Logger getLog()
    {
        return this.m_roomLog;
    } 
    
    /**
     * Add a restriction to the room from inheriting classes
     * @param toAdd the restriction to add
     * @return true if the restriction was added, false otherwise
     */
    protected Boolean addRestriction(IRestriction toAdd) 
    {
        if(toAdd == null)
        {
            throw new IllegalArgumentException("can't assign a null restriction");
        }
        else
        {
            return m_restrictions.add(toAdd);
        }
    }
    
    @Override
    public Boolean allowedAccess(IReadOnlyUser toCheck) 
    {
        if(toCheck == null)
        {
            throw new IllegalArgumentException("can't check a null user");
        }
        else
        {
            for(IRestriction current : m_restrictions)
            {
                if(current.allowAccess(toCheck, e_currentMode))
                {
                    return true;
                }
            }
            
            return false; 
        }
    }
    
    @Override
    public Boolean allowedAccess(IReadOnlyUser toCheck, LocalTime timeToCheck) 
    {
        if(toCheck == null)
        {
            throw new IllegalArgumentException("can't check a null user");
        }
        else
        {
            for(IRestriction current : m_restrictions)
            {
                if(current.allowAccess(toCheck, timeToCheck, e_currentMode))
                {
                    return true;
                }
            }
            
            return false; 
        }
    }  
    
    @Override
    public Access swipeCard(ICard swipeCard) throws IllegalArgumentException
    {        
        if(swipeCard == null)
        {
            throw new IllegalArgumentException("can't swipe card");
        }
        else if(allowedAccess(swipeCard.viewUser()))
        {
            logSwipeCard(swipeCard, Access.Allowed);  
            return Access.Allowed;
        }        
        else
        {
            logSwipeCard(swipeCard, Access.Refused);        
            return Access.Refused;
        }
    }
    
    @Override
    public void logSwipeCard(ICard swipeCard, Access accessed) throws IllegalArgumentException
    {
        if(swipeCard == null || accessed == null)
        {
            throw new IllegalArgumentException("can't log a swipe card with null values");
        }
        else
        {
            TimeStamp newEntry = p_entryBuilder.buildSwipeEntryStamp(swipeCard, accessed);
            m_roomLog.makeEntry(newEntry);
            notifyObservers();
        }   
    }

    @Override
    public void changeMode(OperatingMode changeTo) throws IllegalArgumentException
    {
        if(changeTo == null)
        {
            throw new IllegalArgumentException("changeTo can't be null");
        }
        else
        {
            e_currentMode = changeTo;
            p_entryBuilder.setRoomMode(e_currentMode);
            notifyObservers();
        }
    }

    @Override
    public void logModeChange() 
    {
        String details = "Mode was changed in " + m_roomCode + " to " + e_currentMode;
        String reason = "No reason specified";
        
        ModeChangeTimeStamp newTimeStamp = new ModeChangeTimeStamp(details, reason);
        
        m_roomLog.makeEntry(newTimeStamp);  
        notifyObservers();
    }  

    @Override
    public void logModeChange(String reason) throws IllegalArgumentException
    {
        if(reason == null)
        {
            throw new IllegalArgumentException("reason can't be null");
        }
        else
        {
            String details = "Mode was changed in: " + m_roomCode + " to: " + e_currentMode;
            ModeChangeTimeStamp newTimeStamp = new ModeChangeTimeStamp(details, reason);

            m_roomLog.makeEntry(newTimeStamp);
            notifyObservers();        
        }
    }

    @Override
    public void logModeChange(String reason, String details) throws IllegalArgumentException
    {        
        if(reason == null || details == null)
        {
            throw new IllegalArgumentException("can't create a stampe with null values");
        }
        else
        {
            ModeChangeTimeStamp newTimeStamp = new ModeChangeTimeStamp(details, reason);

            m_roomLog.makeEntry(newTimeStamp);
            notifyObservers();
        }   
    }

    /**
     * Returns the room code of the room
     * @return the room as a string
     */
    @Override
    public String toString() 
    {
        return  "" + m_roomCode;
    }    

    @Override
    public final Boolean registerObserver(IObserver observer)
    { 
        if(onChangeEvent == null)
        {
            onChangeEvent = new ObservableEvent();
        }
        return onChangeEvent.registerObserver(observer);
    }

    @Override
    public final Boolean removeObserver(IObserver toRemove) 
    {
        return onChangeEvent.removeObserver(toRemove);
    }

    @Override
    public final void notifyObservers() 
    {
        onChangeEvent.notifyObservers();
    }  

    @Override
    public final List<IObserver> getObservers() 
    {
        return onChangeEvent.getObservers();
    }

    @Override
    public void onRead() 
    {
        m_roomLog = new Logger();
        p_entryBuilder = new SwipeEntryTimeStampBuilder(m_roomCode, m_building, m_floorNum, RoomType.StaffRoom);
        onChangeEvent = new ObservableEvent();
    }

    @Override
    public void onWrite() 
    {      
    }   
}
