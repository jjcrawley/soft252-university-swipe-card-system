/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datamodel.campus.rooms.interfaces;

import datamodel.restrictions.OperatingMode;

/**
 * Defines standard behaviours for a class that can have it's mode changed
 * @author jjcrawley
 */
public interface IModeChangeable 
{

    /**
     * Change the current operating mode of the implementation
     * @param changeTo the mode to change to
     * @throws IllegalArgumentException thrown when changeTo is null
     */
    void changeMode(OperatingMode changeTo) throws IllegalArgumentException;

    /**
     * Behaviour to log a mode change with default logging details
     */
    void logModeChange();

    /**
     * Behaviour to log a mode change providing a reason for the change
     * @param reason the reason
     * @throws IllegalArgumentException thrown when the reason is null
     */
    void logModeChange(String reason) throws IllegalArgumentException;

    /**
     * Behaviour to log a mode change given a reason and comment
     * @param reason the reason
     * @param comment a comment
     * @throws IllegalArgumentException thrown when nullable arguments are null
     */
    void logModeChange(String reason, String comment) throws IllegalArgumentException;    
}
