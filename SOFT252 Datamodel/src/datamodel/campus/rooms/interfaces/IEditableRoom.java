/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datamodel.campus.rooms.interfaces;

import datamodel.campus.interfaces.IBuilding;
import datamodel.campus.rooms.RoomType;

/**
 * Defines basic functionality for a room that can have it's details edited
 * @author jjcrawley
 */
public interface IEditableRoom extends IReadOnlyRoom
{

    /**
     * Set the building of the room
     * May have side effects depending upon implementation
     * @param newBuilding the building to set the room to
     */
    public void setBuilding(IBuilding newBuilding);    

    /**
     * Set the room code of the room
     * @param roomCode the new room code
     */
    public void setRoomCode(String roomCode);    

    /**
     * Set the floor number of the room
     * @param floorNum the new floor number
     */
    public void setFloorNum(int floorNum);    

    /**
     * Retrieve the current room type
     * @return the current room type
     */
    public abstract RoomType getRoomType();    

    /**
     * Set the comments of the room
     * @param comments the new comments
     */
    public void setComments(String comments);    
}
