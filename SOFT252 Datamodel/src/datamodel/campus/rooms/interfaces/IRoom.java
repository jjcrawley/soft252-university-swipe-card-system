/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datamodel.campus.rooms.interfaces;

import datamodel.logging.Logger;
import datamodel.restrictions.interfaces.IRestrictable;
import observer.interfaces.IObservable;

/**
 * Defines standard behaviours for an editable room, that is restrictable, is observable and can have it's mode changed
 * @author jjcrawley
 */
public interface IRoom extends IEditableRoom, IRestrictable, IObservable, IModeChangeable
{    
    /**
     * Get the room log
     * @return the room log
     */
    public Logger getLog();    
}
