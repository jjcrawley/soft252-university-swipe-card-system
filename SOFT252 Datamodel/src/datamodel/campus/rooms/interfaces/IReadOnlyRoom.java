/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datamodel.campus.rooms.interfaces;

import datamodel.campus.interfaces.IReadOnlyBuilding;
import datamodel.restrictions.OperatingMode;
import serialisation.ISerialisable;

/**
 * Defines basic functionality for a room that can have its details read
 * @author jjcrawley
 */
public interface IReadOnlyRoom extends ISerialisable
{

    /**
     * Retrieve the room code of the room 
     * @return the room code
     */
    public String getRoomCode();

    /**
     * Retrieve the floor number of the room
     * @return the floor number
     */
    public int getFloorNum();

    /**
     * Retrieve the current operating mode of the room
     * @return the current operating mode 
     */
    public OperatingMode getMode();

    /**
     * Retrieve the comments of the room
     * @return the comments
     */
    public String getComments();

    /**
     * Retrieve the building associated with the room
     * @return the read-only version of the building
     */
    public IReadOnlyBuilding viewBuilding();
}
