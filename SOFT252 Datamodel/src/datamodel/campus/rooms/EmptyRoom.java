/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datamodel.campus.rooms;

/**
 * A simple empty room, the room does nothing on its own and is used to 
 * highlight when you tried to create an invalid room type
 * @author jjcrawley
 */
public final class EmptyRoom extends Room
{

    /**
     * Setup the default restrictions for this room, their aren't any
     */
    @Override
    public void setupDefaultRestrictions() 
    {
    }
    
    @Override
    public RoomType getRoomType() 
    {
        return null;
    }
}
