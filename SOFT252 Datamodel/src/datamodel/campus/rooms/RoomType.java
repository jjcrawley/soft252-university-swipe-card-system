/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datamodel.campus.rooms;

import java.io.Serializable;

/**
 * An enumeration of all the basic room types that a swipe card system can provide
 * This can be used to provide advanced filtering solutions of rooms stored in an implementation
 * This is used by the Room factory for the creation of all basic room types
 * @author jjcrawley
 */
public enum RoomType implements Serializable
{

    /**
     * A lecture hall
     */
    LectureHall,

    /**
     * A student lab
     */
    StudentLab,

    /**
     * A research lab
     */
    ResearchLab,

    /**
     * A staff room
     */
    StaffRoom,

    /**
     * A secure room
     */
    SecureRoom;
}
