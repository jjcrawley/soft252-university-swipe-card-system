/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datamodel.campus.rooms;

import datamodel.campus.Building;
import datamodel.campus.interfaces.IBuilding;
import datamodel.campus.rooms.interfaces.IRoom;

/**
 * A builder for the procedural construction of any supported room types
 * It is advisable that you use this builder rather than hand building them with the new keyword
 * @author jjcrawley
 */
public final class RoomBuilder 
{
    private String m_roomCode = "Unknown";
    private int m_roomFloor = 0;
    private String m_comments = "No comments";
    private IBuilding m_building = new Building();
    private RoomType m_roomType = RoomType.LectureHall;
    private final RoomFactory m_factory = RoomFactory.getRoomFactory();
    
    private RoomBuilder()
    {}
    
    private RoomBuilder(IBuilding building)
    {
        setBuilding(building);
    }
    
    /**
     * Sets the room code for the room being constructed
     * @param newCode the buildings code
     * @return the room builder instance
     * @throws IllegalArgumentException thrown when the new code is null or empty
     */
    public RoomBuilder setRoomCode(String newCode) throws IllegalArgumentException
    {
        if(newCode == null)
        {
            throw new IllegalArgumentException("new code can't be null");
        }
        else if(newCode.isEmpty())
        {
            throw new IllegalArgumentException("new code can't be empty");
        }
        else
        {
            m_roomCode = newCode;
            return this;
        }   
    }
    
    /**
     * Set the floor number of the room being constructed
     * @param floorNum the rooms floor number
     * @return the room builder instance
     * @throws IllegalArgumentException thrown then the floor number less than 0
     */
    public RoomBuilder setRoomFloor(int floorNum) throws IllegalArgumentException
    {
        if(floorNum < 0)
        {
            throw new IllegalArgumentException("can't have a floor below zero");
        }
        else
        {
            m_roomFloor = floorNum;
            return this;
        }
    }
    
    /**
     * Set the comments for the room being constructed
     * @param comments the rooms comments
     * @return the room builder instance
     * @throws IllegalArgumentException thrown when the room comments are null
     */
    public RoomBuilder setRoomComments(String comments) throws IllegalArgumentException
    {        
        if(comments == null)
        {
            throw new IllegalArgumentException("comments can't be null");
        }
        else
        {
            m_comments = comments;
            return this;
        }
    }
    
    /**
     * Set the builder of the room being constructed
     * @param building the rooms building
     * @return the room builder instance
     * @throws IllegalArgumentException thrown when the building is null
     */
    public RoomBuilder setBuilding(IBuilding building) throws IllegalArgumentException
    {
        if(building == null)
        {
            throw new IllegalArgumentException("builing can't be set to null");
        }
        else
        {
            m_building = building;
            return this;
        }
    }
    
    /**
     * Sets the room type of the room being constructed
     * @param type the rooms type
     * @return the room builder instance
     * @throws IllegalArgumentException thrown when the type is null
     */
    public RoomBuilder setRoomType(RoomType type) throws IllegalArgumentException
    {
        if(type == null)
        { 
            throw new IllegalArgumentException("can't accept a null type");
        }
        else
        {
            m_roomType = type;
            return this;
        }
    }
    
    /**
     * Once all the details are setup you call this method to build the room
     * @return the newly constructed room
     */
    public IRoom buildRoom()
    {
        return m_factory.CreateRoom(m_roomType, m_building, m_roomCode, m_roomFloor, m_comments);
    }
    
    /**
     * Once all the details are setup you can call this method to build the room as a specific type
     * @param type the type to build as
     * @return the newly constructed room
     */
    public IRoom buildRoom(RoomType type)
    {
        return m_factory.CreateRoom(type, m_building, m_roomCode, m_roomFloor, m_comments);
    }
    
    /**
     * Get an instance of the room builder
     * @return a new room builder
     */
    public static RoomBuilder getRoomBuilder()
    {
        return new RoomBuilder();
    } 
    
    /**
     * Get an instance of the room builder with the building setup
     * @param building the building to use
     * @return a new room builder with the prescribed building
     */
    public static RoomBuilder getRoomBuilder(IBuilding building)
    {
        return new RoomBuilder(building);
    }
}
