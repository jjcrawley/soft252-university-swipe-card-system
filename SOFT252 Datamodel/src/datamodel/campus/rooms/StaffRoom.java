/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datamodel.campus.rooms;

import datamodel.campus.interfaces.IBuilding;
import datamodel.restrictions.DefaultRoomRestrictionBuilder;
import datamodel.restrictions.interfaces.IRestriction;
import java.util.List;

/**
 * A simple implementation of a staff room
 * @author jjcrawley
 */
public final class StaffRoom extends Room
{

    /**
     * Create a simple staff room
     * Restrictions are automatically setup
     */
    public StaffRoom() 
    {
        super();
        setupDefaultRestrictions();
        p_entryBuilder.setRoomType(RoomType.StaffRoom);
    }   

    /**
     * Creates a simple staff room with the prescribed details
     * @param building the rooms building
     * @param roomCode the rooms code
     * @param floorNum the rooms floor number
     * @param comments the rooms comments
     */
    public StaffRoom(IBuilding building, String roomCode, int floorNum, String comments) 
    {
        super(building, roomCode, floorNum, comments);
        setupDefaultRestrictions();
        p_entryBuilder.setRoomType(RoomType.StaffRoom);
    }    

    /**
     * Sets up the default restrictions for the room 
     * This uses the defaultRestrictionBuilder class provided in datamodel.restrictions
     */
    @Override
    public void setupDefaultRestrictions() 
    {
        List<IRestriction> restrictionsToApply; //= new ArrayList<>();
        
        restrictionsToApply = DefaultRoomRestrictionBuilder.getInstance().buildStaffRoomDefaults();
        
        //restrictionsToApply.addAll(DefaultRoomRestrictionSets.getInstance().buildStaffRoomDefaults());
        
        restrictionsToApply.stream().forEach((currentRestriction) -> 
        {
            this.addRestriction(currentRestriction);
        });
    }
    
    @Override
    public RoomType getRoomType() 
    {
        return RoomType.StaffRoom;
    }   
}
