/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datamodel.campus.rooms;

import datamodel.campus.interfaces.IBuilding;
import datamodel.campus.rooms.interfaces.IRoom;

/**
 * Use this class to control the creation of different supported room types
 * @author jjcrawley
 */
public class RoomFactory 
{
    private RoomFactory()
    {}   
    
    /**
     * Create a room of given type with the prescribed details
     * @param toCreate the rooms type 
     * @param building the rooms building
     * @param roomCode the rooms code
     * @param floorNum the rooms floor number
     * @param comments the rooms comments
     * @return the newly created room, an empty room is returned if the room type is unrecognised
     */
    public IRoom CreateRoom(RoomType toCreate, IBuilding building, String roomCode, int floorNum, String comments) 
    {        
        switch(toCreate)
        {
            case LectureHall:
                return new LectureHall(building, roomCode, floorNum, comments);               
            case ResearchLab:
                return new ResearchLab(building, roomCode, floorNum, comments);
            case SecureRoom:
                return new SecureRoom(building, roomCode, floorNum, comments);
            case StaffRoom:
                return new StaffRoom(building, roomCode, floorNum, comments);
            case StudentLab:
                return new StudentLab(building, roomCode, floorNum, comments);                    
            default:
                return new EmptyRoom();
        }         
    }
    
    /**
     * Get a new instance of the room factory class
     * @return a new room factory
     */
    public static RoomFactory getRoomFactory()
    {
        return new RoomFactory();
    }            
}
