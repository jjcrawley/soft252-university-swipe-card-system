/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datamodel.campus.rooms;

import datamodel.campus.interfaces.IBuilding;
import datamodel.restrictions.DefaultRoomRestrictionBuilder;
import datamodel.restrictions.interfaces.IRestriction;
import java.util.List;

/**
 * A simple lecture hall can't be extended from
 * @author jjcrawley
 */
public final class LectureHall extends Room
{

    /**
     * Create an empty lecture hall, this will setup default restrictions as well
     */
    public LectureHall()
    {
        super();
        setupDefaultRestrictions();
        p_entryBuilder.setRoomType(RoomType.LectureHall);
    }     

    /**
     * Create an empty lecture hall with the given properties
     * This will also setup default restrictions
     * @param building the rooms building
     * @param roomCode the rooms code
     * @param floorNum the rooms floor number
     * @param comments the rooms comments
     */
    public LectureHall(IBuilding building, String roomCode, int floorNum, String comments) 
    {
        super(building, roomCode, floorNum, comments);
        setupDefaultRestrictions();
        p_entryBuilder.setRoomType(RoomType.LectureHall);
    }    

    /**
     * Sets up the default restrictions for the room 
     * This uses the defaultRestrictionBuilder class provided in datamodel.restrictions
     */
    @Override
    public void setupDefaultRestrictions() 
    {
        List<IRestriction> restrictionsToApply; //= new ArrayList<>();
        
        //restrictionsToApply.addAll(DefaultRoomRestrictionSets.getInstance().buildLectureHallDefaults());
        
        restrictionsToApply = DefaultRoomRestrictionBuilder.getInstance().buildLectureHallDefaults();
        
        restrictionsToApply.stream().forEach((currentRestriction) -> 
        {
            this.addRestriction(currentRestriction);
        });
    }
    
    @Override
    public RoomType getRoomType()
    {
        return RoomType.LectureHall;
    }    
}
