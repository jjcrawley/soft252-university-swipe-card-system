/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datamodel.campus.rooms;

import datamodel.campus.interfaces.IBuilding;
import datamodel.restrictions.DefaultRoomRestrictionBuilder;
import datamodel.restrictions.interfaces.IRestriction;
import java.util.List;

/**
 * A simple student lad can't be extended from
 * @author jjcrawley
 */
public final class StudentLab extends Room
{

    /**
     * Creates an empty student lab with no details
     * this will also setup default restrictions for the room
     */
    public StudentLab() 
    {
        super();
        setupDefaultRestrictions();
        p_entryBuilder.setRoomType(RoomType.StudentLab);
    }  

    /**
     * Create a student lab with the given properties
     * @param building the rooms building
     * @param roomCode the rooms code
     * @param floorNum the rooms floor number
     * @param comments the rooms comments
     */
    public StudentLab(IBuilding building, String roomCode, int floorNum, String comments) 
    {
        super(building, roomCode, floorNum, comments);
        setupDefaultRestrictions();
        p_entryBuilder.setRoomType(RoomType.StudentLab);
    }

    /**
     * Sets up the default restrictions for the room 
     * This uses the defaultRestrictionBuilder class provided in datamodel.restrictions
     */
    @Override
    public void setupDefaultRestrictions() 
    {
        List<IRestriction> restrictionsToApply; //= new ArrayList<>();
        
        //restrictionsToApply.addAll(DefaultRoomRestrictionSets.getInstance().buildStudentLabDefaults());
        
        restrictionsToApply = DefaultRoomRestrictionBuilder.getInstance().buildStudentLabDefaults(); 
        
        restrictionsToApply.stream().forEach((currentRestriction) -> 
        {
            this.addRestriction(currentRestriction);
        });        
    }
    
    @Override
    public RoomType getRoomType() 
    {
        return RoomType.StudentLab;
    }
}
