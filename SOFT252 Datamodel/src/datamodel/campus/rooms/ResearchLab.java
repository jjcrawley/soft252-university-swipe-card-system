/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datamodel.campus.rooms;

import datamodel.campus.interfaces.IBuilding;
import datamodel.restrictions.DefaultRoomRestrictionBuilder;
import datamodel.restrictions.interfaces.IRestriction;
import java.util.List;

/**
 * A simple research lab, can't be inherited from
 * @author jjcrawley
 */
public final class ResearchLab extends Room
{

    /**
     * Create a default research lab, restrictions are automatically setup
     */
    public ResearchLab() 
    {
        super();
        setupDefaultRestrictions();
        p_entryBuilder.setRoomType(RoomType.ResearchLab);
    }   

    /**
     * Creates a simple research lab with the prescribed details
     * Restrictions are automatically setup
     * @param building the rooms building
     * @param roomCode the rooms code
     * @param floorNum the rooms floor number
     * @param comments the rooms comments
     */
    public ResearchLab(IBuilding building, String roomCode, int floorNum, String comments) 
    {
        super(building, roomCode, floorNum, comments);
        setupDefaultRestrictions();
        p_entryBuilder.setRoomType(RoomType.ResearchLab);
    }

    /**
     * Sets up the default restrictions for the room 
     * This uses the defaultRestrictionBuilder class provided in datamodel.restrictions
     */
    @Override
    public void setupDefaultRestrictions() 
    {
        List<IRestriction> restrictionsToApply; //= new ArrayList<>();
        
        //restrictionsToApply.addAll(DefaultRoomRestrictionSets.getInstance().buildResearchLabDefaults());
        
        restrictionsToApply = DefaultRoomRestrictionBuilder.getInstance().buildResearchLabDefaults();
        
        restrictionsToApply.stream().forEach((currentRestriction) -> 
        {
            this.addRestriction(currentRestriction);
        });
    }
    
    @Override
    public RoomType getRoomType() 
    {
        return RoomType.ResearchLab;
    }   
}
