/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datamodel.campus;

import datamodel.campus.interfaces.IBuilding;
import datamodel.campus.interfaces.ICampus;
import datamodel.campus.interfaces.IReadOnlyBuilding;
import datamodel.logging.Logger;
import datamodel.logging.ModeChangeTimeStamp;
import datamodel.logging.TimeStamp;
import datamodel.restrictions.OperatingMode;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import observer.ObservableEvent;
import observer.interfaces.IObserver;
import serialisation.ISerialisable;

/**
 * A campus based implementation that allows you to store a collection of IBuilding's
 * @author Joshua
 */

public final class Campus implements ICampus, ISerialisable
{
    //private final BuildingsManager buildings;
    //private final List<Building> buildings;
    private final Map<String, IBuilding> m_buildings;
    private OperatingMode e_campusMode = OperatingMode.Normal;
    private String m_campusCode = "Unknown";
    private transient Logger m_campusLog;  
    private transient ObservableEvent onChange = new ObservableEvent();
    
    /**
     * Create an empty campus
     */
    public Campus()
    {       
        m_buildings = new HashMap<>();
        m_campusLog = new Logger();
    }

    /**
     * Create a campus with the given campus code
     * @param campusCode the campuses code
     * @throws IllegalArgumentException - thrown when the campus code is null or empty
     */
    public Campus(String campusCode) throws IllegalArgumentException
    {
        if(campusCode == null || campusCode.isEmpty())
        {
            throw new IllegalArgumentException("needs a campus code");
        }
        else
        {
            m_campusCode = campusCode;            
            m_buildings = new HashMap<>();
            m_campusLog = new Logger();            
        }   
    }

    @Override
    public String getCampusCode() 
    {
        return m_campusCode;
    }

    @Override
    public void setCampusCode(String campusCode) throws IllegalArgumentException
    {
        if(campusCode != null && !campusCode.isEmpty())
        {
            m_campusCode = campusCode;
            notifyObservers();
        }  
        else
        {
            throw new IllegalArgumentException("campus code can't be empty");
        }
    }

    @Override
    public List<IReadOnlyBuilding> viewBuildings() 
    {
        List<IReadOnlyBuilding> copyOfBuildings = new ArrayList<>();
        
        m_buildings.values().stream().forEach((currentBuilding) -> 
        {
            copyOfBuildings.add(currentBuilding);
        });       
        
        return copyOfBuildings;
        //return buildings;
    }  

    @Override
    public List<IBuilding> getBuildings() 
    {
        List<IBuilding> copyOfBuildings = new ArrayList<>();
        
        m_buildings.values().stream().forEach((currentBuilding) -> 
        {
            copyOfBuildings.add(currentBuilding);
        });       
        
        return copyOfBuildings;
    }   

    @Override
    public OperatingMode getCampusMode() 
    {
        return e_campusMode;
    } 
    
    @Override
    public Logger getLog()
    {
        return m_campusLog;
    }
    
    @Override
    public List<Logger> getBuildingLogs()
    {
        List<Logger> copyOfBuildingLogs = new ArrayList<>();
        
        m_buildings.values().stream().forEach((currentBuilding) -> 
        {
            copyOfBuildingLogs.add(currentBuilding.getLog());
        });
        
        return copyOfBuildingLogs;
    }
    
    @Override
    public List<Logger> getRoomLogs()
    {
        List<Logger> copyOfRoomLoggers = new ArrayList<>();
        
        m_buildings.values().stream().forEach((currentBuilding) -> 
        {
            currentBuilding.getRooms().stream().forEach((currentRoom) -> 
            {
                copyOfRoomLoggers.add(currentRoom.getLog());
            });
        });
        
        return copyOfRoomLoggers;        
    }
    
    @Override
    public Boolean addBuilding(IBuilding toAdd) throws IllegalArgumentException
    {
        if(toAdd == null)
        {
            throw new IllegalArgumentException("can't add a null value");
        }
        else if(m_buildings.containsKey(toAdd.getBuildingCode()) || m_buildings.containsValue(toAdd))
        {
            return false;
        }
        else
        {
            m_buildings.put(toAdd.getBuildingCode(), toAdd);            
            toAdd.registerObserver(CentralServer.getInstance());
            toAdd.changeMode(e_campusMode);
            notifyObservers();

            return true;                        
        }              
    }
   
    @Override
    public Boolean removeBuilding(String buildingCode)
    {
        if(buildingCode.isEmpty())
        {
            return false;
        }
        else if(m_buildings.containsKey(buildingCode))
        {
            IBuilding removedBuilding = m_buildings.remove(buildingCode);
            removedBuilding.removeObserver(CentralServer.getInstance());
            removedBuilding.clearBuilding();
            
            notifyObservers();

            return true;
        }
        
        return false;                
    } 
    
    @Override
    public void refactorBuildingCode(String oldCode, String newCode) throws IllegalArgumentException
    {
        if(oldCode.isEmpty() || newCode.isEmpty())
        {
            throw new IllegalArgumentException("can't refactor with current parameters");
        }      
        else if(!m_buildings.containsKey(oldCode))
        {
            throw new IllegalArgumentException("the building doesn't exist");
        }
        else
        {
            IBuilding toRefactor = m_buildings.get(oldCode);
            
            m_buildings.remove(oldCode);
            
            toRefactor.setBuildingCode(newCode);
            
            m_buildings.put(newCode, toRefactor);        
            
            notifyObservers();
        }
    }
    
    @Override
    public void refactorBuildingCode(IBuilding toRefactor, String newCode) throws IllegalArgumentException
    {
        if(newCode == null || toRefactor == null)
        {
            throw new IllegalArgumentException("can't refactor with null parameters");
        }
        if(newCode.isEmpty())
        {
            throw new IllegalArgumentException("can't refactor with an empty code");
        }       
        else
        {
            String oldCode = toRefactor.getBuildingCode();
            
            m_buildings.remove(oldCode);
            
            toRefactor.setBuildingCode(newCode);
            
            m_buildings.put(newCode, toRefactor);
            
            notifyObservers();
        }
    }
    
    
    @Override
    public boolean moveBuildingToCampus(ICampus destination, String buildingCode) throws IllegalArgumentException
    {
        if(destination == null || buildingCode == null)
        {
            throw new IllegalArgumentException("can't move with null parameters");
        }
        else if(!m_buildings.containsKey(buildingCode))
        {
            return false;
        }
        else
        {
            IBuilding toMove;
            
            toMove = m_buildings.get(buildingCode);
                        
            m_buildings.remove(buildingCode);
                      
            return destination.addBuilding(toMove);
        }           
    }
    
    @Override
    public IBuilding getBuilding(String buildingCode)
    {
        return m_buildings.get(buildingCode);
    }

    @Override
    public void clearCampus()
    {
        m_buildings.values().stream().forEach((building) -> 
                {
                    building.clearBuilding();
                });
    }    
    
    @Override
    public void changeMode(OperatingMode changeTo) throws IllegalArgumentException
    {
        if(changeTo == null)
        {
            throw new IllegalArgumentException("must be a valid mode");
        }
        else
        {
            e_campusMode = changeTo;
            
            m_buildings.values().stream().forEach((currentBuilding) -> 
            {
                currentBuilding.changeMode(changeTo);
            });
            
            notifyObservers();
        }
    }

    @Override
    public void logModeChange() 
    {
        String comment = "Campus: " + m_campusCode + " mode was changed to " + e_campusMode;
        String reason = "No details specified";
        
        TimeStamp modeChange = new ModeChangeTimeStamp(comment, reason);
        m_campusLog.makeEntry(modeChange);
        notifyObservers();
    } 

    @Override
    public void logModeChange(String reason) throws IllegalArgumentException
    {
        String details = "Campus: " + m_campusCode + " mode was changed to " + e_campusMode;
        
        if(reason == null)
        {
            throw new IllegalArgumentException("reason can't be null");
        }
        
        TimeStamp modeChange = new ModeChangeTimeStamp(details, reason);
        m_campusLog.makeEntry(modeChange);
        notifyObservers();        
    }   

    @Override
    public void logModeChange(String reason, String details) throws IllegalArgumentException
    {
        if(reason == null || details == null)
        {
            throw new IllegalArgumentException("arguments can't be null");
        }
        
        if(details.isEmpty())
        {
            throw new IllegalArgumentException("comments can't be empty");
        }
        
        TimeStamp modeChange = new ModeChangeTimeStamp(reason, details);
        m_campusLog.makeEntry(modeChange);
        notifyObservers();
    }   

    /**
     * Get a string representation of the campus: code
     * @return campus string
     */
    @Override
    public String toString() 
    {
        return "" + m_campusCode;
    }       

    @Override
    public final Boolean registerObserver(IObserver observer) 
    {
        if(onChange == null)
        {
            onChange = new ObservableEvent();
        }
        
        return onChange.registerObserver(observer);
    }

    @Override
    public final Boolean removeObserver(IObserver toRemove) 
    {
        if(onChange == null)
        {
            return false;
        }
        
        return onChange.removeObserver(toRemove);
    }

    @Override
    public final void notifyObservers() 
    {
        if(onChange == null)
        {
            return;
        }
        
        onChange.notifyObservers();
    }

    @Override
    public final List<IObserver> getObservers() 
    {
        return onChange.getObservers();
    }   

    @Override
    public void onRead() 
    {
        m_campusLog = new Logger();
        onChange = new ObservableEvent();
        
        m_buildings.values().stream().forEach((building) -> 
        {
            building.onRead();
        });
    }

    @Override
    public void onWrite() 
    {        
    }     
}
