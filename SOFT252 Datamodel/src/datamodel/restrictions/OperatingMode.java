/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datamodel.restrictions;

import java.io.Serializable;

/**
 * A simple enumeration for all permitted modes of the system
 * @author jjcrawley
 */
public enum OperatingMode implements Serializable
{

    /**
     * Emergency mode
     */
    Emergency,

    /**
     * Normal mode
     */
    Normal;
}
