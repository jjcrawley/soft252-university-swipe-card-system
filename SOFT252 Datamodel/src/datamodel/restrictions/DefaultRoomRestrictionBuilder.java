/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datamodel.restrictions;

import datamodel.campus.rooms.RoomType;
import datamodel.restrictions.interfaces.IRestriction;
import datamodel.users.UserRole;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

/**
 * A simple singleton to help with the creation of default room restrictions
 * @author jjcrawley
 */
public class DefaultRoomRestrictionBuilder 
{
    private final static DefaultRoomRestrictionBuilder s_instance = new DefaultRoomRestrictionBuilder();
            
    private DefaultRoomRestrictionBuilder()
    {
             
    }  
    
    /**
     * Build the default room restrictions for a lecture hall
     * @return a list of restrictions
     */
    public List<IRestriction> buildLectureHallDefaults()
    {
        List<IRestriction> lectureHallDefaults = new ArrayList<>();
        
        lectureHallDefaults.addAll(buildContractCleanerDefaults(RoomType.LectureHall));
        lectureHallDefaults.addAll(buildEmergencyResponderDefaults(RoomType.LectureHall));
        lectureHallDefaults.addAll(buildGuestDefaults(RoomType.LectureHall));
        lectureHallDefaults.addAll(buildSecurityDefaults(RoomType.LectureHall));
        lectureHallDefaults.addAll(buildStaffDefaults(RoomType.LectureHall));
        lectureHallDefaults.addAll(buildStudentDefaults(RoomType.LectureHall));
        lectureHallDefaults.addAll(buildManagerDefaults(RoomType.LectureHall));            
        
        return lectureHallDefaults;
    }
    
    /**
     * Build the default secure room restrictions
     * @return a list of restrictions
     */
    public List<IRestriction> buildSecureRoomDefaults()
    {
        List<IRestriction> secureRoomDefaults = new ArrayList<>();
        
        secureRoomDefaults.addAll(buildContractCleanerDefaults(RoomType.SecureRoom));
        secureRoomDefaults.addAll(buildEmergencyResponderDefaults(RoomType.SecureRoom));
        secureRoomDefaults.addAll(buildGuestDefaults(RoomType.SecureRoom));
        secureRoomDefaults.addAll(buildSecurityDefaults(RoomType.SecureRoom));
        secureRoomDefaults.addAll(buildStaffDefaults(RoomType.SecureRoom));
        secureRoomDefaults.addAll(buildStudentDefaults(RoomType.SecureRoom));
        secureRoomDefaults.addAll(buildManagerDefaults(RoomType.SecureRoom));            
        
        return secureRoomDefaults;
    }
    
    /**
     * Build the default restrictions for the staff room
     * @return a list of restrictions
     */
    public List<IRestriction> buildStaffRoomDefaults()
    {
        List<IRestriction> staffRoomDefaults = new ArrayList<>();
        
        staffRoomDefaults.addAll(buildContractCleanerDefaults(RoomType.StaffRoom));
        staffRoomDefaults.addAll(buildEmergencyResponderDefaults(RoomType.StaffRoom));
        staffRoomDefaults.addAll(buildGuestDefaults(RoomType.StaffRoom));
        staffRoomDefaults.addAll(buildSecurityDefaults(RoomType.StaffRoom));
        staffRoomDefaults.addAll(buildStaffDefaults(RoomType.StaffRoom));
        staffRoomDefaults.addAll(buildStudentDefaults(RoomType.StaffRoom));
        staffRoomDefaults.addAll(buildManagerDefaults(RoomType.StaffRoom));            
        
        return staffRoomDefaults;
    }
    
    /**
     * Build the default restrictions for the research lab
     * @return a list of restrictions
     */
    public List<IRestriction> buildResearchLabDefaults()
    {
        List<IRestriction> researchRoomDefaults = new ArrayList<>();
        
        researchRoomDefaults.addAll(buildContractCleanerDefaults(RoomType.ResearchLab));
        researchRoomDefaults.addAll(buildEmergencyResponderDefaults(RoomType.ResearchLab));
        researchRoomDefaults.addAll(buildGuestDefaults(RoomType.ResearchLab));
        researchRoomDefaults.addAll(buildSecurityDefaults(RoomType.ResearchLab));
        researchRoomDefaults.addAll(buildStaffDefaults(RoomType.ResearchLab));
        researchRoomDefaults.addAll(buildStudentDefaults(RoomType.ResearchLab));
        researchRoomDefaults.addAll(buildManagerDefaults(RoomType.ResearchLab));            
        
        return researchRoomDefaults;
    }
    
    /**
     * Builds a list of default restrictions for the student lab
     * @return a list of restrictions
     */
    public List<IRestriction> buildStudentLabDefaults()
    {
        List<IRestriction> studentLabDefaults = new ArrayList<>();
        
        studentLabDefaults.addAll(buildContractCleanerDefaults(RoomType.StudentLab));
        studentLabDefaults.addAll(buildEmergencyResponderDefaults(RoomType.StudentLab));
        studentLabDefaults.addAll(buildGuestDefaults(RoomType.StudentLab));
        studentLabDefaults.addAll(buildSecurityDefaults(RoomType.StudentLab));
        studentLabDefaults.addAll(buildStaffDefaults(RoomType.StudentLab));
        studentLabDefaults.addAll(buildStudentDefaults(RoomType.StudentLab));
        studentLabDefaults.addAll(buildManagerDefaults(RoomType.StudentLab));            
        
        return studentLabDefaults;
    }
        
    /**
     * Build security default restrictions for a given rooms type
     * Security staff have 24 hour access to rooms regardless of mode
     * @param toBuild the type to build the restrictions for
     * @return the security defaults
     */
    public List<IRestriction> buildSecurityDefaults(RoomType toBuild)
    {
        List<IRestriction> defaults = new ArrayList<>();
        
        switch(toBuild)
        {
            default:
                Restriction securityDefaultNormal = new Restriction(UserRole.Security, OperatingMode.Normal);                        
                securityDefaultNormal.addTimePeriod(new TimePeriod(LocalTime.MIDNIGHT, LocalTime.MAX));

                Restriction securityDefaultEmergency = new Restriction(UserRole.Security, OperatingMode.Emergency); 
                securityDefaultEmergency.addTimePeriod(new TimePeriod(LocalTime.MIDNIGHT, LocalTime.MAX));

                defaults.add(securityDefaultNormal);
                defaults.add(securityDefaultEmergency);
                break;
        }
        return defaults;        
    }
    
    /**
     * Build the student default restrictions for a given room type
     * Students have timed access (8:30 to 22:00) for lecture halls and student labs
     * @param toBuild the type to build for
     * @return student defaults
     */
    public List<IRestriction> buildStudentDefaults(RoomType toBuild)
    {
        List<IRestriction> defaults = new ArrayList<>();
        
        switch(toBuild)
        {
            case LectureHall:                
                Restriction studentDefault = new Restriction(UserRole.Student, OperatingMode.Normal);                
                studentDefault.addTimePeriod(new TimePeriod(LocalTime.of(8, 30), LocalTime.of(22, 0)));
                defaults.add(studentDefault);
                break;
            case StudentLab:
                Restriction studentDefaultLab = new Restriction(UserRole.Student, OperatingMode.Normal);        
                studentDefaultLab.addTimePeriod(new TimePeriod(LocalTime.of(8, 30), LocalTime.of(22, 0)));                
                defaults.add(studentDefaultLab);
                break;
            default:
                return defaults;
        }
        
        return defaults;               
    }
    
    /**
     * Build the staff default restrictions for a given room type
     * Staff have timed access (5:30 to 00:00) to all rooms except secure rooms
     * @param toBuild the room type to build for
     * @return a list of staff restrictions
     */
    public List<IRestriction> buildStaffDefaults(RoomType toBuild)
    {
        List<IRestriction> staffDefaults = new ArrayList<>();                
        
        switch(toBuild)
        {
            case SecureRoom:
                break;                
            default:
                Restriction staffDefaultResearch = new Restriction(UserRole.Staff, OperatingMode.Normal);        
                staffDefaultResearch.addTimePeriod(new TimePeriod(LocalTime.of(5, 30), LocalTime.MAX));
                staffDefaults.add(staffDefaultResearch);
                break;                   
        }       
        
        return staffDefaults;
    }
    
    /**
     * Build contract cleaner defaults for a given room type
     * Contract cleaners have timed access (5:30 to 10:30 and 17:30 to 22:30) to all rooms except secure rooms
     * @param toBuild the room type to build for
     * @return a list of default staff restrictions
     */
    public List<IRestriction> buildContractCleanerDefaults(RoomType toBuild)
    {
        List<IRestriction> contractCleanerSet = new ArrayList<>();
        
        switch(toBuild)
        {
            case SecureRoom:
                break;
            default:
                Restriction contractCleanerDefault = new Restriction(UserRole.ContractCleaner, OperatingMode.Normal);
                contractCleanerDefault.addTimePeriod(new TimePeriod(LocalTime.of(5, 30), LocalTime.of(10, 30)));        
                contractCleanerDefault.addTimePeriod(new TimePeriod(LocalTime.of(17, 30), LocalTime.of(22, 30)));                      
                contractCleanerSet.add(contractCleanerDefault);
                break;
        }
        
        return contractCleanerSet;        
    }
    
    /**
     * Builds the manager defaults for a given room type
     * Managers have 24 hour access to all rooms except in emergency mode
     * @param toBuild the room type to build for
     * @return the list of manager defaults
     */
    public List<IRestriction> buildManagerDefaults(RoomType toBuild)
    {
        List<IRestriction> managerDefaults = new ArrayList<>();
        
        switch(toBuild)
        {            
            default:
                Restriction managerDefault = new Restriction(UserRole.Manager, OperatingMode.Normal);
                managerDefault.addTimePeriod(new TimePeriod(LocalTime.MIDNIGHT, LocalTime.MAX));               
                managerDefaults.add(managerDefault);
                break;              
        }                 
        
        return managerDefaults;
    }
    
    /**
     * Builds the default restrictions for an emergency responder for a given room type
     * Emergency responders have 24 hour access all rooms, only during emergency mode
     * @param toBuild the type to build for
     * @return the list of emergency responder defaults
     */
    public List<IRestriction> buildEmergencyResponderDefaults(RoomType toBuild)
    {
        List<IRestriction> emergencyResponderDefaults = new ArrayList<>();
        
        switch(toBuild)
        {
            default:
                Restriction defaultRestriction = new Restriction(UserRole.EmergencyResponder, OperatingMode.Emergency);        
                defaultRestriction.addTimePeriod(new TimePeriod(LocalTime.MIDNIGHT, LocalTime.MAX));
                emergencyResponderDefaults.add(defaultRestriction);
                break;
        }
        
        return emergencyResponderDefaults;        
    }
    
    /**
     * Builds the guest default restrictions for a given room type
     * Guests have timed access (8:30 to 22:00) to Lecture halls only
     * @param toBuild the type to build for
     * @return the list of guest default restrictions
     */
    public List<IRestriction> buildGuestDefaults(RoomType toBuild)
    {
        List<IRestriction> guestSet = new ArrayList<>();       
        
        switch(toBuild)
        {
            case LectureHall:
                Restriction guestDefault = new Restriction(UserRole.Guest, OperatingMode.Normal);
                guestDefault.addTimePeriod(new TimePeriod(LocalTime.of(8, 30), LocalTime.of(22, 0)));
                guestSet.add(guestDefault);
                break;
            default:
                break;
        }   
        
        return guestSet;
    }   
    
    /**
     * Gets the instance of the defaultRoomSwipeEntryBuilder
     * @return the instance
     */
    public static DefaultRoomRestrictionBuilder getInstance()
    {
        return s_instance;
    }
}
