/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datamodel.restrictions;

import datamodel.restrictions.interfaces.IRestriction;
import java.util.*;
import datamodel.users.UserRole;
import datamodel.users.interfaces.IReadOnlyUser;
import datamodel.users.interfaces.IUser;
import java.io.Serializable;
import java.time.LocalTime;

/**
 * A restriction implementation that allows you to define simple restriction settings 
 * based on time period, user type and operating mode
 * @author jjcrawley
 */
public class Restriction implements Serializable, IRestriction
{
    private OperatingMode e_accessMode;
    private final List<TimePeriod> m_times;
    private UserRole e_role;
    
    /**
     * Create a simple restriction 
     * Defaults will restrict Guests in Normal operating mode with no time period
     */
    public Restriction()
    {
        e_role = UserRole.Guest;
        e_accessMode = OperatingMode.Normal;
        m_times = new ArrayList<>();
    }

    /**
     * Creates a simple restriction restricting the prescribed user role
     * @param role the role to restrict 
     * @throws IllegalArgumentException thrown when role is null
     */
    public Restriction(UserRole role) throws IllegalArgumentException
    {
        if(role == null)
        {
            throw new IllegalArgumentException("can't assign a restriction to a null role");
        }
        else
        {
            m_times = new ArrayList<>();
            e_role = role;
            e_accessMode = OperatingMode.Normal;
        }   
    }
    
    /**
     * Creates a simple restriction restricting the prescribed role and mode
     * @param toRestrict the role to restrict
     * @param restrictionMode the mode to restrict
     * @throws IllegalArgumentException thrown when either argument is null
     */
    public Restriction(UserRole toRestrict, OperatingMode restrictionMode) throws IllegalArgumentException
    {
        if(toRestrict == null || restrictionMode == null)
        {
            throw new IllegalArgumentException("can't create restriction with current parameters");
        }    
        else
        {            
            e_role = toRestrict;
            m_times = new ArrayList<>(); 
            e_accessMode = restrictionMode;
        }
    } 

    @Override
    public UserRole getRole() 
    {
        return e_role;
    }   

    @Override
    public void setAccessMode(OperatingMode accessMode) throws IllegalArgumentException
    {
        if(accessMode == null)
        {
            throw new IllegalArgumentException("can't assign null mode");
        }
        else
        {
            e_accessMode = accessMode;
        }
    }
    
    @Override
    public OperatingMode getModeRestriction() 
    {
        return e_accessMode;
    }

    @Override
    public void setRole(UserRole role) throws IllegalArgumentException
    {
        if(role == null)
        {
            throw new IllegalArgumentException("can't accept a null role");
        }
        else
        {
            e_role = role;
        }    
    }  

    @Override
    public Boolean addTimePeriod(TimePeriod toAdd) throws IllegalArgumentException
    {
        if(toAdd == null)
        {
            throw new IllegalArgumentException("can't add a null time period");
        }
        else if(!m_times.contains(toAdd))
        {
            return m_times.add(toAdd);
        }
        
        return false;
    }

    @Override
    public Boolean removeTimePeriod(int index) throws IndexOutOfBoundsException
    {
        if(index > m_times.size())
        {
            throw new IndexOutOfBoundsException("this time period is out of range");
        }
        else
        {
            return m_times.remove(index) != null;
        }    
    }

    @Override
    public Boolean allowAccess(IReadOnlyUser toCheck, LocalTime timetoCheck, OperatingMode mode) throws IllegalArgumentException
    {
        if(toCheck == null || timetoCheck == null)
        {
            throw new IllegalArgumentException("can't validate using these values");
        }
        else
        {
            if(mode.equals(e_accessMode))
            { 
                if(toCheck.viewCard().hasUserRole(e_role))                  
                {
                    return checkTimePeriods(timetoCheck);
                }                                    
            }           
        }
        
        return false;
    }

    @Override
    public Boolean allowAccess(IReadOnlyUser toCheck, OperatingMode mode) throws IllegalArgumentException
    {
        if(toCheck == null || mode == null)
        {
            throw new IllegalArgumentException("can't validate with current parameters");
        }
        else
        {
            LocalTime now = LocalTime.now();
            
            if(mode.equals(e_accessMode))
            {     
                if(toCheck.viewCard().hasUserRole(e_role))
                {
                    return checkTimePeriods(now);                    
                }                
            }       
        }       
                
        return false;
    }
    
    private boolean checkTimePeriods(LocalTime toCheck)
    {
        if(m_times.isEmpty())
        {
            return true;
        }
        else
        {
            for (TimePeriod m_time : m_times) 
            {
                if (m_time.withinRange(toCheck)) 
                {
                    return true;                
                }
            } 
        }
        
        return false;
    }
}
