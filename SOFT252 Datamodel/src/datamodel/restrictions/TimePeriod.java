/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datamodel.restrictions;

import java.io.Serializable;
import java.time.LocalTime;
import java.util.Objects;

/**
 * Allows for the simple creation of a time period
 * This stores two times as a period of time which can be used in a similar fashion to the java.time.Duration class, 
 * except here you define a start and end time
 * @author jjcrawley
 */
public class TimePeriod implements Serializable
{     
    private final LocalTime m_startTime;
    private final LocalTime m_endTime;
    
    /**
     * Create an empty time period with start and end values initialised to LocalTime.Midnight and LocalTime.Max respectively
     */
    public TimePeriod()
    {
        m_startTime = LocalTime.MIDNIGHT;
        m_endTime = LocalTime.MAX;
    }
    
    /**
     * Create a time period with the prescribed start and end time
     * @param start the start of the period
     * @param end the end of the period
     * @throws IllegalArgumentException thrown when either argument is null or when start and end are the same
     */
    public TimePeriod(LocalTime start, LocalTime end) throws IllegalArgumentException
    {
        if(start == null || end == null)
        {
            throw new IllegalArgumentException("the times can't be null");
        }
        else if(start.equals(end))
        {
            throw new IllegalArgumentException("the start time is the same as the end time");
        }
        else 
        {
            if(start.isBefore(end))
            {
                m_startTime = start;
                m_endTime = end;
            }   
            else
            {
                m_startTime = end;
                m_endTime = start;
            }
        }
    }
    
    /**
     * Checks whether a LocalTime is within the time period
     * @param toCheck the time to check
     * @return true if it's in range, false otherwise
     */
    public Boolean withinRange(LocalTime toCheck)
    {
        //Integer greaterThan = startTime.compareTo(toCheck);
        //Integer lessThan = endTime.compareTo(toCheck);
   
        //return greaterThan > 0 && lessThan < 0;
        
        return m_startTime.isBefore(toCheck) && m_endTime.isAfter(toCheck);
    }   

    /**
     * Gets the start time of the time period
     * @return the start time
     */
    public LocalTime getStartTime() 
    {
        return m_startTime;
    }
    
    /**
     * Gets the end time of the time period
     * @return the end time
     */
    public LocalTime getEndTime() 
    {
        return m_endTime;
    }

    /**
     * Checks whether a time period is equal to another
     * @param toCompare the period to compare
     * @return true if the start and end times match up or if the time period to compare is this instance
     */
    @Override
    public boolean equals(Object toCompare) 
    {
        if(toCompare == null)
        {
            return false;
        }
        else if(toCompare == this)
        {
            return true;
        }
        else if(toCompare.getClass() == TimePeriod.class)
        {
            TimePeriod comparePeriod = (TimePeriod)toCompare;
            
            boolean result;
            
            result = comparePeriod.getEndTime().equals(m_endTime) && comparePeriod.getStartTime().equals(m_startTime);
            
            return result;
        }
        
        return false;
    }

    /**
     * Get the hash code of the period
     * @return a hash code
     */
    @Override
    public int hashCode() 
    {
        int hash = 7;
        hash = 31 * hash + Objects.hashCode(this.m_startTime);
        hash = 31 * hash + Objects.hashCode(this.m_endTime);
        return hash;
    }
}