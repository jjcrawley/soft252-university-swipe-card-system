/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datamodel.restrictions.interfaces;

import datamodel.restrictions.OperatingMode;
import datamodel.restrictions.TimePeriod;
import datamodel.users.UserRole;
import datamodel.users.interfaces.IReadOnlyUser;
import java.time.LocalTime;

/**
 * Defines standard behaviour for a restriction based system
 * @author jjcrawley
 */
public interface IRestriction 
{

    /**
     * Adds a time period to the restriction
     * @param toAdd the time period to add
     * @return true if the period was added, false otherwise
     */
    public Boolean addTimePeriod(TimePeriod toAdd);

    /**
     * Removes a time period given an index
     * @param index the index to remove at
     * @return true if the removal was successful, false otherwise
     */
    public Boolean removeTimePeriod(int index);

    /**
     * Given a user, time and operating mode checks whether the user is allowed access
     * @param toCheck the user to check with
     * @param timetoCheck the time to check with
     * @param mode the operating mode to check with
     * @return true if the user is allowed access, false otherwise
     */
    public Boolean allowAccess(IReadOnlyUser toCheck, LocalTime timetoCheck, OperatingMode mode);

    /**
     * Given a user and operating mode check whether the user is allowed access
     * @param toCheck the user to check with
     * @param mode the mode to check with
     * @return true if the user is allowed access, false otherwise
     */
    public Boolean allowAccess(IReadOnlyUser toCheck, OperatingMode mode);

    /**
     * Retrieve the mode that the restriction is restricting
     * @return the restricted mode
     */
    public OperatingMode getModeRestriction();

    /**
     * Retrieve the user role that is being restricted
     * @return the restricted role
     */
    public UserRole getRole();

    /**
     * Set the access mode for the restriction
     * @param mode the mode to restrict
     */
    public void setAccessMode(OperatingMode mode);

    /**
     * Set the user Role to restrict
     * @param toRestrict the user role to restrict
     */
    public void setRole(UserRole toRestrict);
}
