/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datamodel.restrictions.interfaces;

import datamodel.logging.Access;
import datamodel.users.interfaces.ICard;
import datamodel.users.interfaces.IReadOnlyUser;
import java.time.LocalTime;

/**
 * Defines standard functionality that allows a class to have restrictions applied to it
 * This functionality uses a swipe card to enforce restrictions
 * @author jjcrawley
 */
public interface IRestrictable 
{

    /**
     * Given a user, check whether the user is allowed access
     * @param toCheck the user to check
     * @return true if access is granted, false otherwise
     */
    public Boolean allowedAccess(IReadOnlyUser toCheck);

    /**
     * Given a user and time, check whether the user is allowed access
     * @param toCheck the user to check
     * @param timeToCheck the time to check
     * @return true if access is granted, false otherwise
     */
    public Boolean allowedAccess(IReadOnlyUser toCheck, LocalTime timeToCheck);

    /**
     * Swipes a card and return the access type
     * @param swipeCard the swipe card to swipe
     * @return the access permission that was given 
     */
    public Access swipeCard(ICard swipeCard);

    /**
     * Logs a swipe card 
     * @param swipeCard the swipe card to log
     * @param accessed the access type
     */
    public void logSwipeCard(ICard swipeCard, Access accessed);
}
