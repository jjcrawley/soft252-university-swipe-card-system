/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datamodel.users;

/**
 * A simple empty user, this class does nothing and only serves to point out when an invalid user type was made
 * @author jjcrawley
 */
public final class EmptyUser extends User
{

    /**
     * Create an empty user
     */
    public EmptyUser()
    {
        super();
    } 
    
    @Override
    public UserRole viewRole() 
    {
        return null;
    }
}
