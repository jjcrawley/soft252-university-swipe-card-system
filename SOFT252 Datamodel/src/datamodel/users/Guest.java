/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datamodel.users;

import datamodel.details.interfaces.IAddress;
import datamodel.users.interfaces.ICard;
import datamodel.users.interfaces.IName;

/**
 * A simple guest user implementation
 * @author jjcrawley
 */
public final class Guest extends User
{

    /**
     * Create an empty guest user, a card is automatically assigned
     */
    public Guest()
    {
        super();
        assignCard(new Card(hashCode()));
    }      

    /**
     * Creates a guest user with the specified details
     * @param userName the users name
     * @param gender the users gender
     * @param userAge the users age
     * @param userAddress the users address 
     * @param userID the users id
     * @param userCard the users card
     */
    public Guest(IName userName, Gender gender, int userAge, IAddress userAddress, int userID, ICard userCard)
    {
        super(userName, gender, userAge, userAddress, userID, userCard);
    }    
    
    @Override
    public UserRole viewRole()
    {
        return UserRole.Guest;        
    }    
}
