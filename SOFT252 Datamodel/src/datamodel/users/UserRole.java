/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datamodel.users;

import java.io.Serializable;

/**
 * A basic role enumeration for all user types supported by the system
 * @author jjcrawley
 */
public enum UserRole implements Serializable
{

    /**
     * Guest type
     */
    Guest,

    /**
     * Staff type
     */
    Staff,

    /**
     * Student type
     */
    Student,

    /**
     * Contract cleaner type
     */
    ContractCleaner,

    /**
     * Manager type
     */
    Manager,

    /**
     * Security type
     */
    Security,

    /**
     * Emergency Responder type
     */
    EmergencyResponder;
}
