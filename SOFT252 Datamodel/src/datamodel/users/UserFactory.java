/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datamodel.users;

import datamodel.details.interfaces.IAddress;
import datamodel.users.interfaces.ICard;
import datamodel.users.interfaces.IName;
import datamodel.users.interfaces.IUser;

/**
 * A user factory to aid in the construction of all supported user types
 * @author jjcrawley
 */
public class UserFactory
{
    private UserFactory()
    {} 
    
    /**
     * Creates a user of a given type with the prescribed details
     * @param typeToCreate the user type to create
     * @param userName the users name
     * @param gender the user gender
     * @param userAge the users age
     * @param userAddress the users address
     * @param userID the users id
     * @param userCard the users card
     * @return the newly constructed user, if the type wasn't recognised then an empty user is returned
     */
    public IUser createUser(UserRole typeToCreate, IName userName, Gender gender, int userAge, IAddress userAddress, int userID, ICard userCard) 
    {
        switch(typeToCreate)
        {
            case Guest:
                return new Guest(userName, gender, userAge, userAddress, userID, userCard);
            case Student:
                return new Student(userName, gender, userAge, userAddress, userID, userCard);
            case Manager:
                return new Manager(userName, gender, userAge, userAddress, userID, userCard);
            case ContractCleaner:
                return new ContractCleaner(userName, gender, userAge, userAddress, userID, userCard);
            case Security:
                return new Security(userName, gender, userAge, userAddress, userID, userCard);
            case EmergencyResponder:
                return new EmergencyResponder(userName, gender, userAge, userAddress, userID, userCard);
            case Staff:
                return new Staff(userName, gender, userAge, userAddress, userID, userCard);                
            default:
                return new EmptyUser();
        }
    } 
    
    /**
     * Get a new instance of the user factory
     * @return a new user factory
     */
    public static UserFactory getUserFactory()
    {
        return new UserFactory();
    }
}
