/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datamodel.users;

import datamodel.details.Address;
import datamodel.details.interfaces.IAddress;
import datamodel.details.interfaces.IReadOnlyAddress;
import datamodel.users.interfaces.*;
import java.io.Serializable;

/**
 * An abstract user class to be inherited by any and all user types
 * @author jjcrawley
 */
public abstract class User implements IUser, Serializable
{
    private int m_userID = 0;
    private ICard m_userCard = null;
    private IName m_userName = new UserName();
    private int m_userAge;
    private Gender m_userGender = Gender.Male;
    private IAddress m_userAddress = new Address(); 
    
    /**
     * Creates an empty user with the id being set to the hash code
     */
    protected User()
    {
        m_userID = hashCode();
        m_userAge = 0;
    }  
    
    /**
     * Creates a user with the specified details
     * @param userName the users name
     * @param gender the users gender
     * @param userAge the users age
     * @param userAddress the users address 
     * @param userID the users id
     * @param userCard the users card
     * @throws IllegalArgumentException thrown when any nullable value is null or the id or user age is less than 0
     */
    protected User(IName userName, Gender gender, int userAge, IAddress userAddress, int userID, ICard userCard) throws IllegalArgumentException
    {
        if(userName == null || userID < 0 || userAddress == null || userCard == null || gender == null)
        {
            throw new IllegalArgumentException("user ID must be greater than zero and no values can be null");
        }
        else
        {
            assignCard(userCard);
            m_userAddress = userAddress;
            m_userGender = gender;
            m_userName = userName;
            m_userID = userID;
            m_userAge = userAge;
        }
    }

    @Override
    public int viewAge() 
    {
        return m_userAge;
    }

    @Override
    public void setAge(int newAge) throws IllegalArgumentException
    {
        if(newAge < 0)
        {
            throw new IllegalArgumentException("new age can't be zero");
        }
        else
        {
            m_userAge = newAge;
        }
    }

    @Override
    public void setGender(Gender newGender) throws IllegalArgumentException
    {
        if(newGender != null)
        {
            m_userGender = newGender;
        }
        else
        {
            throw new IllegalArgumentException("new gender can't be null");            
        }
    }
    
    @Override
    public IAddress getAddressDetails() 
    {
        return m_userAddress;
    }

    @Override
    public IReadOnlyAddress viewUserAddress() 
    {
        return m_userAddress;
    }

    @Override
    public Gender viewGender() 
    {
        return m_userGender;
    }   

    @Override
    public IName getUserName() 
    {
        return m_userName;        
    }

    @Override
    public IReadOnlyName viewUserName() 
    {
        return m_userName;
    }   
    
    @Override
    public final void assignCard(ICard toAssign) throws IllegalArgumentException
    {
        if(toAssign != null)
        {
            m_userCard = toAssign;
            toAssign.setUser(this);
        }
        else
        {       
            throw new IllegalArgumentException("can't assign a null card");                
        }    
    }
    
    @Override
    public ICard getUserCard() 
    {
        return m_userCard;
    }   

    @Override
    public IReadOnlyCard viewCard() 
    {
        return m_userCard;
    }

    @Override
    public int viewUserID() 
    {
        return m_userID;
    }   
    
    @Override
    public abstract UserRole viewRole();

    @Override
    public void clearUser()
    {
        m_userCard = null;
    }   
    
    /**
     * Get an instance of the user builder class
     * @return a new user builder
     */
    public static UserBuilder getUserBuilder()
    {
        return UserBuilder.getUserBuilder();
    }
    
    /**
     * Get an instance of the user factory class
     * @return a new user factory
     */
    public static UserFactory getUserFactory()
    {
        return UserFactory.getUserFactory();
    }    

    /**
     * Retrieve a string representation of the user: userID, userFullName
     * @return formatted string of the user
     */
    @Override
    public String toString() 
    {
        return "User: " + m_userID + " " + m_userName.getFullName();
    }   
}
