/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datamodel.users;

import datamodel.details.interfaces.IAddress;
import datamodel.users.interfaces.ICard;
import datamodel.users.interfaces.IName;

/**
 * A simple staff member implementation
 * @author jjcrawley
 */
public final class Staff extends User
{

    /**
     * Create an empty staff member, a card is automatically assigned
     */
    public Staff()            
    {
        super();
        assignCard(new Card(hashCode()));
    }     

    /**
     * Creates a staff member with the specified details
     * @param userName the users name
     * @param gender the users gender
     * @param userAge the users age
     * @param userAddress the users address 
     * @param userID the users id
     * @param userCard the users card
     */
    public Staff(IName userName, Gender gender, int userAge, IAddress userAddress, int userID, ICard userCard) 
    {
        super(userName, gender, userAge, userAddress, userID, userCard);
    }
    
    
    
    @Override
    public UserRole viewRole() 
    {
        return UserRole.Staff;
    }    
}
