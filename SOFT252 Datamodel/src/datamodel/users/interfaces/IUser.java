/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datamodel.users.interfaces;

import datamodel.details.interfaces.IAddress;
import datamodel.users.Gender;

/**
 * Defines standard behaviours for a user with editable details 
 * @author jjcrawley
 */
public interface IUser extends IReadOnlyUser
{

    /**
     * Assign a card to the user
     * @param toAssign the card to assign
     * @throws IllegalArgumentException thrown if the card is null
     */
    public void assignCard(ICard toAssign) throws IllegalArgumentException;

    /**
     * Set the users age
     * @param newAge the new age of the user
     * @throws IllegalArgumentException thrown if the age is less than 0
     */
    public void setAge(int newAge) throws IllegalArgumentException;

    /**
     * Set the users gender
     * @param newGender the new gender
     * @throws IllegalArgumentException thrown if the gender is null
     */
    public void setGender(Gender newGender) throws IllegalArgumentException;

    /**
     * Retrieve the users card
     * @return the users card
     */
    public ICard getUserCard();

    /**
     * Retrieve an editable version of the user name
     * @return the user name
     */
    public IName getUserName();

    /**
     * Retrieve and editable version of the user address
     * @return the user address
     */
    public IAddress getAddressDetails();
    
    /**
     * clears the user
     * Use this to perform any additional cleanup
     */
    public void clearUser();
}
