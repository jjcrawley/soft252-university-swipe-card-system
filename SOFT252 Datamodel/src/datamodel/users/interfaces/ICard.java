/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datamodel.users.interfaces;

import datamodel.users.UserRole;
import java.util.List;

/**
 * Defines standard behaviour for a card that can have its details edited
 * @author jjcrawley
 */
public interface ICard extends IReadOnlyCard
{        

    /**
     * Adds a user role to the card
     * @param toAdd the role to add
     * @return true if the role was added, false otherwise
     */
    public Boolean addRole(UserRole toAdd);

    /**
     * Removes a user role from the card
     * @param toRemove the role to remove
     * @return true if the role was removed, false otherwise
     */
    public boolean removeRole(UserRole toRemove);     

    /**
     * Set the user of the card
     * @param user the user to assign the card to
     */
    public void setUser(IUser user);
}
