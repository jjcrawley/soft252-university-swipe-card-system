/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datamodel.users.interfaces;

/**
 * Defines standard behaviours for an editable user name
 * @author jjcrawley
 */
public interface IName extends IReadOnlyName
{   

    /**
     * Set the first name
     * @param newFirstName the new first name 
     */
    public void setFirstName(String newFirstName);

    /**
     * Set the middle name
     * @param newMiddleName the new middle name
     */
    public void setMiddleName(String newMiddleName);

    /**
     * Set the surname
     * @param newSurname the new surname
     */
    public void setSurname(String newSurname);       
}
