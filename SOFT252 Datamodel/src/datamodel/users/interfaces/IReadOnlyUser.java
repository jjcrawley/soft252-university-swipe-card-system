/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datamodel.users.interfaces;

import datamodel.details.interfaces.IReadOnlyAddress;
import datamodel.users.Gender;
import datamodel.users.UserRole;

/**
 * Defines standard functionality for a user that can have their details read
 * @author jjcrawley
 */
public interface IReadOnlyUser 
{    

    /**
     * Get the user id of the user
     * @return the user id
     */
    public int viewUserID();

    /**
     * Get the user role of the user
     * @return the user role
     */
    public UserRole viewRole();    

    /**
     * Get a read only version of the user name
     * @return the user name
     */
    public IReadOnlyName viewUserName();

    /**
     * Get a readonly version of the user address
     * @return the users address
     */
    public IReadOnlyAddress viewUserAddress();

    /**
     * Get the users gender
     * @return the users gender
     */
    public Gender viewGender();

    /**
     * Get the age of the user
     * @return the users age
     */
    public int viewAge();

    /**
     * Get a read only version of the users card
     * @return the users card
     */
    public IReadOnlyCard viewCard();
}