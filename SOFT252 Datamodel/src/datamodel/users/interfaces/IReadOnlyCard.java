/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datamodel.users.interfaces;

import datamodel.users.UserRole;
import java.util.List;

/**
 * Defines standard behaviours for a read only card
 * @author jjcrawley
 */
public interface IReadOnlyCard 
{

    /**
     * Retrieve the card id
     * @return the card id
     */
    public int viewCardID(); 

    /**
     * Retrieve a read only version of the cards user
     * @return the cards user
     */
    public IReadOnlyUser viewUser(); 

    /**
     * Checks whether the card has the prescribed role
     * @param toCheck the role to check with
     * @return true if the card has the role, false otherwise
     */
    public boolean hasUserRole(UserRole toCheck);

    /**
     * Retrieves the list of card roles within the card
     * @return a list of card roles
     */
    public List<UserRole> viewCardRoles();
}
