/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datamodel.users.interfaces;

/**
 * Defines standard behaviours for a read only user name
 * @author jjcrawley
 */
public interface IReadOnlyName 
{

    /**
     * Retrieve the first name
     * @return the first name
     */
    public String getFirstName();

    /**
     * Retrieve the surname
     * @return the surname
     */
    public String getSurname();

    /**
     * Retrieve the middle name
     * @return the middle name
     */
    public String getMiddleName();    

    /**
     * Retrieves a formatted string of the full user name
     * @return the full user name
     */
    public String getFullName();  
}
