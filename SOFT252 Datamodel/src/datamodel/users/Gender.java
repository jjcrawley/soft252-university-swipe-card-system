/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datamodel.users;

/**
 * A simple enumeration for all user genders supported by the system
 * @author jjcrawley
 */
public enum Gender 
{

    /**
     * Male gender
     */
    Male,

    /**
     * Female gender
     */
    Female;
}
