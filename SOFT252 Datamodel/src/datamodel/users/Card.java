/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datamodel.users;

import datamodel.users.interfaces.ICard;
import datamodel.users.interfaces.IUser;
import java.io.Serializable;
import java.util.*;

/**
 * A user card implementation to be used by the system
 * @author jjcrawley
 */
public class Card implements ICard, Serializable
{
    private final List<UserRole> m_cardRoles = new ArrayList<>();
    private final int m_cardID;
    private IUser m_user;
    
    /**
     * Create a basic card, the card id is initialised to the hash code
     */
    public Card()
    {
        m_cardID = hashCode();
        m_user = new EmptyUser();
    }  

    /**
     * Creates a user card with the prescribed card id
     * @param cardID the card id
     */
    public Card(int cardID)
    {
        m_user = new EmptyUser();
        m_cardID = cardID;
    }   

    @Override
    public boolean hasUserRole(UserRole toCheck) 
    {
        if(toCheck == null)
        {
            return false;
        }
        else
        {
            return m_cardRoles.contains(toCheck);
        }
    }   
    
    @Override
    public Boolean addRole(UserRole toAdd)
    {
        if(toAdd == null)
        {
            return false;
        }
        else if(m_cardRoles.contains(toAdd))
        {
            return false;
        }
        else
        {
            return m_cardRoles.add(toAdd);
        }
    }
    
    @Override
    public boolean removeRole(UserRole toRemove)
    {
        if(toRemove == null)
        {   
            return false;
        }
        else if(m_cardRoles.contains(toRemove))
        {
            if(toRemove.equals(m_user.viewRole()))
            {
                return false;
            }
            else
            {
                return m_cardRoles.remove(toRemove);
            }
        }
        else
        {
            return false;
        }
    }

    @Override
    public int viewCardID() 
    {
        return m_cardID;
    }    

    @Override
    public IUser viewUser() 
    {
        return m_user;
    }    

    /**
     * Returns a string representation of the card: cardRoles, card id, user
     * @return a string representation
     */
    @Override
    public String toString() 
    {
        return "Card{" + "cardRoles: " + m_cardRoles + ", cardID: " + m_cardID + ", myUser: " + m_user + '}';
    }   

    @Override
    public void setUser(IUser user) throws IllegalArgumentException
    {
        if(user != null)
        {
            m_user = user;
            m_cardRoles.clear();
            m_cardRoles.add(user.viewRole());
        }
        else
        {
            throw new IllegalArgumentException("User can't be null");
        }
    }

    @Override
    public List<UserRole> viewCardRoles() 
    {
        List<UserRole> rolesList = new ArrayList<>(m_cardRoles.size());
        
        m_cardRoles.stream().forEach((currentRole) -> 
        {
            rolesList.add(currentRole);
        });
        
        return rolesList;                
    }   
}
