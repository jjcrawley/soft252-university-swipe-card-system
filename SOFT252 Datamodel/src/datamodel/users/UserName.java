/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datamodel.users;

import datamodel.users.interfaces.IName;
import java.io.Serializable;

/**
 * An implementation for a basic user name that has a first, middle and surname
 * @author jjcrawley
 */
public class UserName implements IName, Serializable
{
    private String m_firstName = "Unknown";
    private String m_secondName = "";
    private String m_surname = "Unknown";
    
    /**
     * Create an empty user name
     */
    public UserName()
    {}
    
    /**
     * Create a user name with the prescribed details
     * @param firstName the first name
     * @param surname the surname
     * @throws IllegalArgumentException thrown when either detail is null, or empty
     */
    public UserName(String firstName, String surname) throws IllegalArgumentException
    {
        if(firstName == null || surname == null)
        {
            throw new IllegalArgumentException("can't create a legal name with the current parameters");  
        }
        else if(firstName.isEmpty() || surname.isEmpty())
        {
            throw new IllegalArgumentException("can't create a legal name with the current parameters");             
        }
        else
        {
            m_firstName = firstName;
            m_surname = surname;
        }
    }
    
    /**
     * Create a user name with the given parameters
     * @param firstName the users first name
     * @param secondName the users second name
     * @param surname the users surname
     * @throws IllegalArgumentException thrown when either argument is null or empty, exception being second name which may be empty
     */
    public UserName(String firstName, String secondName, String surname) throws IllegalArgumentException
    {
        if(firstName == null || surname == null || secondName == null)
        {
            throw new IllegalArgumentException("can't create a legal name with the current parameters");  
        }
        else if(firstName.isEmpty() || surname.isEmpty())
        {
            throw new IllegalArgumentException("can't create a legal name with the current parameters");
        }
        else
        {
            this.m_firstName = firstName;
            this.m_secondName = secondName;
            this.m_surname = surname;
        }
    }

    @Override
    public String getFirstName() 
    {
        return m_firstName;
    }

    @Override
    public String getSurname() 
    {
        return m_surname;
    }

    @Override
    public String getMiddleName() 
    {
        return m_secondName;
    }

    @Override
    public String getFullName() 
    {        
        return m_firstName + " " + m_secondName + " " + m_surname;
    }     

    @Override
    public void setFirstName(String newFirstName) throws IllegalArgumentException
    {
        if(newFirstName == null)
        {
            throw new IllegalArgumentException("first name can't be null");
        }
        else if(!newFirstName.isEmpty())
        {
            m_firstName = newFirstName;
        }
        else
        {
            throw new IllegalArgumentException("first name can't be empty");
        }
    }

    @Override
    public void setMiddleName(String newMiddleName) throws IllegalArgumentException
    {
        if(newMiddleName == null)
        {
            throw new IllegalArgumentException("middle name can't be null");
        }
        else 
        {
            m_secondName = newMiddleName;
        }        
    }

    @Override
    public void setSurname(String newSurname) throws IllegalArgumentException
    {
        if(newSurname == null)
        {
            throw new IllegalArgumentException("user surname can't be null");
        }
        if(!newSurname.isEmpty())
        {
            m_surname = newSurname;
        }
        else
        {
            throw new IllegalArgumentException("user surname can't be empty");
        }
    }    
}
