/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datamodel.users;

import datamodel.details.interfaces.IAddress;
import datamodel.users.interfaces.ICard;
import datamodel.users.interfaces.IName;

/**
 * A simple contract cleaner implementation
 * @author jjcrawley
 */
public final class ContractCleaner extends User
{

    /**
     * Create an empty contract cleaner, a card is automatically assigned
     */
    public ContractCleaner()
    {
        super();
        assignCard(new Card(hashCode()));
    }      

    /**
     * Create a contract cleaner with the prescribed details
     * @param userName the username
     * @param gender the gender
     * @param userAge the users age
     * @param userAddress the users address
     * @param userID the users id
     * @param userCard the users card
     */
    public ContractCleaner(IName userName, Gender gender, int userAge, IAddress userAddress, int userID, ICard userCard) 
    {
        super(userName, gender, userAge, userAddress, userID, userCard);
    }    

    @Override
    public UserRole viewRole() 
    {
        return UserRole.ContractCleaner;
    }   
}
