/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datamodel.users;

import datamodel.details.Address;
import datamodel.details.interfaces.IAddress;
import datamodel.users.interfaces.ICard;
import datamodel.users.interfaces.IName;
import datamodel.users.interfaces.IUser;

/**
 * A user builder class to aid with the construction of users
 * Should over manually constructing users with the new key word
 * @author jjcrawley
 */
public final class UserBuilder 
{
    private IName m_userName = new UserName(); 
    private int m_userAge = 0;
    private IAddress m_address = new Address();
    private Gender m_userGender = Gender.Female;
    private int m_userID = 0;
    private ICard m_userCard = new Card();
    private UserRole m_userRole = UserRole.Guest;
    private final UserFactory m_factory = UserFactory.getUserFactory();
    
    private UserBuilder()
    {
        
    }

    /**
     * Set the user name for the builder
     * @param userName the user name
     * @return this instance of the builder
     * @throws IllegalArgumentException thrown when the user name is null
     */
    public UserBuilder setUserName(IName userName) throws IllegalArgumentException
    {
        if(userName == null)
        {
            throw new IllegalArgumentException("can't accept a null usr name");
        }
        else
        {
            m_userName = userName;
            return this;
        }
    }
    
    /**
     * Sets the user age of the user being constructed
     * @param newAge the user age
     * @return this instance of the builder
     * @throws IllegalArgumentException thrown when the user age is less than 0
     */
    public UserBuilder setUserAge(int newAge) throws IllegalArgumentException
    {
        if(newAge <= 0)
        {
            throw new IllegalArgumentException("new age can't be less than or equal to zero");
        }
        else
        {           
            m_userAge = newAge;
            return this;
        }
    }

    /**
     * Set the address of the user being constructed
     * @param address the user address
     * @return this instance of the builder
     * @throws IllegalArgumentException thrown when the user address is null
     */
    public UserBuilder setUserAddress(IAddress address) throws IllegalArgumentException
    {
        if(address == null)
        {
            throw new IllegalArgumentException("address can't be empty");
        }
        else
        {
            m_address = address;
            return this;
        }
    }

    /**
     * Set the gender of the user being constructed
     * @param userGender the user gender
     * @return this instance of the builder
     * @throws IllegalArgumentException thrown when the gender is null
     */
    public UserBuilder setUserGender(Gender userGender) throws IllegalArgumentException
    {
        if(userGender == null)
        {
            throw new IllegalArgumentException("the user must have a gender");
        }
        else
        {
            m_userGender = userGender;
            return this;
        }
    }

    /**
     * Set the user id of the user being constructed
     * @param userID the user id
     * @return this instance of the builder
     * @throws IllegalArgumentException thrown when the user id is less than 0
     */
    public UserBuilder setUserID(int userID) throws IllegalArgumentException
    {
        if(userID < 0)
        {
            throw new IllegalArgumentException("invalid user id, can't be less than zero");
        }
        else
        {
            m_userID = userID;
            return this;
        }
    }

    /**
     * Set the user card of the user being constructed
     * @param userCard the users card
     * @return this instance of the builder
     * @throws IllegalArgumentException thrown when the user card is null
     */
    public UserBuilder setUserCard(ICard userCard) throws IllegalArgumentException
    {
        if(userCard == null)
        {
            throw new IllegalArgumentException("can't assign an empty card");
        }
        else
        {
            m_userCard = userCard;
            return this;
        } 
    }
    
    /**
     * Set the user role of the user being constructed
     * @param userRole the user role
     * @return this instance of the builder
     * @throws IllegalArgumentException thrown when the user role is null
     */
    public UserBuilder setUserRole(UserRole userRole) throws IllegalArgumentException
    {
        if(userRole == null)
        {
            throw new IllegalArgumentException("can't assign a null user role");
        }
        else
        {
            m_userRole = userRole;
            return this;
        }
    }
    
    /**
     * Retrieve an instance of the user builder
     * @return a new instance of the user builder
     */
    public static UserBuilder getUserBuilder()
    {
        return new UserBuilder();
    }   
    
    /**
     * Build a user with the details stored in the builder
     * @return the newly constructed user
     */
    public IUser buildUser()
    {
        return m_factory.createUser(m_userRole, m_userName, m_userGender,  m_userAge, m_address, m_userID, m_userCard);
    }
    
    /**
     * Build a user with the details stored in the builder of a given type
     * @param toCreate the type of user to construct
     * @return the newly constructed user
     */
    public IUser buildUser(UserRole toCreate)
    {
        return m_factory.createUser(toCreate, m_userName, m_userGender,  m_userAge, m_address, m_userID, m_userCard);
    }
}
