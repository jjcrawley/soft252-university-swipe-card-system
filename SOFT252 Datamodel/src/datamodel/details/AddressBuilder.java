/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datamodel.details;

import datamodel.details.interfaces.IAddress;

/**
 * Use this class to create a simple with the prescribed details
 * Should be used in favour of manually creating an address
 * @author jjcrawley
 */
public class AddressBuilder 
{
    private String m_addressLine1 = "Unknown";
    private String m_addressLine2 = "";
    private String m_city = "Unknown";
    private String m_county = "Unknown";
    private String m_postCode = "Unknown";
    
    private AddressBuilder()
    {}

    /**
     * Sets the address line 1 for the address to be constructed
     * @param addressLine1 the new address line 1
     * @return the address builder instance
     * @throws IllegalArgumentException thrown when the address line is null or empty
     */
    public AddressBuilder setAddressLine1(String addressLine1) throws IllegalArgumentException
    {
        if(addressLine1 == null || addressLine1.isEmpty())
        {
            throw new IllegalArgumentException("invalid address line1");
        }
        else
        {        
            m_addressLine1 = addressLine1;
            return this;
        }
    }

    /**
     * Sets the address line 2 for the address to be constructed
     * @param addressLine2 the new address line 2
     * @return the address builder instance
     * @throws IllegalArgumentException thrown when the address line is null
     */
    public AddressBuilder setAddressLine2(String addressLine2) throws IllegalArgumentException
    {
        if(addressLine2 == null)
        {
            throw new IllegalArgumentException("invalid address line2");
        }
        else
        {   
            m_addressLine2 = addressLine2;
            return this;
        }
    }

    /**
     * Sets the city for the address to be constructed
     * @param city the new city
     * @return the address builder instance
     * @throws IllegalArgumentException thrown when the city is null or empty
     */
    public AddressBuilder setCity(String city) throws IllegalArgumentException
    {
        if(city == null || city.isEmpty())
        {
            throw new IllegalArgumentException("invalid city name");
        }
        else
        {
            m_city = city;
            return this;
        }
    }

    /**
     * Sets the county for the address to be constructed
     * @param county the county for the address
     * @return the address builder instance
     * @throws IllegalArgumentException thrown when the county is null or empty
     */
    public AddressBuilder setCounty(String county) throws IllegalArgumentException
    {
        if(county == null || county.isEmpty())
        {
            throw new IllegalArgumentException("invalid county");
        }
        else
        {
            m_county = county;
            return this;
        }
    }

    /**
     * Sets the post code for the address to be constructed
     * @param postCode the addresses post code
     * @return the address builder instance
     * @throws IllegalArgumentException thrown when the post code is null or empty
     */
    public AddressBuilder setPostCode(String postCode) throws IllegalArgumentException
    {
        if(postCode == null || postCode.isEmpty())
        {
            throw new IllegalArgumentException("invalid postCode");
        }
        else
        {
            m_postCode = postCode;
            return this;
        } 
    }
    
    /**
     * Get an address builder instance
     * @return a new address builder
     */
    public static AddressBuilder getAddressBuilder()
    {
        return new AddressBuilder();
    }
    
    /**
     * Builds the address using the given information
     * @return the newly constructed address
     * @throws IllegalArgumentException thrown when theirs an issue in construction
     */
    public IAddress buildAddress() throws IllegalArgumentException
    {
        return new Address(m_addressLine1, m_addressLine2, m_city, m_county, m_postCode);
    }
}
