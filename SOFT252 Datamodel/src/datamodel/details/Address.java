/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datamodel.details;

import datamodel.details.interfaces.IAddress;
import java.io.Serializable;

/**
 * A simple address implementation that stores an address line 1, 2, a city name, a county and a postcode
 * @author jjcrawley
 */
public class Address implements IAddress, Serializable
{
    private String m_addressLine1 = "";
    private String m_addressLine2 = "";
    private String m_city = "";
    private String m_county = "";
    private String m_postCode = "";

    /**
     * Create an empty address
     */
    public Address() 
    {
    }
    
    /**
     * Create an address with the prescribed details
     * @param addressLine1 the addresses address line 1
     * @param addressLine2 the addresses address line 2
     * @param city the addresses city
     * @param county the addresses county
     * @param postCode the addresses post code
     * @throws IllegalArgumentException thrown when either of details is null or empty, only exception being address line 2, which can be empty
     */
    public Address(String addressLine1, String addressLine2, String city, String county, String postCode) throws IllegalArgumentException
    {
        if(addressLine1 == null || addressLine2 == null || city == null || county == null || postCode == null)
        {
            throw new IllegalArgumentException("can't create a legal address with the current parameters");
        }
        else if(addressLine1.isEmpty() || city.isEmpty() || county.isEmpty() || postCode.isEmpty())
        {
            throw new IllegalArgumentException("can't create a legal address with the current parameters");
        }
        else
        {
            m_addressLine1 = addressLine1;
            m_addressLine2 = addressLine2;
            m_city = city;
            m_county = county;
            m_postCode = postCode;
        }   
    }
    
    @Override
    public void setAddressLine1(String newAddressLine1) throws IllegalArgumentException
    {
        if(newAddressLine1 == null || newAddressLine1.isEmpty())
        {
            throw new IllegalArgumentException("invalid address line1");
        }
        else
        {
            m_addressLine1 = newAddressLine1;
        }
    }

    @Override
    public void setAddressLine2(String newAddressLine2) throws IllegalArgumentException
    {       
        if(newAddressLine2 != null)
        {
            m_addressLine2 = newAddressLine2;      
        }
        else
        {
            throw new IllegalArgumentException("invalid address line2");            
        }                
    }

    @Override
    public void setCity(String newCity) throws IllegalArgumentException
    {
        if(newCity == null || newCity.isEmpty())
        {
            throw new IllegalArgumentException("invalid address line2");
        }
        else
        {
            m_city = newCity;
        }
    }

    @Override
    public void setCounty(String newCounty) throws IllegalArgumentException
    {
        if(newCounty == null || newCounty.isEmpty())
        {
            throw new IllegalArgumentException("invalid county");
        }
        else
        {
            m_county = newCounty;
        }
    }

    @Override
    public void setPostCode(String newPostCode) throws IllegalArgumentException
    {
        if(newPostCode == null || newPostCode.isEmpty())
        {
            throw new IllegalArgumentException("invalid postCode");
        }
        else
        {
            m_postCode = newPostCode;
        }
    }

    @Override
    public String getAddressLine1() 
    {
        return m_addressLine1;
    }

    @Override
    public String getAddressLine2() 
    {
        return m_addressLine2;
    }

    @Override
    public String getCity() 
    {
        return m_city;
    }

    @Override
    public String getCounty() 
    {
        return m_county;
    }

    @Override
    public String getPostCode() 
    {
        return m_postCode;
    }        
}
