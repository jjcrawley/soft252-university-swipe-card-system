/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datamodel.details.interfaces;

/**
 * Defines standard behaviours for an editable address
 * @author jjcrawley
 */
public interface IAddress extends IReadOnlyAddress
{

    /**
     * Set addressLine1 of the address
     * @param newLine1 the new AddressLine1
     * @throws IllegalArgumentException thrown if the address line is null or empty
     */
    public void setAddressLine1(String newLine1) throws IllegalArgumentException;

    /**
     * Set addressLine2 of the address
     * @param newLine2 the new AddressLine2
     * @throws IllegalArgumentException thrown if the address line is null
     */
    public void setAddressLine2(String newLine2) throws IllegalArgumentException;

    /**
     * Set the city of the address
     * @param newCity the new city
     * @throws IllegalArgumentException thrown if the city is null or empty
     */
    public void setCity(String newCity) throws IllegalArgumentException;

    /**
     * Set the county of the address
     * @param newCounty the new county
     * @throws IllegalArgumentException thrown if the county is null or empty
     */
    public void setCounty(String newCounty) throws IllegalArgumentException;

    /**
     * Set the Post code of the address
     * @param newPostCode the new post code
     * @throws IllegalArgumentException thrown if the post code is null or empty
     */
    public void setPostCode(String newPostCode) throws IllegalArgumentException;
}
