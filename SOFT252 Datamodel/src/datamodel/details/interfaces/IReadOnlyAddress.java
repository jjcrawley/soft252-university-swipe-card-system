/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datamodel.details.interfaces;

/**
 * Defines standard behaviour to provide a read only address
 * @author jjcrawley
 */
public interface IReadOnlyAddress 
{

    /**
     * Retrieve addressLine1 of the address
     * @return addressLine1
     */
    public String getAddressLine1();

    /**
     * Retrieve addressLine2 of the address
     * @return addressLine2
     */
    public String getAddressLine2();

    /**
     * Retrieve the city 
     * @return the city
     */
    public String getCity();

    /**
     * Retrieve the county
     * @return the county
     */
    public String getCounty();

    /** 
     * Retrieve the post code 
     * @return the post code
     */
    public String getPostCode();
}
