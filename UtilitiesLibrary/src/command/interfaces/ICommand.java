/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package command.interfaces;

/**
 * An interface for creating simple command object
 * @author jjcrawley
 */
public interface ICommand extends ICommandBehaviour
{

    /**
     * Checks if the command has been executed
     * @return true if it's been executed, false otherwise
     */
    Boolean isExecuted();

    /**
     * Sets the executed state of the command
     * @param flag the new execution state, can't be null
     * @return the new executed state
     */
    Boolean setExecuted(Boolean flag);

    /**
     * Checks if the command has been undone
     * @return true if it's been undone, false otherwise
     */
    Boolean isUndone();
}
