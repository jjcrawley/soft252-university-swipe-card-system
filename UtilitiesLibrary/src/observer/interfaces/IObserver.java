/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package observer.interfaces;

/**
 * Defines standard behaviours for an observer
 * @author jjcrawley
 */
public interface IObserver 
{

    /**
     * Update the observer
     */
    public void update();    
}
