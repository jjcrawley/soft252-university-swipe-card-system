/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package observer.interfaces;

import java.util.List;

/**
 * Defines standard behaviours for a class that can be observed
 * @author jjcrawley
 */
public interface IObservable 
{

    /**
     * Register a new observer
     * @param observer the observer to register
     * @return true if the observer was registered, false otherwise
     */
    public Boolean registerObserver(IObserver observer);

    /**
     * Remove an observer
     * @param toRemove the observer to remove
     * @return true if the observer was removed, false otherwise
     */
    public Boolean removeObserver(IObserver toRemove);

    /**
     * Notify all observers
     */
    public void notifyObservers();

    /**
     * Get a list of all observers
     * @return the list of observers
     */
    public List<IObserver> getObservers();
}
