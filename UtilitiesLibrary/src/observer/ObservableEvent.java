/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package observer;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import observer.interfaces.IObserver;
import observer.interfaces.IObservable;

/**
 * A simple observable event
 * @author jjcrawley
 */
public class ObservableEvent implements IObservable, Serializable
{
    private final List<IObserver> m_observers;
    
    /**
     * Create a new observable event
     */
    public ObservableEvent()
    {
        m_observers = new ArrayList<>();
    }
    
    /**
     * Register a new observer to the event
     * @param observer the observer to register, can't be null
     * @return true if the observer was successfully registered
     */
    @Override
    public Boolean registerObserver(IObserver observer) 
    {        
        if(observer != null)
        {
            if(!m_observers.contains(observer))
            {
                return m_observers.add(observer);
            }
        }
        
        return false;
    }

    /**
     * Removes and observer from the event
     * @param toRemove the observer to remove, can't be null
     * @return true if the observer was successfully removed
     */
    @Override
    public Boolean removeObserver(IObserver toRemove) 
    {        
        if(toRemove != null)
        {
            if(!m_observers.contains(toRemove))
            {
               return m_observers.remove(toRemove);
            }
        }
        
        return false;
    }

    /**
     * Notify all observers of the event
     */
    @Override
    public void notifyObservers()
    {
        m_observers.stream().forEach((current) -> 
        {
            current.update();
        });
    }  
    
    /**
     * Allows you to see all the observers observing the event
     * @return a list of all the observers
     */
    @Override
    public List<IObserver> getObservers()
    {
        List<IObserver> observers = new ArrayList<>();
        
        m_observers.stream().forEach((observer) -> 
        {
            observers.add(observer);
        });
        
        return observers;
    }
}
