/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package serialisation;

import java.io.Serializable;

/**
 * Defines some additional behaviours that can be used for serialisation
 * You call these methods before or after your object has been serialised to allow for some additional setup if needed
 * @author Joshua
 */
public interface ISerialisable extends Serializable
{

    /**
     * Call this method after de-serialisation to initialise any necessary fields
     */
    void onRead();

    /**
     * Call this method before serialisation to prep it  
     */
    void onWrite();
}
